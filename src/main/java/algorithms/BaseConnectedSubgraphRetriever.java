package algorithms;

import domain.Grid;

/**
 * Created by ekravi on 18/05/2017.
 */
public abstract class BaseConnectedSubgraphRetriever implements ConnectedSubgraphRetriever {
    protected Grid grid;

    protected BaseConnectedSubgraphRetriever(Grid grid) {
        this.grid = grid;
    }
}
