package algorithms;

import domain.ConnectedSubgraph;

import java.util.Set;

/**
 * Created by ekravi on 18/05/2017.
 */
public interface ConnectedSubgraphRetriever {
    public Set<ConnectedSubgraph> retrieveForSize(int k);
}
