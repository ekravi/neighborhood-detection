package algorithms;


import domain.ConnectedSubgraph;
import domain.Grid;
import domain.Grid.Cell;

import java.util.HashSet;
import java.util.Set;

/**
 * Iterates over all connected subgraphs
 * Implementation based on
 * <a href=|http://stackoverflow.com/questions/15658245/efficiently-find-all-connected-induced-subgraphs">this link</a>
 */
public class Exhaustive extends BaseConnectedSubgraphRetriever {

    private Set<ConnectedSubgraph> cs = new HashSet<>();

    public Exhaustive(Grid grid) {
        super(grid);
    }

    @Override
    public Set<ConnectedSubgraph> retrieveForSize(int k) {
        cs.clear();
        recGetConnectedSugraphsForSize(new HashSet<>(grid.getCells()), null, new HashSet<Cell>(), k);
        return cs;
    }

    void recGetConnectedSugraphsForSize(Set<Cell> optional, ConnectedSubgraph currCS, Set<Cell> neighbors, int k) {
        // base case - solution larger than k break
        if (currCS != null) {
            if (currCS.size() > k)
                return;
                // base case - plot solution
            else
                cs.add(currCS);
        }

        // relevant cells are:
        // for empty solution - all optional cells
        // for non-empty solution - intersection of the optional and neighbors cells
        Set<Cell> relevant = (currCS == null) ? new HashSet<>(optional) :
                getRelevantCells(optional, neighbors);
        // for a relevant cell c, c is either in the solution or not
        for (Cell c : relevant) {
            Set<Cell> updatedOptional = getUpdatedOptional(optional, c);
            // a solution not including c
            recGetConnectedSugraphsForSize(updatedOptional, currCS, neighbors, k);

            // a solution including c
            ConnectedSubgraph updatedSolution = getUpdatedSolution(currCS, c);
            recGetConnectedSugraphsForSize(updatedOptional, updatedSolution, getUpdatedNeighbors(neighbors, c,
                    updatedSolution),
                    k);
        }
    }

    Set<Cell> getUpdatedOptional(Set<Cell> optional, Cell c) {
        Set<Cell> $ = new HashSet<>(optional);
        $.remove(c);
        return $;
    }

    /**
     * add neighbors of c that are not in the current solution
     */
    Set<Cell> getUpdatedNeighbors(Set<Cell> neighbors, Cell c, ConnectedSubgraph updatedSolution) {
        Set<Cell> $ = new HashSet<>(neighbors);
        $.addAll(grid.getNeighbors(c));
        if (updatedSolution != null) $.removeAll(updatedSolution.getCells());
        return $;
    }

    ConnectedSubgraph getUpdatedSolution(ConnectedSubgraph currSolution, Cell c) {
        if (currSolution == null) {
            Set<Cell> cs = new HashSet<>();
            cs.add(c);
            return new ConnectedSubgraph(grid, cs);
        } else {
            return new ConnectedSubgraph(currSolution).extend(c);
        }
    }

    Set<Cell> getRelevantCells(Set<Cell> optional, Set<Cell> neighbors) {
        Set<Cell> $ = new HashSet<>(optional);
        $.retainAll(neighbors);
        return $;
    }
}
