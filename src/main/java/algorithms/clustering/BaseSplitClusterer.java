package algorithms.clustering;

import algorithms.distance.IDistanceMeasure;

public abstract class BaseSplitClusterer implements ISplitClusterer {

    protected final IDistanceMeasure dm;

    protected BaseSplitClusterer(IDistanceMeasure dm) {
        this.dm = dm;
    }

}
