package algorithms.clustering;

import algorithms.distance.IDistanceMeasure;
import com.vividsolutions.jts.geom.Polygon;
import experiments.domain.RunResults;
import javafx.util.Pair;

import java.util.*;

public class BottomUpClusterer extends BaseSplitClusterer {

    public BottomUpClusterer(IDistanceMeasure dm) {
        super(dm);
    }

    @Override
    public RunResults cluster(Set<Polygon> toCluster, int k) {
        long startTime = new Date().getTime();

        // Saves the distance score for each pair of cells
        Map<Set<Polygon>, Set<Set<Polygon>>> neighbors = new HashMap<>();
        PriorityQueue<Pair<Set<Polygon>, Set<Polygon>>> byDistance = new PriorityQueue<>(dm);
        init(neighbors, byDistance, toCluster);

        System.out.println(neighbors.size());
        while (neighbors.size() > k) {
            if (neighbors.size() % 10 == 0)
                System.out.println(neighbors.size());
            singleMerge(neighbors, byDistance);
        }

        List<List<Polygon>> finalSolution = new ArrayList<>(neighbors.size());
        for (Set<Polygon> c : neighbors.keySet()) {
            finalSolution.add(new ArrayList<>(c));
        }
        return new RunResults(finalSolution, new Date().getTime() - startTime);
    }

    void singleMerge(Map<Set<Polygon>, Set<Set<Polygon>>> neighbors, PriorityQueue<Pair<Set<Polygon>, Set<Polygon>>>
            byDistance) {
        // poll the closests sets, denoted by a,b
        Pair<Set<Polygon>, Set<Polygon>> nearestClusters = byDistance.poll();

        Set<Polygon> from = nearestClusters.getKey(), to = nearestClusters.getValue();
        Set<Set<Polygon>> fromN = neighbors.get(nearestClusters.getKey()), toN = neighbors.get(to);

        toN.remove(from);
        fromN.remove(to);

        // merge them a<-a \cup b
        Set<Polygon> fromAndTo = new HashSet<>(from);
        fromAndTo.addAll(to);

        updateMap(neighbors, from, to, fromN, toN, fromAndTo);
        updatePQ(byDistance, from, to, fromN, toN, fromAndTo);
    }

    void updateMap(Map<Set<Polygon>, Set<Set<Polygon>>> neighbors, Set<Polygon> from, Set<Polygon> to,
                   Set<Set<Polygon>> fromN, Set<Set<Polygon>> toN, Set<Polygon> fromAndTo) {
        for (Set<Polygon> toNP : toN) {
            Set<Set<Polygon>> sets = neighbors.get(toNP);
            sets.remove(to);
            sets.remove(from);
            sets.add(fromAndTo);
        }
        for (Set<Polygon> fromNP : fromN) {
            Set<Set<Polygon>> sets = neighbors.get(fromNP);
            sets.remove(from);
            sets.remove(to);
            sets.add(fromAndTo);
        }
        neighbors.put(fromAndTo, new HashSet<>());
        neighbors.get(fromAndTo).addAll(fromN);
        neighbors.get(fromAndTo).addAll(toN);

        neighbors.remove(from);
        neighbors.remove(to);
    }

    void updatePQ(PriorityQueue<Pair<Set<Polygon>, Set<Polygon>>> byDistance, Set<Polygon> from, Set<Polygon>
            to, Set<Set<Polygon>> fromN, Set<Set<Polygon>> toN, Set<Polygon> fromAndTo) {
        Set<Set<Polygon>> nbrs = new HashSet<>(toN);
        nbrs.addAll(fromN);
        for (Set<Polygon> ps : nbrs) {
            byDistance.remove(new Pair<>(to, ps));
            byDistance.remove(new Pair<>(ps, to));
            byDistance.remove(new Pair<>(from, ps));
            byDistance.remove(new Pair<>(ps, from));

            byDistance.add(new Pair<>(fromAndTo, ps));
            byDistance.add(new Pair<>(ps, fromAndTo));
        }

        // from,to was polled before
        byDistance.remove(new Pair<>(to, from));
    }

    void init(Map<Set<Polygon>, Set<Set<Polygon>>> neighbors, PriorityQueue<Pair<Set<Polygon>,
            Set<Polygon>>> byDistance, Set<Polygon> toCluster) {
        for (Polygon fromP : toCluster) {
            for (Polygon toP : toCluster) {
                if (fromP.equals(toP) || !fromP.intersects(toP) || !fromP.intersection(toP).getGeometryType().equals
                        ("LineString"))
                    continue;
                HashSet<Polygon> from = new HashSet<Polygon>(), to = new HashSet<Polygon>();
                from.add(fromP);
                to.add(toP);

                if (!neighbors.containsKey(from))
                    neighbors.put(from, new HashSet<>());
                neighbors.get(from).add(to);
                byDistance.add(new Pair<>(from, to));
            }
        }
    }
}
