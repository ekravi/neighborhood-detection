package algorithms.clustering;

import com.vividsolutions.jts.geom.Polygon;
import experiments.domain.RunResults;

import java.util.Set;

public interface ISplitClusterer {
    /**
     * cluster a set of grid cells to k connected subgraphs
     */
    RunResults cluster(Set<Polygon> toCluster, int k);

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
