package algorithms.distance;

import com.vividsolutions.jts.geom.Polygon;
import javafx.util.Pair;

import java.util.Comparator;
import java.util.Set;

/**
 * Created by ekravi on 21/05/2017.
 */
public interface IDistanceMeasure extends Comparator<Pair<Set<Polygon>, Set<Polygon>>> {
    public double getDistance(Set<Polygon> from, Set<Polygon> to);

    public double getDistance(Polygon from, Polygon to);

    default int compare(Pair<Set<Polygon>, Set<Polygon>> p1, Pair<Set<Polygon>, Set<Polygon>> p2) {
        // minus to make MAX priority queue
        return -Double.compare(getDistance(p1.getKey(), p1.getValue()), getDistance(p2.getKey(), p2.getValue()));
    }
}
