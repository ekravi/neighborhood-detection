package algorithms.gridGraph;

import org.geotools.graph.build.basic.BasicGraphBuilder;
import org.geotools.graph.structure.Graph;
import org.geotools.graph.structure.Node;
import org.geotools.graph.structure.basic.BasicNode;
import org.geotools.graph.traverse.GraphIterator;
import org.geotools.graph.traverse.basic.BasicGraphTraversal;
import org.geotools.graph.traverse.basic.SimpleGraphWalker;
import org.geotools.graph.traverse.standard.BreadthFirstIterator;

/**
 * Created by ekravi on 20/07/2017.
 */
public class ConnectedSubgraphVerifier {
    private Graph m_graph;
    private int m_nvisited;

    public ConnectedSubgraphVerifier(Graph graph) {
        this.m_graph = graph;
    }

    public boolean verifyConnected(int nodesID) {
        ConnectedSubgraphVisitor visitor = new ConnectedSubgraphVisitor(nodesID);
        SimpleGraphWalker sgv = new SimpleGraphWalker(visitor);
        GraphIterator iterator = new BreadthFirstIterator();
        BasicGraphTraversal bgt = new BasicGraphTraversal(m_graph, sgv, iterator);

        bgt.traverse();

        System.out.println("connected found: " + visitor.getCount());
        return true;
    }

    public static void main(String[] args) {
        Node n11 = new BasicNode(), n12 = new BasicNode(), n13 = new BasicNode(), n21 = new BasicNode(), n22 = new
                BasicNode();
        n11.setID(1);
        n12.setID(1);
        n13.setID(1);
        n21.setID(2);

        BasicGraphBuilder b = new BasicGraphBuilder();
        b.addNode(n11);
        b.addNode(n12);
        b.addNode(n13);
        b.addNode(n21);

        b.buildEdge(n11, n12);
        b.buildEdge(n11, n21);
        b.buildEdge(n12, n21);
        b.buildEdge(n13, n21);

        new ConnectedSubgraphVerifier(b.getGraph()).verifyConnected(1);
    }
}
