package algorithms.gridGraph;

import org.geotools.graph.structure.GraphVisitor;
import org.geotools.graph.structure.Graphable;
import org.geotools.graph.traverse.GraphTraversal;

import java.util.Iterator;

/**
 * Created by ekravi on 20/07/2017.
 */
public class ConnectedSubgraphVisitor implements GraphVisitor {
    public final int id;
    public int idCount = 0;

    public ConnectedSubgraphVisitor(int id) {
        this.id = id;
    }

    @Override
    public int visit(Graphable component) {
        Iterator related = component.getRelated();
        if (!related.hasNext())
            return GraphTraversal.KILL_BRANCH;
        else {
            while (related.hasNext()) {
                Graphable neighbor = (Graphable) related.next();
                if (neighbor.getID() == id && !neighbor.isVisited()) {
                    idCount++;
                    return GraphTraversal.CONTINUE;
                }
            }
            return GraphTraversal.KILL_BRANCH;
        }
    }

    public int getCount() {
        return idCount;
    }
}
