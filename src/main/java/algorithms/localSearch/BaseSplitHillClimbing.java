package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import javafx.util.Pair;

import java.util.*;

public abstract class BaseSplitHillClimbing implements IBetterNeighborSplitFinder {
    protected final IStepEvaluator heur;

    public BaseSplitHillClimbing(IStepEvaluator heur) {
        this.heur = heur;
    }

    @Override
    public IStepEvaluator getHeur() {
        return heur;
    }


    /**
     * @return A map: <cell_id, subgraph_id> => Set<subgraphs_id>
     * Describing the neighboring subgraphs of each bordeing cell
     */
    Map<Pair<Integer, Integer>, Set<Integer>> getCellsWithNeighbors(List<List<Polygon>> current) {
        // for each bordering grid cell
        // build a map of neighbors of the cell and their associated MP
        Map<Pair<Integer, Integer>, Set<Integer>> $ = new HashMap<>();

        for (int i = 0; i < current.size(); ++i) {
            List<Polygon> currCS = current.get(i);
            for (int j = 0; j < currCS.size(); ++j) {
                Polygon cell = currCS.get(j);

                // iterate over next connected subgraphs of the list verifying each pair is examined once
                for (int k = i + 1; k < current.size(); ++k) {
                    List<Polygon> otherCS = current.get(k);
                    for (int l = 0; l < otherCS.size(); ++l) {
                        Polygon otherCell = otherCS.get(l);


                        if (cell.intersects(otherCell) && cell.intersection(otherCell).getGeometryType().equals
                                ("LineString")) {
                            Pair<Integer, Integer> borderCell = new Pair<>(i, j), bordersWith = new Pair<>(k, l);
                            if (!$.containsKey(borderCell))
                                $.put(borderCell, new HashSet<>());
                            if (!$.containsKey(bordersWith))
                                $.put(bordersWith, new HashSet<>());
                            $.get(borderCell).add(k);
                            $.get(bordersWith).add(i);
                        }
                    }
                }
            }
        }
        return $;
    }

    /**
     * run kind of BFS over the multi polygon to find whether it is a connected subgraph
     *
     * @param toCheck
     */
    // TODO: add unitest
    boolean isConnectedSubgraph(List<Polygon> toCheck) {
        Map<Integer, Boolean> visited = new HashMap<>();
        for (int i = 0; i < toCheck.size(); ++i) {
            visited.put(i, false);
        }
        LinkedList<Integer> queue = new LinkedList<>();
        int numVisited = 0;
        queue.addFirst(0);
        visited.put(0, true);
        do {
            Integer currIdx = queue.pollFirst();
            Polygon currPol = (Polygon) toCheck.get(currIdx);
            numVisited++;

            // adding neighbors to queue
            for (int i = 1; i < toCheck.size(); ++i) {
                if (visited.get(i).equals(false)) {
                    Polygon iPol = toCheck.get(i);
                    if (currPol.intersects(iPol) && currPol.intersection(iPol).getGeometryType().equals("LineString")) {
                        visited.put(i, true);
                        queue.addLast(i);
                    }
                }
            }
        } while (!queue.isEmpty());

        return numVisited == toCheck.size();
    }
}
