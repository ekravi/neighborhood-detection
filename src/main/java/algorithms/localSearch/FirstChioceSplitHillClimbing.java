package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import experiments.domain.StepResults;
import javafx.util.Pair;

import java.io.IOException;
import java.util.*;

/**
 * Created by ekravi on 16/07/2017.
 */
public class FirstChioceSplitHillClimbing extends BaseSplitHillClimbing {
    public FirstChioceSplitHillClimbing(IStepEvaluator heur) {
        super(heur);
    }

    /**
     * Given a split, the algorithm:
     * (1) finds all the bordering cells
     * (2) randomly chooses a cell in the border
     * (3) examines whether a new split where this cell is moved can further improve the split
     * (4) if yes - return the new split, otherwise considers the next split
     * Worst case - all neighboring splits are examined
     */
    @Override
    public StepResults getImprovedNeighbor(List<List<Polygon>> current) throws IOException {
        long startTime = new Date().getTime();
        Map<Pair<Integer, Integer>, Set<Integer>> neighboringCells = getCellsWithNeighbors(current);

//        List<List<Polygon>> $ = current;
        double currScr = heur.evaluate(current);
//        System.out.println("base score: " + currScr);

        // randomly choosing the cell to improve
        ArrayList<Pair<Integer, Integer>> pairs = new ArrayList<>(neighboringCells.keySet());
        Collections.shuffle(pairs);

        for (int i = 0; i < pairs.size(); ++i) {
            Pair<Integer, Integer> c = pairs.get(i);
            List<Polygon> source = current.get(c.getKey()), updatedSource = new ArrayList<>(source);
            Polygon toMove = source.get(c.getValue());
            updatedSource.remove(toMove);

            // if removing the cell creates a non connected subgraph skip this
            if (isConnectedSubgraph(updatedSource)) {
                for (int n : neighboringCells.get(c)) {
                    List<Polygon> dest = current.get(n), updatedDest = new ArrayList<>(dest);
                    updatedDest.add(toMove);

                    List<List<Polygon>> newSolution = new ArrayList<>(current);
                    newSolution.remove(source);
                    newSolution.remove(dest);
                    newSolution.add(updatedSource);
                    newSolution.add(updatedDest);

                    double newScr = heur.evaluate(newSolution);
//                    System.out.println("new score: " + newScr);
                    if (newScr > currScr)
                        return new StepResults(newSolution, i, new Date().getTime() - startTime);
                }
            }
        }
        return new StepResults(null, pairs.size(), new Date().getTime() - startTime);
    }

//    private boolean isConnected(MultiPolygon toCheck, int i, Geometry g) {
//        for (int j = 0; j < toCheck.getNumGeometries(); ++j) {
//            if (j == i) continue;
//            Geometry g2 = toCheck.getGeometryN(j);
//            if (g.intersects(g2) && !g.intersection(g2).getGeometryType().equals("Point")) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    MultiPolygon getUpdatedDest(MultiPolygon dest, Polygon toMove) {
//        List<Polygon> polygons = new LinkedList<>();
//        for (int i = 0; i < dest.getNumGeometries(); ++i) {
//            polygons.add((Polygon) dest.getGeometryN(i));
//        }
//        polygons.add(toMove);
//
//        Polygon[] arr = new Polygon[polygons.size()];
//        MultiPolygon $ = new GeometryBuilder().multiPolygon(polygons.toArray(arr));
//        return $;
//    }
//
//    private List<Polygon> getCellsWithNeighbors(List<List<Polygon>> current,
//                                                PolygonGraphGenerator iCGraph) {
//        List<Polygon> $ = new LinkedList<>();
//        Collection<Graphable> nodes = iCGraph.getGraph().getNodes();
//        for (Graphable n : nodes) {
//            int currId = n.getID();
//            Iterator neighbors = n.getRelated();
//            while (neighbors.hasNext()) {
//                Graphable neighbor = (Graphable) neighbors.next();
//                if (neighbor.getID() != currId) {
//                    $.add((Polygon) n.getObject());
//                    break;
//                }
//            }
//        }
//        return $;
//    }
}
