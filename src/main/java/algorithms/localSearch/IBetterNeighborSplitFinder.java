package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import experiments.domain.StepResults;

import java.io.IOException;
import java.util.List;

/**
 * Created by ekravi on 16/07/2017.
 */
public interface IBetterNeighborSplitFinder {
    StepResults getImprovedNeighbor(List<List<Polygon>> current) throws IOException;

    IStepEvaluator getHeur();

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
