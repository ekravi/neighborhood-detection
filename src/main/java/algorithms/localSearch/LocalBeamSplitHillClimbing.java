package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import experiments.domain.StepResults;
import javafx.util.Pair;

import java.io.IOException;
import java.util.*;

/**
 * Searches for a neighboring split improving current split by applying a probability to each improving neighbor
 * considering its improving score. The score scale is set according to a lookahead in a set of improving neighboring
 * splits
 */
public class LocalBeamSplitHillClimbing extends BaseSplitHillClimbing {
    private final int beamSize;

    public LocalBeamSplitHillClimbing(IStepEvaluator heur, int beamSize) {
        super(heur);
        this.beamSize = beamSize;
    }

    @Override
    public StepResults getImprovedNeighbor(List<List<Polygon>> current) throws IOException {
        long startTime = new Date().getTime();
        Map<Pair<Integer, Integer>, Set<Integer>> neighboringCells = getCellsWithNeighbors(current);

        double currScr = heur.evaluate(current);

        //TODO: choose from all objects of <cell_id, cs_id>-><ncs_id>
        // randomly choosing a set of cells to improve
        ArrayList<Pair<Integer, Integer>> pairs = new ArrayList<>(neighboringCells.keySet());
        Collections.shuffle(pairs);

        Map<StepResults, Double> improvingNeighbors = new HashMap<>(beamSize);

        for (int i = 0; i < pairs.size(); ++i) {
            Pair<Integer, Integer> c = pairs.get(i);
            List<Polygon> source = current.get(c.getKey()), updatedSource = new ArrayList<>(source);
            Polygon toMove = source.get(c.getValue());
            updatedSource.remove(toMove);

            // if removing the cell creates a non connected subgraph skip this
            if (isConnectedSubgraph(updatedSource)) {
                for (int n : neighboringCells.get(c)) {
                    List<Polygon> dest = current.get(n), updatedDest = new ArrayList<>(dest);
                    updatedDest.add(toMove);

                    List<List<Polygon>> newSolution = new ArrayList<>(current);
                    newSolution.remove(source);
                    newSolution.remove(dest);
                    newSolution.add(updatedSource);
                    newSolution.add(updatedDest);

                    double newScr = heur.evaluate(newSolution);
//                    System.out.println("new score: " + newScr);
                    if (newScr > currScr) {
                        improvingNeighbors.put(new StepResults(newSolution, i, new Date().getTime() - startTime),
                                newScr);
                        if (improvingNeighbors.size() == beamSize) break;
                    }
                }
            }
        }

        if (!improvingNeighbors.isEmpty()) return chooseStochastically(improvingNeighbors, currScr);
        else return new StepResults(null, pairs.size(), new Date().getTime() - startTime);

    }

    /**
     * @return solution in probability of (p(sol)-min(p(from)))/(max(p(from))-min(p(from)))
     */
    private StepResults chooseStochastically(Map<StepResults, Double> from, double currScr) {
        double min = Double.MAX_VALUE, max = -Double.MAX_VALUE;
        Random rand = new Random();

        for (Double val : from.values()) {
            max = Math.max(max, val);
        }

//        if (min == max) return from.keySet().iterator().next();

        for (StepResults candidate : from.keySet()) {
            if (rand.nextDouble() < (from.get(candidate) - currScr) / (max - currScr))
                return candidate;
        }
        // should not be called
        return null;
    }
}
