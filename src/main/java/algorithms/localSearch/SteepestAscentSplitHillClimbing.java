package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import experiments.domain.StepResults;
import javafx.util.Pair;

import java.io.IOException;
import java.util.*;

/**
 * Created by ekravi on 16/07/2017.
 */
public class SteepestAscentSplitHillClimbing extends BaseSplitHillClimbing {
    public SteepestAscentSplitHillClimbing(IStepEvaluator heur) {
        super(heur);
    }

    /**
     * Given a split, the algorithm:
     * (1) iterates over all all the bordering cells
     * (2) returns the split that most improves the current state.
     * all neighboring splits are examined
     */
    @Override
    public StepResults getImprovedNeighbor(List<List<Polygon>> current) throws IOException {
        long startTime = new Date().getTime();
        Map<Pair<Integer, Integer>, Set<Integer>> neighboringCells = getCellsWithNeighbors(current);


        // randomly choosing the cell to improve
        ArrayList<Pair<Integer, Integer>> pairs = new ArrayList<>(neighboringCells.keySet());

        double currScr = heur.evaluate(current);
        StepResults bestSolution = new StepResults(null, pairs.size(), new Date().getTime() - startTime);

        for (int i = 0; i < pairs.size(); ++i) {
            Pair<Integer, Integer> c = pairs.get(i);
            List<Polygon> source = current.get(c.getKey()), updatedSource = new ArrayList<>(source);
            Polygon toMove = source.get(c.getValue());
            updatedSource.remove(toMove);

            // if removing the cell creates a non connected subgraph skip this
            if (isConnectedSubgraph(updatedSource)) {
                for (int n : neighboringCells.get(c)) {
                    List<Polygon> dest = current.get(n), updatedDest = new ArrayList<>(dest);
                    updatedDest.add(toMove);

                    List<List<Polygon>> newSolution = new ArrayList<>(current);
                    newSolution.remove(source);
                    newSolution.remove(dest);
                    newSolution.add(updatedSource);
                    newSolution.add(updatedDest);

                    double newScr = heur.evaluate(newSolution);
                    if (newScr > currScr)
                        bestSolution = new StepResults(newSolution, i, new Date().getTime() - startTime);
                }
            }
        }
        return bestSolution;
    }
}
