package algorithms.splits.InitialSplit;

import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

/**
 * Created by ekravi on 06/07/2017.
 */
public interface IRegionSplit {
//    public Set<ConnectedSubgraph> splitToConnectedSubgraphs(ConnectedSubgraph region, int k);

    List<List<Polygon>> splitToConnectedSubgraphs(List<Polygon> region, int k);

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
