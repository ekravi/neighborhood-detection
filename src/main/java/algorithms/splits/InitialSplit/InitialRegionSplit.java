package algorithms.splits.InitialSplit;

import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;

import java.util.LinkedList;
import java.util.List;

public class InitialRegionSplit {

    public List<List<Polygon>> loadInitialSolution(List<Polygon> grid, GeometryCollection
            geomSolution, List<Polygon> intersectingCells) {
        List<List<Polygon>> $ = new LinkedList<>();
        GridService gs = new GridService();

        for (int i = 0; i < geomSolution.getNumGeometries(); ++i) {
            List<Polygon> curr = gs.getIntersectingCells(grid, geomSolution.getGeometryN(i));
            intersectingCells.addAll(curr);

            // removing intersection with previous set of polygons
            if (i > 0)
                curr.removeAll($.get(i - 1));

            $.add(i, curr);
        }
        return $;
    }
}
