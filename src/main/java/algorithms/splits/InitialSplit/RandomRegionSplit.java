package algorithms.splits.InitialSplit;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import domain.ConnectedSubgraph;
import domain.Grid.Cell;
import org.geotools.geometry.jts.GeometryBuilder;
import org.geotools.graph.build.polygon.PolygonGraphGenerator;

import java.util.*;

/**
 * Split a region to k connected subgraphs such that equal size regions have the same probability
 */
public class RandomRegionSplit implements IRegionSplit {

    /**
     * @param region expected to be a set of grid cells
     * @param k      number of regions  @return a random split of region to k connected regions
     */
    @Override
    public List<List<Polygon>> splitToConnectedSubgraphs(List<Polygon> region, int k) {
        if (region.size() < k)
            throw new IllegalArgumentException("region size smaller than k");

        Map<Integer, Set<Integer>> neighbors = initNeighborsMap(region);
        LinkedHashMap<Integer, Set<Integer>> split = new LinkedHashMap<>();
        Map<Integer, Integer> cellAssignment = new HashMap<>();
        List<Integer> centers = getCenters(region, k);

        initMappings(split, cellAssignment, centers);


        Random rand = new Random();
        int covered = k;
        while (covered < region.size()) {
            randomCellCover(neighbors, k, split, cellAssignment, rand);
            covered++;
        }

        return getSplit(region, split);
    }

    /**
     * (1) Selects k random centers
     * (2) While there exists a cell not assigned to a center:
     * (2.1) Select random center
     * (2.2) Add unassigned neighbor cell to the center
     * (3) return the generated regions
     */
    public Set<ConnectedSubgraph> splitToConnectedSubgraphs(ConnectedSubgraph region, int k) {
        if (region.size() < k)
            throw new IllegalArgumentException("region size smaller than k");

        LinkedHashMap<Cell, ConnectedSubgraph> split = new LinkedHashMap<>();
        Map<Cell, Cell> cellAssignment = new HashMap<>();

        List<Cell> lRegion = new ArrayList<>(region.getCells());
        Collections.shuffle(lRegion);
        List<Cell> centers = lRegion.subList(0, k);

        // Assign centers to themselves
        for (Cell center : centers) {
            cellAssignment.put(center, center);
        }

        // Init split
        for (Cell center : centers) {
            split.put(center, new ConnectedSubgraph(region.getGrid(), new HashSet<>(Arrays.asList(new Cell[]{center})
            )));
        }

        Random rand = new Random();

        int covered = k;
        while (covered < region.size()) {
            randomCellCover(region, k, split, cellAssignment, rand);
            covered++;
        }

        return new HashSet<>(split.values());
    }


    public List<List<Polygon>> splitToConnectedSubgraphs(List<Polygon> region, PolygonGraphGenerator iCGraph, int k) {
        if (region.size() < k)
            throw new IllegalArgumentException("region size smaller than k");

        LinkedHashMap<Integer, Set<Integer>> split = new LinkedHashMap<>();
        Map<Integer, Integer> cellAssignment = new HashMap<>();
        List<Integer> centers = getCenters(region, k);

        initMappings(split, cellAssignment, centers);


        Random rand = new Random();
        int covered = k;
        while (covered < region.size()) {
            coverRandomCell(iCGraph, k, split, cellAssignment, rand);
            covered++;
        }

        return getSplit(region, split);
    }

    private List<List<Polygon>> getSplit(List<Polygon> region, LinkedHashMap<Integer, Set<Integer>> split) {
        List<List<Polygon>> $ = new ArrayList<>(split.size());
        for (Set<Integer> cs : split.values()) {
            List<Polygon> cells = new ArrayList<>(cs.size());
            for (int i : cs) {
                cells.add(region.get(i));
            }
            $.add(cells);
        }
        return $;
    }

    private List<MultiPolygon> getMultiPolygons(List<List<Polygon>> split) {
        List<MultiPolygon> $ = new ArrayList<>(split.size());
        for (List<Polygon> region : split) {
            Polygon[] arr = new Polygon[region.size()];
            $.add(new GeometryBuilder().multiPolygon(region.toArray(arr)));
        }
        return $;
    }

    private void coverRandomCell(PolygonGraphGenerator iCGraph, int k, LinkedHashMap<Integer, Set<Integer>> split,
                                 Map<Integer, Integer> cellAssignment, Random rand) {
    }

    private void initMappings(LinkedHashMap<Integer, Set<Integer>> split, Map<Integer, Integer> cellAssignment,
                              List<Integer>
                                      centers) {
        // Assign centers to themselves
        for (int i : centers) {
            cellAssignment.put(i, i);
        }

        // Init split
        for (int i : centers) {
            Set<Integer> assigned = new HashSet<>();
            assigned.add(i);
            split.put(i, assigned);
        }
    }

    private List<Integer> getCenters(List<Polygon> region, int k) {
        List<Integer> lRegion = new ArrayList<>(region.size());
        for (int i = 0; i < region.size(); ++i) {
            lRegion.add(i);
        }
        Collections.shuffle(lRegion);
        return lRegion.subList(0, k);
    }

    /**
     * Mapping of a cell to its neighbor cells
     */
    private Map<Integer, Set<Integer>> initNeighborsMap(List<Polygon> region) {
        Map<Integer, Set<Integer>> $ = new HashMap<>();
//        for (int i = 0; i < gridMapping.length; ++i) {
//            for (int j = 0; i < gridMapping.length; ++i) {
//                if (gridMapping[i][j] == 1)
//                    $.put(new Pair<>(i, j), new HashSet<>());
//            }
//        }
//        for (Pair<Integer, Integer> cell : $.keySet()) {
//            int row = cell.getKey(), col = cell.getValue();
//            if (row > 0)
//                if (gridMapping[row - 1][col] == 1)
//                    $.get(cell).add(new Pair<>(row - 1, col));
//            if (col > 0)
//                if (gridMapping[row][col - 1] == 1)
//                    $.get(cell).add(new Pair<>(row, col - 1));
//            if (row < gridMapping.length)
//                if (gridMapping[row + 1][col] == 1)
//                    $.get(cell).add(new Pair<>(row + 1, col));
//            if (col < gridMapping[0].length)
//                if (gridMapping[row][col + 1] == 1)
//                    $.get(cell).add(new Pair<>(row, col + 1));
        for (int i = 0; i < region.size(); ++i) {
            Polygon g = region.get(i);
            for (int j = i; j < region.size(); ++j) {
                Polygon g2 = region.get(j);
                if (g.intersects(g2) && !g.intersection(g2).getGeometryType().equals("Point")) {
                    if (!$.containsKey(i))
                        $.put(i, new HashSet<>());
                    if (!$.containsKey(j))
                        $.put(j, new HashSet<>());
                    $.get(i).add(j);
                    $.get(j).add(i);
                }
            }
        }
        return $;
    }

    /**
     * Randomly covers a cell in the input region, by choosing a random center, then choosing a random neighbor of
     * this center
     */
    void randomCellCover(ConnectedSubgraph region, int k, LinkedHashMap<Cell, ConnectedSubgraph> split,
                         Map<Cell, Cell> cellAssignment, Random rand) {
        Set<Cell> neighbors = Collections.EMPTY_SET;
        Cell randCenter, randNeighbor;
        // until finding a center with neighbors
        do {
            randCenter = new ArrayList<>(split.keySet()).get(rand.nextInt(k));
            neighbors = region.intersects(split.get(randCenter).getNeighbors());
        } while (neighbors.isEmpty());
        randNeighbor = new ArrayList<Cell>(neighbors).get(rand.nextInt(neighbors.size()));

        split.put(randCenter, split.get(randCenter).extend(randNeighbor));
        cellAssignment.put(randNeighbor, randCenter);
    }

    /**
     * Randomly covers a cell in the input region, by choosing a random center, then choosing a random neighbor of
     * this center
     */
    void randomCellCover(Map<Integer, Set<Integer>> neighbors, int k,
                         LinkedHashMap<Integer,
                                 Set<Integer>> split, Map<Integer, Integer> cellAssignment, Random rand) {
        int randCenter, randNeighbor;
        Set<Integer> unassignedNeighbors = Collections.EMPTY_SET;
        // until finding a center with neighbors
        do {
            randCenter = new ArrayList<>(split.keySet()).get(rand.nextInt(k));
            unassignedNeighbors = getUnassignedNeighbors(split.get(randCenter), neighbors, cellAssignment);
        } while (unassignedNeighbors.isEmpty());
        randNeighbor = new ArrayList<Integer>(unassignedNeighbors).get(rand.nextInt(unassignedNeighbors.size()));

        split.get(randCenter).add(randNeighbor);
        cellAssignment.put(randNeighbor, randCenter);
    }

    /**
     * @return neighbor cells of current connected subgraph, that are not assigned to other center
     */
    private Set<Integer> getUnassignedNeighbors(Set<Integer> connectedSubgraph, Map<Integer, Set<Integer>>
            neighborsMap, Map<Integer, Integer> cellAssignment) {
        Set<Integer> $ = new HashSet<>();
        for (int i : connectedSubgraph) {
            $.addAll(neighborsMap.get(i));
        }
        $.removeAll(connectedSubgraph);
        for (int i : new HashSet<>($)) {
            if (cellAssignment.containsKey(i))
                $.remove(i);
        }
        return $;
    }
}
