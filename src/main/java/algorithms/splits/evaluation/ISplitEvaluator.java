package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.Polygon;
import domain.Message;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created by ekravi on 16/07/2017.
 */
public interface ISplitEvaluator {
    /**
     * evaluate a split
     */
//    double evaluate(List<List<Polygon>> toEvaluate) throws IOException;

    /**
     * evaluate a split over a given set of test messages
     */
    void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException;


    // TODO: change test to conatin a map from multipolygon to Set<Message>

    /**
     * evaluate a split over a given set of test messages
     */
    double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException;

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
