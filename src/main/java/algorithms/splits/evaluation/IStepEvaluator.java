package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.Polygon;
import domain.Message;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface IStepEvaluator {
    /**
     * evaluate a split
     */
    double evaluate(List<List<Polygon>> toEvaluate) throws IOException;

    /**
     * init datastructure by an initial set of polygons
     */
    void init(Set<Polygon> roi, Set<Message> initSet);
}
