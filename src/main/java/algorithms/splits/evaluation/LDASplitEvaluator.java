package algorithms.splits.evaluation;

import algorithms.topics.BaseTopicCalculator;
import cc.mallet.topics.ParallelTopicModel;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.SpatioTextualAuxilary;
import domain.Message;
import experiments.settings.DefaultSettings;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Created by ekravi on 16/07/2017.
 */
public class LDASplitEvaluator implements ISplitEvaluator, IStepEvaluator {

    //    private final Set<String> stopwords;
    private int numTopics;
    private final int numIterations;
    private Set<Message> msgs;

    private ParallelTopicModel model = null;
    BaseTopicCalculator tc;

    private SpatioTextualAuxilary aux = new SpatioTextualAuxilary();

    public LDASplitEvaluator(Set<String> stopwords) {
        this(stopwords, DefaultSettings.LDA_NUM_TOPICS);
    }

    public LDASplitEvaluator(Set<String> stopwords, int numTopics) {
        this(stopwords, numTopics, DefaultSettings.SINGLE_LDA_NUM_ITERATIONS);
    }

    public LDASplitEvaluator(Set<String> stopwords, int numTopics, int numIterations) {
//        this.stopwords = stopwords;
        this.numTopics = numTopics;
        this.numIterations = numIterations;
        this.tc = new BaseTopicCalculator(stopwords);
    }

    /**
     * Evaluates the accuracy of the given split by first, training a model for the given split, where each region is
     * considered as a single document composed from a concatination of all the messages contained within the region.
     * Second, evaluates the model accuracy for the set of documents
     */
    @Override
    public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
        List<String> docs = aux.extractRegionString(toEvaluate, msgs);
        model = tc.trainStandardModel(docs, numTopics);

        return tc.evaluate(model, docs);
    }

    /**
     * Trains a model for the given split, where each region is considered as a single document composed from a
     * concatination of all the messages contained within the region.
     */
    @Override
    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
        List<String> docs = aux.extractRegionString(toEvaluate, trainSet);
        model = tc.trainStandardModel(docs, numTopics);
    }

    /**
     * Tests the model accuracy for the given set of test messages
     */
    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        if (model == null)
            throw new IllegalStateException("train first");
        List<String> docs = aux.extractRegionString(toEvaluate, testSet);
        return tc.evaluate(model, docs);
    }

    @Override
    public void init(Set<Polygon> roi, Set<Message> initSet) {
        this.msgs = initSet;
    }
}
