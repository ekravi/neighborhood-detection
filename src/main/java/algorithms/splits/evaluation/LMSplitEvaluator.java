package algorithms.splits.evaluation;

import algorithms.distance.IDistanceMeasure;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import ir.EDLM;
import ir.LM;
import ir.distance.KullbackLeiblerDivergenceCalculator;
import org.geotools.geometry.jts.GeometryBuilder;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.util.*;

/**
 * Created by ekravi on 19/07/2017.
 */
public class LMSplitEvaluator implements ISplitEvaluator, IStepEvaluator, IDistanceMeasure {
    protected final int lmOrder;
    protected Map<Polygon, Set<List<String>>> msgsAssignment = null;
    protected final Map<Polygon, LM> lms = new HashMap<>();

    /**
     * Assuming the set of messages is contained within the evaluated split
     */
    public LMSplitEvaluator(int lmOrder) {
        this.lmOrder = lmOrder;
    }

    @Override
    public void init(Set<Polygon> cells, Set<Message> msgs) {
        msgsAssignment = mapMsgs(cells, msgs);
        for (Polygon p : msgsAssignment.keySet()) {
            Set<List<String>> lmInput = new HashSet<>();
            for (List<String> msg : msgsAssignment.get(p)) {
                lmInput.add(msg);
            }
            LM lm = new EDLM(lmOrder).train(lmInput).generate();
            if (lm == null)
                throw new IllegalArgumentException("couldn't generate lm");
            lms.put(p, lm);
        }
    }

    @Override
    public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
        if (msgsAssignment == null)
            throw new IllegalArgumentException("call init or train");
        double $ = 0;

        for (List<Polygon> polygons : toEvaluate) {
//            Set<Polygon> polygons = getPolygons(mp);
            LM lm = getAggregatedLM(new HashSet<>(polygons));
            Set<List<String>> mpMsgs = aggregateMsgs(polygons);
            for (List<String> m : mpMsgs) {
                $ += lm.getLogProb(m);
            }
        }
        return $;
    }

    @Override
    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
        Set<Polygon> polygons = new HashSet<>();
        for (List<Polygon> ps : toEvaluate) {
            polygons.addAll(ps);
        }

        init(polygons, trainSet);
    }

    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        if (msgsAssignment == null)
            throw new IllegalArgumentException("call init or train");
        double $ = 0;
        Map<MultiPolygon, Set<List<String>>> map = mapMsgs(toEvaluate, testSet);

        for (List<Polygon> polygons : toEvaluate) {
            LM lm = getAggregatedLM(new HashSet<>(polygons));
            if (lm == null) continue;
            Polygon[] pArr = new Polygon[polygons.size()];
            Set<List<String>> mpMsgs = map.get(new GeometryBuilder().multiPolygon(polygons.toArray(pArr)));

            for (List<String> m : mpMsgs) {
                $ += lm.getLogProb(m);
            }
        }
        return $;
    }

    Map<MultiPolygon, Set<List<String>>> mapMsgs(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws
            IOException {
        Map<MultiPolygon, Set<List<String>>> map = new LinkedHashMap<>();

        for (List<Polygon> lp : toEvaluate) {
            map.put(new GridService().getMultiPolygon(lp), new HashSet<>());
        }
        for (Message m : testSet) {
            for (MultiPolygon mp : map.keySet()) {
                if (mp.contains(m.location))
                    map.get(mp).add(Arrays.asList(m.getMsg().split("\\s+")));
            }
        }
        return map;
    }

    public String explain(List<List<Polygon>> toEvaluate) {
        SeparatorStringBuilder lines = new SeparatorStringBuilder('\n');
        lines.append("Aggregated language model");
        for (List<Polygon> polygons : toEvaluate) {
//            Set<Polygon> polygons = getPolygons(mp);
            LM lm = getAggregatedLM(new HashSet<>(polygons));
            lines.append(new SeparatorStringBuilder('\t').append("Perplexity: ").append(lm.getPerplexity
                    (aggregateMsgs(polygons))));
            for (List<Polygon> other : toEvaluate) {
                if (!other.equals(polygons)) {
                    LM otherLM = getAggregatedLM(new HashSet<>(other));
                    lines.append("Distance: " + Double.toString(new KullbackLeiblerDivergenceCalculator().getDistance
                            (lm,
                                    otherLM)));
                    lines.append("Top Distinctive Terms: ").append(new KullbackLeiblerDivergenceCalculator()
                            .getTopKEntries(lm,
                                    otherLM, 10).toString());
                    lines.append("");// empty line
                }
            }
        }
        return lines.toString();
    }

    private Set<List<String>> aggregateMsgs(List<Polygon> polygons) {
        Set<List<String>> $ = new HashSet<>();
        for (Polygon p : polygons) {
            Set<List<String>> msgs = msgsAssignment.get(p);
            if (msgs != null)
                $.addAll(msgs);
        }
        return $;
    }


    /**
     * Generate a map associating each polygon in the ROI to the messages contained within it.
     */
    private Map<Polygon, Set<List<String>>> mapMsgs(Set<Polygon> toInit, Set<Message> toMap) {
        Map<Polygon, Set<List<String>>> $ = new HashMap<>();
        for (Message m : toMap) {
            for (Polygon p : toInit) {
                if (m.location.within(p)) {
                    if (!$.containsKey(p))
                        $.put(p, new HashSet<>());
                    $.get(p).add(new ArrayList<>(Arrays.asList(m.getMsg().split("\\s+"))));
                }
            }
        }
        return $;
    }

//    private Set<Polygon> getPolygons(MultiPolygon mp) {
//        Set<Polygon> polygons = new HashSet<>();
//        for (int i = 0; i < mp.getNumGeometries(); ++i) {
//            polygons.add((Polygon) mp.getGeometryN(i));
//        }
//        return polygons;
//    }

    protected LM getAggregatedLM(Set<Polygon> polygons) {
        Set<LM> $ = new HashSet<>();
        for (Polygon p : polygons) {
            if (!lms.containsKey(p)) {
//                throw new IllegalArgumentException("no message was sent from polygon " + p);
                continue;
            }
            $.add(lms.get(p));
        }
        if ($.isEmpty()) return null;
        return new EDLM().aggregate($);
    }

    public List<LM> getAggregatedLMs(List<List<Polygon>> solution) {
        List<LM> $ = new ArrayList<>(solution.size());
        for (List<Polygon> split : solution) {
            LM lm = getAggregatedLM(new HashSet<>(split));
            if (lm != null)
                $.add(lm);
        }
        return $;
    }

    @Override
    public double getDistance(Set<Polygon> from, Set<Polygon> to) {
        if (msgsAssignment == null)
            throw new IllegalArgumentException("call init");
        LM fromLM = getAggregatedLM(from),
                toLM = getAggregatedLM(to);
        if (fromLM == null || toLM == null)
            return -Double.MAX_VALUE;

        return new KullbackLeiblerDivergenceCalculator().getDistance(fromLM, toLM);
    }

    @Override
    public double getDistance(Polygon from, Polygon to) {
        if (msgsAssignment == null)
            throw new IllegalArgumentException("call init");
        HashSet<Polygon> fromHS = new HashSet<>(), toHS = new HashSet<>();
        fromHS.add(from);
        toHS.add(to);
        LM fromLM = getAggregatedLM(fromHS),
                toLM = getAggregatedLM(toHS);

        return new KullbackLeiblerDivergenceCalculator().getDistance(fromLM, toLM);
    }
}