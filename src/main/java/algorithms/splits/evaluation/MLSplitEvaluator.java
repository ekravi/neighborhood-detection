package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import org.apache.commons.io.IOUtils;
import service.SeparatorStringBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class MLSplitEvaluator implements ISplitEvaluator {

    private final Set<Message> trainMsgs;
    private final Set<Message> testMsgs;
    private final ColumnDataClassifier cdc;

    /**
     * Should not use the given messages for the split process
     *
     * @param testMsgs
     */
    public MLSplitEvaluator(Set<Message> trainMsgs, Set<Message> testMsgs) {
        this.trainMsgs = Collections.unmodifiableSet(trainMsgs);
        this.testMsgs = Collections.unmodifiableSet(testMsgs);
        this.cdc = new ColumnDataClassifier(getSettings());
    }

    /**
     * trains a classifier over the given split
     */
//    @Override
//    public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
//        // Generate train and test files as input to classifier
//        List<MultiPolygon> split = getMPs(toEvaluate);
//        String trainFile = getTmpFile(split, ".train", trainMsgs);
//        String testFile = getTmpFile(split, ".test", testMsgs);
//
//        cdc.trainClassifier(trainFile);
//        Pair<Double, Double> performance = cdc.testClassifier(testFile);
//        System.out.printf("Accuracy: %.3f; macro-F1: %.3f%n", performance.first(), performance.second());
//
//        return performance.first();
//
//
//    }
    @Override
    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
        String trainFile = getTmpFile(getMPs(toEvaluate), ".train", trainSet);
        cdc.trainClassifier(trainFile);
    }

    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        String testFile = getTmpFile(getMPs(toEvaluate), ".test", testMsgs);
        //        System.out.printf("Accuracy: %.3f; macro-F1: %.3f%n", performance.first(), performance.second());
        return cdc.testClassifier(testFile).first();
    }

    private String getTmpFile(List<MultiPolygon> split, String suffix, Set<Message> trainMsgs) throws IOException {
        Path path = Files.createTempFile("msgsAssgnmnt", suffix);
        appendMessages(split, path, trainMsgs);

        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        File file = path.toFile();
        file.deleteOnExit();
        return file.getAbsolutePath();
    }

    private void appendMessages(List<MultiPolygon> split, Path path, Set<Message> toAppend) throws
            IOException {
        int i = 0;
        for (Message m : toAppend) {
            for (MultiPolygon mp : split) {
                if (mp.contains(m.location)) {
                    ++i;
                    // writing sample data
                    String s = new SeparatorStringBuilder('\t').append(split.indexOf(mp)).append(m.getMsg()).append
                            ('\n')
                            .toString();
                    Files.write(path, s.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
                }
            }
        }
    }

    private List<MultiPolygon> getMPs(List<List<Polygon>> toEvaluate) {
        List<MultiPolygon> $ = new ArrayList<>(toEvaluate.size());
        GridService gs = new GridService();
        for (List<Polygon> polygons : toEvaluate) {
            try {
                $.add(gs.getMultiPolygon(polygons));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return $;
    }

    public Properties getSettings() {
        Properties $ = new Properties();
        IOUtils.toInputStream(
                // features
                "useClassFeature=false\n" + // Include a feature for the class (as a class marginal)
                        "1.useNGrams=true\n" + // Make features from letter n-grams
                        "1.usePrefixSuffixNGrams=true\n" + // Make features from prefix and suffix substrings
                        "1.maxNGramLeng=4\n" + // n-grams above this size will not be used in the model
                        "1.minNGramLeng=1\n" + // n-grams below this size will not be used in the model
                        "1.binnedLengths=10,20,30\n" + // The feature represents the length of the String in this
                        // column.

                        // Printing
                        "printClassifierParam=200\n" + // A parameter to the printing style

                        // Mapping
                        "goldAnswerColumn=0\n" + // Column number that contains the correct class for each data item
                        "displayedColumn=1\n" + // Column number that will be printed out to stdout in the output

                        //Optimization
                        "intern=true\n" + // intern all of the (final) feature names
                        "sigma=3\n" + // smoothing (i.e., regularization) methods, bigger number is more smoothing
                        "useQN=true\n" + // Use Quasi-Newton optimization
                        "QNsize=15\n" + // Number of previous iterations of Quasi-Newton to store
                        "tolerance=1e-4\n" + // Convergence tolerance in parameter optimization

                        // Training input
                        "trainFile=./examples/cheeseDisease.train\n" +
                        "testFile=./examples/cheeseDisease.test\n", Charset.defaultCharset());

        return $;
    }

}
