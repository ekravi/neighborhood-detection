package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import ir.LM;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PerplexityEvaluator extends LMSplitEvaluator {

    /**
     * Assuming the set of messages is contained within the evaluated split
     *
     * @param lmOrder
     */
    public PerplexityEvaluator(int lmOrder) {
        super(lmOrder);
    }

    @Override
    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
        super.train(toEvaluate, trainSet);
    }

    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        if (msgsAssignment == null)
            throw new IllegalArgumentException("call init or train");
        Map<MultiPolygon, Set<List<String>>> mpMap = mapMsgs(toEvaluate, testSet);

        double $ = 0.0;
        int i = 0;
        for (List<Polygon> polygons : toEvaluate) {
            LM lm = getAggregatedLM(new HashSet<>(polygons));
            if (lm == null) continue;

            MultiPolygon mp = new GridService().getMultiPolygon(polygons);
            $ += lm.getPerplexity(mpMap.get(mp));
            ++i;
        }
        return $ / i;
    }
}