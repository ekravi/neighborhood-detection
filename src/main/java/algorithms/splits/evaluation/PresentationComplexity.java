package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import org.geotools.filter.AreaFunction;
import org.geotools.geometry.jts.GeometryBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class PresentationComplexity implements ISplitEvaluator {

    @Override
    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
        // do nothing
    }

    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        GridService gs = new GridService();
        Geometry[] split = new Geometry[toEvaluate.size()];
        for (int i = 0; i < toEvaluate.size(); ++i) {
            split[i] = gs.getMultiPolygon(toEvaluate.get(i)).union();
        }

        GeometryCollection c = new GeometryBuilder().geometryCollection(split);

        // converting result in degrees to distance in meters (for short distances)
        return (new AreaFunction().getPerimeter(c) * (Math.PI / 180) * 6378.137);
    }
}
