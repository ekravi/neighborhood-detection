package algorithms.splits.evaluation;

import algorithms.distance.IDistanceMeasure;
import algorithms.topics.BaseTopicCalculator;
import cc.mallet.topics.ParallelTopicModel;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.settings.DefaultSettings;
import ir.distance.KullbackLeiblerDivergenceCalculator;
import org.geotools.geometry.jts.GeometryBuilder;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.util.*;

/**
 * Running LDA one time for each cell and then
 */
public class SingleLDASplitEvaluator implements IDistanceMeasure, ISplitEvaluator, IStepEvaluator {
    private final int numTopics, numIterations;
    ParallelTopicModel model = null;
    BaseTopicCalculator tc;
    private final Set<String> stopwords;
    private KullbackLeiblerDivergenceCalculator klCalc = new KullbackLeiblerDivergenceCalculator();

    Map<Polygon, String> msgsAssignment;
    // topic distribution for for each polygon
    Map<Polygon, Set<Message>> polygonMsgs;
    Map<Polygon, double[]> topicsPerCell = new HashMap<Polygon, double[]>();
    private GridService gs = new GridService();


    public Map<Polygon, double[]> getTopicPerCell() {
        return Collections.unmodifiableMap(topicsPerCell);
    }

    /**
     * @return a map for each topic describing the top terms and their appearance count
     */
    public List<Map<String, Integer>> getTopTermsPerTopic(int k) {
        if (model == null)
            throw new IllegalStateException("call init first");
        return tc.getTopWordsPerTopic(model, new ArrayList<String>(msgsAssignment.values()), k);
    }

    public SingleLDASplitEvaluator(Set<String> stopwords) {
        this(stopwords, DefaultSettings.LDA_NUM_TOPICS);
    }

    public SingleLDASplitEvaluator(Set<String> stopwords, int numTopics) {
        this(stopwords, numTopics, DefaultSettings.SINGLE_LDA_NUM_ITERATIONS);
    }

    public SingleLDASplitEvaluator(Set<String> stopwords, int numTopics, int numIterations) {
        this.stopwords = stopwords;
        this.numTopics = numTopics;
        this.numIterations = numIterations;
    }


    @Override
    public void init(Set<Polygon> roi, Set<Message> initSet) {
        if (roi == null || initSet == null)
            throw new IllegalArgumentException();

        // for each polygon the set of messages are aggregated to a single String
        polygonMsgs = mapMsgs(roi, initSet);
        tc = new BaseTopicCalculator(stopwords);

        msgsAssignment = extractStrings(polygonMsgs);
        try {
            model = tc.trainStandardModel(new ArrayList<>(msgsAssignment.values()), numTopics, numIterations);
        } catch (IOException e) {
            throw new RuntimeException("can't train model");
        }

        for (Polygon p : msgsAssignment.keySet()) {
            topicsPerCell.put(p, tc.getInstanceTopicsDistribution(model, msgsAssignment.get(p)));
        }
    }

    // Maps each message to the polygon contains it
    private Map<Polygon, Set<Message>> mapMsgs(Set<Polygon> toInit, Set<Message> toMap) {
        Map<Polygon, Set<Message>> $ = new HashMap<>();
        for (Message m : toMap) {
            for (Polygon p : toInit) {
                if (m.location.within(p)) {
                    if (!$.containsKey(p))
                        $.put(p, new HashSet<>());
                    $.get(p).add(m);
                }
            }
        }
        return $;
    }

    /**
     * Aggregates the content of the messges for each polygon
     */
    private Map<Polygon, String> extractStrings(Map<Polygon, Set<Message>> from) {
        Map<Polygon, String> $ = new HashMap<>();
        for (Polygon p : from.keySet()) {
            SeparatorStringBuilder ssb = new SeparatorStringBuilder(' ');
            for (Message m : from.get(p)) {
                ssb.appendAll(Arrays.asList(m.getMsg().split("\\s+")));
            }
            $.put(p, ssb.toString());
        }
        return $;
    }

    /**
     * Distance between two sets of polygons is defined to be the KLDivergence between their distributions over the
     * topics.
     * A distribution over the topics of a set of polygons is defined as the average distribution over all the polygons
     * in the set.
     */
    @Override
    public double getDistance(Set<Polygon> from, Set<Polygon> to) {
        if (model == null)
            throw new RuntimeException("call init first");
        double[] fromDist = getAverageDist(from), toDist = getAverageDist(to);
        return klCalc.getDistance(fromDist, toDist);
    }

    /**
     * Distance between two polygons is defined to be the KLDivergence between their distributions over the topics.
     */
    @Override
    public double getDistance(Polygon from, Polygon to) {
        if (model == null)
            throw new RuntimeException("call init first");
        if (!topicsPerCell.containsKey(from) || !topicsPerCell.containsKey(to)) throw new
                IllegalArgumentException("empty statistics for polygons");

        double[] fromDist = topicsPerCell.get(from), toDist = topicsPerCell.get(to);
        return klCalc.getDistance(fromDist, toDist);
    }

    double[] getAverageDist(Set<Polygon> from) {
        double[] $ = new double[topicsPerCell.values().iterator().next().length];
        int numMsgs = 0;
        for (Polygon p : from) {
            if (topicsPerCell.containsKey(p)) {
                double[] dist = topicsPerCell.get(p);
                int weight = polygonMsgs.get(p).size();
                for (int i = 0; i < dist.length; ++i) {
                    $[i] += dist[i] * weight;
                    numMsgs += weight;
                }
//            } else {
//                System.err.println("can't find polygon " + new GeometryJSON().toString(p));
            }
        }

        for (int i = 0; i < $.length; ++i) {
            $[i] /= numMsgs;
        }
        return $;
    }

    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
        Set<Polygon> roi = new HashSet<>();
        for (List<Polygon> region : toEvaluate) {
            roi.addAll(region);
        }
        init(roi, trainSet);
    }


    /**
     * not training the LDA model again, but evaluating the topic distribution for the given set of messages.
     * then, calculating the KL divergence between every neighboring regions
     */
    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        Map<Polygon, Set<Message>> polygonSetMap = mapMsgs(polygonMsgs.keySet(), testSet);
        Map<Polygon, String> polygonStringMap = extractStrings(polygonSetMap);

        Map<Polygon, double[]> testTopicsPerCell = new HashMap<Polygon, double[]>();
        for (Polygon p : polygonStringMap.keySet()) {
            testTopicsPerCell.put(p, tc.getInstanceTopicsDistribution(model, polygonStringMap.get(p)));
        }

        Map<MultiPolygon, double[]> topicDist = getAverageDist(toEvaluate);
        double $ = 0;

        int numIntersections = 0;
        for (MultiPolygon from : topicDist.keySet()) {
            for (MultiPolygon to : topicDist.keySet()) {
                if (from.intersects(to)) {
                    $ += getDistance(gs.getPolygons(from), gs.getPolygons(to));
                    ++numIntersections;
                }
            }
        }
        return $ / numIntersections;
    }

    private List<String> getDocs(List<List<Polygon>> toEvaluate, Set<Message> toConvert) {
        Map<MultiPolygon, SeparatorStringBuilder> map = new HashMap<>();
        for (int i = 0; i < toEvaluate.size(); ++i) {
            List<Polygon> polygons = toEvaluate.get(i);
            Polygon[] pArr = new Polygon[polygons.size()];
            map.put(new GeometryBuilder().multiPolygon(polygons.toArray(pArr)), new SeparatorStringBuilder('\n'));
        }

        for (Message m : toConvert) {
            for (MultiPolygon mp : map.keySet()) {
                if (m.location.within(mp)) {
                    map.get(mp).append(m.getMsg());
                }
            }
        }
        List<String> docs = new ArrayList<>(map.size());
        for (SeparatorStringBuilder ssb : map.values()) {
            docs.add(ssb.toString());
        }
        return docs;
    }


    /**
     * We previously obtain the topic distribution of every cell in the grid.
     * Given a set of regions R aggregating different cells in the grid, we denote, for r \in R, the topic
     * distribution of r to be a weighted average over the cells in r.
     * The LDA score is defined to be the sum of KL divergence between the probabilities of neighboring cells
     */
    @Override
    public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
        if (model == null) {
            throw new IllegalStateException("call init first");
        }
        Map<MultiPolygon, double[]> topicDist = getAverageDist(toEvaluate);
        double $ = 0;

        int numIntersections = 0;
        for (MultiPolygon from : topicDist.keySet()) {
            for (MultiPolygon to : topicDist.keySet()) {
                if (from.intersects(to)) {
                    $ += getDistance(gs.getPolygons(from), gs.getPolygons(to));
                    ++numIntersections;
                }
            }
        }
        return $ / numIntersections;
    }

    private Map<MultiPolygon, double[]> getAverageDist(List<List<Polygon>> toEvaluate) throws IOException {
        Map<MultiPolygon, double[]> $ = new HashMap<>();
        for (List<Polygon> region : toEvaluate) {
            $.put(gs.getMultiPolygon(region), getAverageDist(new HashSet<>(region)));
        }
        return $;
    }
}
