package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.settings.DefaultSettings;
import service.SeparatorStringBuilder;
import statistics.Histogram;

import java.io.IOException;
import java.util.*;

public class TopTermsSplitEvaluator implements ISplitEvaluator {
    //    protected Map<Polygon, Set<List<String>>> msgsAssignment = null;
    protected Histogram<String> generalTermHist = new Histogram<>();

    @Override
    public void train(List<List<Polygon>> toEvaluate, Set<Message> trainSet) throws IOException {
//        Set<Polygon> polygons = new HashSet<>();
//        for (List<Polygon> ps : toEvaluate) {
//            polygons.addAll(ps);
//        }
//        msgsAssignment = mapMsgs(polygons, trainSet);
//        for (Set<List<String>> docs : msgsAssignment.values()) {
//            for (List<String> doc : docs) {
//                for (String term : doc) {
//                    generalTermHist.inc(term);
//                }
//            }
//        }

        for (Message m : trainSet) {
            for (String s : m.getMsg().split("\\s+")) {
                generalTermHist.inc(s);
            }
        }
    }

    @Override
    public double test(List<List<Polygon>> toEvaluate, Set<Message> testSet) throws IOException {
        double $ = 0;

        Map<MultiPolygon, Histogram<String>> evaluationMap = getEvalMap(toEvaluate, testSet);
        int cnt = 0;
        for (Histogram<String> regionMapping : evaluationMap.values()) {
            Histogram<String> topK = regionMapping.getTopK(DefaultSettings.IR_K_TERMS);
            for (String t : topK.keySet()) {
                if (generalTermHist.get(t) == null) {
                    $ += 1;
                    System.err.println("term '" + t + "' was not found in train set");
                    continue;
                }
                double occurrencesInRegion = topK.get(t).getValue();
                double generalOccurrences = generalTermHist.get(t).getValue();
                $ += occurrencesInRegion / generalOccurrences;

                cnt++;
                System.out.println(new SeparatorStringBuilder('\t').append(t).append(occurrencesInRegion).append
                        (generalOccurrences).toString());
            }
        }
        return $ / cnt;


//        for (List<Polygon> region : toEvaluate) {
//            Histogram<String> rHist = getRegionHistogram(region);
//            Histogram<String> topK = rHist.getTopK(DefaultSettings.IR_K_TERMS);
//            for (String t : topK.keySet()) {
//                double occurrencesInRegion = topK.get(t).getValue();
//                double generalOccurrences = generalTermHist.get(t).getValue();
//                $ += occurrencesInRegion / generalOccurrences;
//                System.out.println(new SeparatorStringBuilder('\t').append(t).append(occurrencesInRegion).append
//                        (generalOccurrences).toString());
//            }
//        }
    }

    private Map<MultiPolygon, Histogram<String>> getEvalMap(List<List<Polygon>> toEvaluate, Set<Message> testSet)
            throws IOException {
        Map<MultiPolygon, Histogram<String>> $ = new HashMap<>();
        for (List<Polygon> cs : toEvaluate) {
            $.put(new GridService().getMultiPolygon(cs), new Histogram<>());
        }

        for (Message m : testSet) {
            for (MultiPolygon mp : $.keySet()) {
                if (mp.contains(m.location)) {
                    Histogram<String> hist = $.get(mp);
                    for (String s : m.getMsg().split("\\s+")) {
                        hist.inc(s);
                    }
                }
            }
        }
        return $;
    }

//    private Histogram<String> getRegionHistogram(List<Polygon> region) {
//        Histogram<String> $ = new Histogram<>();
//        for (Polygon p : region) {
//            for (List<String> doc : msgsAssignment.get(p)) {
//                for (String term : doc) {
//                    $.inc(term);
//                }
//            }
//        }
//        return $;
//    }


    private Map<Polygon, Set<List<String>>> mapMsgs(Set<Polygon> toInit, Set<Message> toMap) {
        Map<Polygon, Set<List<String>>> $ = new HashMap<>();
        for (Message m : toMap) {
            for (Polygon p : toInit) {
                if (m.location.within(p)) {
                    if (!$.containsKey(p))
                        $.put(p, new HashSet<>());
                    $.get(p).add(new ArrayList<>(Arrays.asList(m.getMsg().split("\\s+"))));
                }
            }
        }
        return $;
    }
}
