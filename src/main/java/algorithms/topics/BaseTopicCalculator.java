package algorithms.topics;

import cc.mallet.pipe.*;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import experiments.settings.DefaultSettings;
import org.apache.commons.io.IOUtils;
import service.MapUtils;
import service.SeparatorStringBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by ekravi on 26/06/2017.
 */
public class BaseTopicCalculator {
    private final String[] stopwords;

    public BaseTopicCalculator(Path confDir) throws IOException {
        this.stopwords = getStopWordsList(confDir);
    }

    public String[] getStopWordsList(Path confDir) throws IOException {
        return IOUtils.toString(new FileInputStream(confDir.resolve("stopwords.txt").toFile()), Charset
                .defaultCharset()).split("\\s+");
    }

    public BaseTopicCalculator(Set<String> stopwords) {
        String[] arr = new String[stopwords.size()];
        this.stopwords = stopwords.toArray(arr);
    }


    /**
     * @return the probability of the most probable topic for the given document
     */
    public double getInstanceProbability(ParallelTopicModel model, String doc) {
        return getInstanceTopicsDistribution(model, doc)[0];
    }

    /**
     * @return probability a probability distribution of all the topics for the given document
     */
    public double[] getInstanceTopicsDistribution(ParallelTopicModel model, String doc) {
        return getInstanceTopicsDistribution(model, doc, DefaultSettings.LDA_NUM_ITERATIONS);
    }

    public double[] getInstanceTopicsDistribution(ParallelTopicModel model, String doc, int numIterations) {
        InstanceList testing = new InstanceList(new SerialPipes(getPipes()));
        testing.addThruPipe(new Instance(doc, null, "test instance", null));

        return model.getInferencer().getSampledDistribution(testing.get(0), numIterations, 1, 5);
    }

    /**
     * Evaluate the log probability of a set of documents using a trained model
     *
     * @return sum of log probability of the documents
     * @see <a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.149.771&rep=rep1&type=pdf">this
     * paper</a> for more information
     */
    public double evaluate(ParallelTopicModel model, List<String> docs) throws IOException {
        InstanceList testing = getInstances(docs, getPipes());
        return model.getProbEstimator().evaluateLeftToRight(testing, 10, false, null);
    }

    /**
     * @return the $k$ most probable words for each topic for the given set of documents
     */
    public List<Map<String, Integer>> getTopWordsPerTopic(ParallelTopicModel model, List<String> docs, int k) {
        ArrayList<TreeSet<IDSorter>> sortedWords = model.getSortedWords();
        double[] topicProbabilities = model.getTopicProbabilities(0);
        Alphabet dataAlphabet = getInstances(docs, getPipes()).getDataAlphabet();
        List<Map<String, Integer>> $ = new ArrayList<>(model.getNumTopics());

        for (int topicIdx = 0; topicIdx < model.getNumTopics(); topicIdx++) {
            Iterator<IDSorter> iterator = sortedWords.get(topicIdx).iterator();
            Map<String, Integer> topTerms = new TreeMap<>();
            int rank = 0;
            while (iterator.hasNext() && rank < k) {
                IDSorter idCountPair = iterator.next();
                topTerms.put(dataAlphabet.lookupObject(idCountPair.getID()).toString(), (int) idCountPair.getWeight());
                rank++;
            }
            topTerms = MapUtils.sortByValueReversed(topTerms);
            $.add(topTerms);
        }
        return $;
    }

    public ParallelTopicModel trainStandardModel(List<String> docs) throws
            IOException {
        return trainStandardModel(docs, DefaultSettings.LDA_NUM_TOPICS);
    }

    public ParallelTopicModel trainStandardModel(List<String> docs, int numTopics) throws
            IOException {
        return trainStandardModel(getInstances(docs, getPipes()), numTopics, DefaultSettings
                .LDA_NUM_ITERATIONS);
    }

    public ParallelTopicModel trainStandardModel(List<String> docs, int numTopics, int numIterations) throws
            IOException {
        return trainStandardModel(getInstances(docs, getPipes()), numTopics, numIterations);
    }

    /**
     * @return trained model for the number of topics and the given set of instances (documents), the accuracy is
     * determined by the number of iterations
     */
    protected ParallelTopicModel trainStandardModel(InstanceList instances, int numTopics, int numIterations) throws
            IOException {
        // Create a model with alpha_t = 0.01, beta_w = 0.01
        //  Note that the first parameter is passed as the sum over topics, while
        //  the second is the parameter for a single dimension of the Dirichlet prior.
        ParallelTopicModel model = new ParallelTopicModel(numTopics, DefaultSettings.LDA_ALPHA_PRIOR * numTopics,
                DefaultSettings.LDA_BETA_PRIOR);
        model.addInstances(instances);

        // Use two parallel samplers, which each look at one half the corpus and combine
        //  statistics after every iteration.
        model.setNumThreads(DefaultSettings.LDA_NUM_THREADS);

        // Run the model for 50 iterations and stop (this is for testing only,
        //  for real applications, use 1000 to 2000 iterations)
        model.setNumIterations(numIterations); // default - 1000
        model.estimate();
        return model;
    }

    //-----------Auxilary----------------

    /**
     * @return list of processing pipes
     */
    protected ArrayList<Pipe> getPipes() {
        // Begin by importing documents from text to feature sequences
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

        // Pipes: lowercase, tokenize, remove stopwords, map to features
        pipeList.add(new CharSequenceLowercase());
        pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
        pipeList.add(new TokenSequenceRemoveStopwords().addStopWords(stopwords).setCaseSensitive(false)
                .setMarkDeletions(false));
        pipeList.add(new TokenSequenceRemoveStopPatterns(new String[]{"^http:", "^https:"}));
        pipeList.add(new TokenSequence2FeatureSequence());
        return pipeList;
    }

    /**
     * Converts a list of documents to a list of instances by pushing them over the pipes
     */
    protected InstanceList getInstances(List<String> docs, ArrayList<Pipe> pipeList) {
        InstanceList instances = new InstanceList(new SerialPipes(pipeList));
        instances.addThruPipe(new Iterator<Instance>() {
            Iterator<String> iter = docs.iterator();

            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public Instance next() {
                String doc = iter.next();
                Instance $ = new Instance(doc, null, null, null);
                return $;
            }
        });
        return instances;
    }

    protected String explainTopWordsPerTopic(int numTopics, Alphabet dataAlphabet, double[] topicDistribution,
                                             ArrayList<TreeSet<IDSorter>> topicSortedWords) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (int topic = 0; topic < numTopics; topic++) {
            Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();

            Formatter out = new Formatter(new StringBuilder(), Locale.US);
            out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
            int rank = 0;
            while (iterator.hasNext() && rank < 5) {
                IDSorter idCountPair = iterator.next();
                out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
                rank++;
            }
            ssb.append(out);
        }
        return ssb.append("").toString();
    }


//    protected List<String> getDocs(List<Path> regions) throws IOException {
//        InputHanlder reader = new InputHanlder();
//        List<String> $ = new LinkedList<String>();
//
//        for (Path region : regions) {
//            SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
//            Set<Message> messages = reader.readFromMessages(region, 2000000);
//            for (Message m : messages) {
//                ssb.append(m.getMsg());
//            }
//            $.add(ssb.toString());
//        }
//        System.out.println($.size() + " documents");
//        return $;
//    }


    /**
     * @return top 5 words in topic with proportions for the first document
     */
    //    /**
    //     * @return top 5 words in topic with proportions for the first document, format: topic_id \t term (weight)
    //     */
//    protected String explainTopWordsPerTopic(int numTopics, Alphabet dataAlphabet, ArrayList<TreeSet<IDSorter>>
//            topicSortedWords, int k) {
//        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
//        for (int topic = 0; topic < numTopics; topic++) {
//            Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
//
//            Formatter out = new Formatter(new StringBuilder(), Locale.US);
//            out.format("%d\t", topic);
//            int rank = 0;
//            while (iterator.hasNext() && rank < k) {
//                IDSorter idCountPair = iterator.next();
//                out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
//                rank++;
//            }
//            ssb.append(out);
//        }
//        return ssb.append("").toString();
//    }

    //    public String getTopics(ParallelTopicModel model, List<String> docs) {
//        FeatureSequence tokens = (FeatureSequence) model.getData().get(0).instance.getData();
//        LabelSequence topics = model.getData().get(0).topicSequence;
//
//        Formatter out = new Formatter(new StringBuilder(), Locale.US);
//        for (int position = 0; position < tokens.getLength(); position++) {
//            out.format("%s-%d ", getInstances(docs, getPipes()).getDataAlphabet().lookupObject(tokens
//                    .getIndexAtPosition(position)), topics
//                    .getIndexAtPosition(position));
//        }
//        return out.toString() + "\n";
//    }

}
