package dataManipulations;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.geo.GridService;
import domain.Message;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.util.*;

public class SpatioTextualAuxilary {
    private final GridService gs = new GridService();
    private final char MESSAGES_SEPARATOR = '\n';


    /**
     * @return a concatination of all the messages for each region
     */
    public List<String> extractRegionString(List<List<Polygon>> toEvaluate, Set<Message> messages) throws IOException {
        List<MultiPolygon> regions = gs.getMultiPolygons(toEvaluate);
        Map<MultiPolygon, SeparatorStringBuilder> map = assignMsgsToMPs(messages, regions);
        return extractString(map.values());
    }

    private List<String> extractString(Collection<SeparatorStringBuilder> stringBuilders) {
        List<String> $ = new ArrayList<>(stringBuilders.size());
        for (SeparatorStringBuilder ssb : stringBuilders) {
            $.add(ssb.toString());
        }
        return $;
    }

    private Map<MultiPolygon, SeparatorStringBuilder> assignMsgsToMPs(Set<Message> messages, List<MultiPolygon>
            regions) {
        Map<MultiPolygon, SeparatorStringBuilder> map = new HashMap<>();
        for (MultiPolygon mp : regions) {
            map.put(mp, new SeparatorStringBuilder(MESSAGES_SEPARATOR));
        }

        for (Message m : messages) {
            for (MultiPolygon mp : map.keySet()) {
                if (m.location.within(mp)) {
                    map.get(mp).append(m.getMsg());
                }
            }
        }
        return map;
    }
}
