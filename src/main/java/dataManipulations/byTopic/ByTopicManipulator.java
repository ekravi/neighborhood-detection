package dataManipulations.byTopic;

import dataManipulations.files.InputHanlder;
import domain.Message;
import experiments.settings.DefaultSettings;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Filter only messages for a given topic
 */
public class ByTopicManipulator {
    public void split(Path inDir) throws IOException {
        Map<String, Set<String>> termsPerTopic = getTermsPerTopic(inDir);
        Set<Message> msgs = new InputHanlder().readFromMessages(DefaultSettings.resourceFolder.resolve(DefaultSettings
                .manhattanMessages), 15000000);

        Map<String, Set<Message>> messagesPerTopic = getMessagesPerTopic(termsPerTopic, msgs);

        for (String topic : messagesPerTopic.keySet()) {
            Set<Message> mpt = messagesPerTopic.get(topic);
            new InputHanlder().saveAsCSV(mpt, inDir.resolve(topic + ".csv"));
            System.out.println(topic + ": " + mpt.size());
        }
    }

    public void filter(Path inTopicFile) throws IOException {
        Set<String> topicTerms = getTopicTerms(inTopicFile);
        Set<Message> msgs = new InputHanlder().readFromMessages(DefaultSettings.resourceFolder.resolve(DefaultSettings
                .manhattanMessages), 15000000), relevant = new HashSet<>();
        for (Message m : msgs) {
            if (isMessageRelevnat(topicTerms, m)) {
                relevant.add(m);
            }
        }
        new InputHanlder().saveAsCSV(relevant, inTopicFile.resolveSibling(inTopicFile.getName(inTopicFile
                .getNameCount() - 1) + ".csv"));
    }

    private Map<String, Set<Message>> getMessagesPerTopic(Map<String, Set<String>> termsPerTopic, Set<Message> msgs) {
        Map<String, Set<Message>> messagesPerTopic = new HashMap<>();
        for (String topic : termsPerTopic.keySet()) {
            Set<String> terms = termsPerTopic.get(topic);
            for (Message m : msgs) {
                if (isMessageRelevnat(terms, m)) {
                    if (!messagesPerTopic.containsKey(topic))
                        messagesPerTopic.put(topic, new HashSet<>());
                    messagesPerTopic.get(topic).add(m);
                }
            }
        }

        return messagesPerTopic;
    }

    private boolean isMessageRelevnat(Set<String> terms, Message m) {
        for (String s : m.getMsg().split("\\s+")) {
            if (terms.contains(s.toLowerCase()))
                return true;
        }
        return false;
    }

    private Map<String, Set<String>> getTermsPerTopic(Path inDir) throws IOException {
        Map<String, Set<String>> termsPerTopc = new HashMap<>();
        for (String fileName : fileList(inDir)) {
            Path file = inDir.resolve(fileName);
            termsPerTopc.put(fileName, getTopicTerms(file));
        }
        return termsPerTopc;
    }

    private Set<String> getTopicTerms(Path file) throws IOException {
        Set<String> $ = new HashSet<>();
        $.add(file.toFile().getName());
        try (BufferedReader br = Files.newBufferedReader(file, Charset.defaultCharset())) {
            String line;
            while ((line = br.readLine()) != null) {
                $.add(line.trim().toLowerCase());
            }
        }
        return $;
    }

    private List<String> fileList(Path inDir) throws IOException {
        List<String> fileNames = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(inDir)) {
            for (Path path : directoryStream) {
                fileNames.add(path.toFile().getName());
            }
        }
        return fileNames;
    }


    public void merge(Path out, Path... inPaths) throws IOException {
        Set<Message> msgs = new HashSet<>();
        InputHanlder handler = new InputHanlder();
        for (Path in : inPaths) {
            msgs.addAll(handler.readFromMessages(in, 1000000));
        }
        handler.saveAsCSV(msgs, out);
    }

    public static void main(String[] args) throws IOException {
//        new ByTopicManipulator().split(Paths.get("src/main/resources/byTopic"));
//        new ByTopicManipulator().merge(DefaultSettings.BY_TOPIC_PATH.resolve("building.nature.sea.csv"), new
//                Path[]{DefaultSettings
//                .BY_TOPIC_PATH.resolve("building.csv"), DefaultSettings.BY_TOPIC_PATH.resolve("nature.csv"),
//                DefaultSettings
//                        .BY_TOPIC_PATH.resolve("sea.csv")});
//        new ByTopicManipulator().merge(DefaultSettings.BY_TOPIC_PATH.resolve("business.living.shopping.csv"),
//                new Path[]{DefaultSettings.BY_TOPIC_PATH.resolve("business.csv"), DefaultSettings.BY_TOPIC_PATH
// .resolve
//                        ("residence.csv"), DefaultSettings.BY_TOPIC_PATH.resolve("shopping.csv")});

        new ByTopicManipulator().filter(DefaultSettings.BY_TOPIC_PATH.resolve("bohemia"));

    }
}
