package dataManipulations.byTopic;

import experiments.settings.DefaultSettings;
import service.SeparatorStringBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class LocationCreator {
    public final String SEPARATOR = "\t";

    public void create(Path in, Path out) throws IOException {
        try (BufferedReader br = Files.newBufferedReader(in); BufferedWriter bw = Files.newBufferedWriter(out, Charset
                .defaultCharset())) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] split = line.split(SEPARATOR);
                bw.write(new SeparatorStringBuilder(SEPARATOR).append(split[2]).append(split[1]).toString());
                bw.newLine();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        String topic = "bohemia";
        new LocationCreator().create(DefaultSettings.BY_TOPIC_PATH.resolve(topic + ".csv"), DefaultSettings
                .BY_TOPIC_PATH
                .resolve(topic + "Locations.csv"));
    }
}
