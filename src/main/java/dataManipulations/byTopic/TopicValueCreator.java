package dataManipulations.byTopic;

import algorithms.splits.evaluation.SingleLDASplitEvaluator;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import domain.Message;
import experiments.settings.DefaultSettings;
import org.apache.commons.io.IOUtils;
import org.geotools.geojson.geom.GeometryJSON;
import service.SeparatorStringBuilder;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TopicValueCreator {
    public void create(Path out) throws IOException {
        Set<Message> relevantMsgs = new InputHanlder().readFromMessages(DefaultSettings.BY_TOPIC_PATH.resolve
                ("building.nature.sea.csv"), 1500000);
        Path mp = DefaultSettings.resourceFolder.resolve
                ("runningConfigurations").resolve("WESideNCentralPark").resolve("intersectingCells.json");

        MultiPolygon collection = new GeometryJSON().readMultiPolygon(new FileReader(mp.toFile()));
        Set<Polygon> roi = new HashSet<>(collection.getNumGeometries());
        for (int i = 0; i < collection.getNumGeometries(); ++i) {
            roi.add((Polygon) collection.getGeometryN(i));
        }
        SingleLDASplitEvaluator eval = new SingleLDASplitEvaluator(new HashSet<>(IOUtils.readLines(new FileInputStream
                (DefaultSettings.resourceFolder.resolve("stopwords.txt").toFile()), Charset.defaultCharset())),
                3/*DefaultSettings.LDA_NUM_TOPICS*/,
                DefaultSettings.SINGLE_LDA_NUM_ITERATIONS);
        eval.init(roi, relevantMsgs);

        List<Map<String, Integer>> topTermsPerTopic = eval.getTopTermsPerTopic(100);
        for (Map<String, Integer> topicTerms : topTermsPerTopic) {
            System.out.println(topicTerms);
        }

        try (BufferedWriter bw = Files.newBufferedWriter(out, Charset.defaultCharset())) {
            for (Map.Entry<Polygon, double[]> entry : eval.getTopicPerCell().entrySet()) {
                SeparatorStringBuilder ssb = new SeparatorStringBuilder('\t');
                Point center = entry.getKey().getCentroid();
                ssb.append(center.getX()).append(center.getY());
                for (double d : entry.getValue()) {
                    ssb.append(d);
                }
                bw.write(ssb.toString());
                bw.newLine();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new TopicValueCreator().create(DefaultSettings.BY_TOPIC_PATH.resolve("building.nature.sea_locations.csv"));
    }
}
