package dataManipulations.geo;

import com.vividsolutions.jts.geom.Polygon;
import experiments.settings.DefaultSettings;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FeatureCollectionToMultiPolygonConvertor {
    public void convert(Path in, Path out) throws IOException {
        FeatureCollection fc = new FeatureJSON().readFeatureCollection(new FileInputStream
                (in.toFile()));

        FeatureIterator iter = fc.features();
        List<Polygon> polygons = new ArrayList<>(fc.size());
//        Polygon union = (Polygon) ((SimpleFeature) fc.features().next()).getDefaultGeometry();
        while (iter.hasNext()) {
            SimpleFeature feature = (SimpleFeature) iter.next();
            Polygon p = (Polygon) feature.getDefaultGeometry();
            polygons.add(p);
//            union = (Polygon) union.union(p);
        }
        Polygon[] arr = new Polygon[polygons.size()];
        new GeometryJSON().writeMultiPolygon(new GeometryBuilder().multiPolygon(polygons.toArray(arr)), new
                FileOutputStream(out.toFile()));
    }

    public static void main(String[] args) throws IOException {
        Path baseDir = DefaultSettings.resourceFolder.resolve("fixedNeighborhoods");

        new FeatureCollectionToMultiPolygonConvertor().convert(baseDir.resolve("out-rewind-clean.json"), baseDir
                .resolve("mnhtnNeighborhoodsMP.json"));
    }
}
