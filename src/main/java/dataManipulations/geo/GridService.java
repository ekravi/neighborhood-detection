package dataManipulations.geo;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import domain.Message;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.GeometryBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.grid.Grids;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.io.IOException;
import java.util.*;

/**
 * Created by ekravi on 11/07/2017.
 */
public class GridService {

    public final static CoordinateReferenceSystem DEFAULT_CRS = DefaultGeographicCRS.WGS84;


    public Set<Polygon> getPolygons(MultiPolygon mp) {
        Set<Polygon> $ = new HashSet<>();
        for (int i = 0; i < mp.getNumGeometries(); ++i) {
            $.add((Polygon) mp.getGeometryN(i));
        }
        return $;
    }


    public SimpleFeatureSource generateGridFromMessages(Set<Message> msgs, double widthInMeters) throws IOException {
        double maxLat = -Double.MAX_VALUE, minLat = Double.MAX_VALUE, maxLon = -Double.MAX_VALUE, minLon = Double
                .MAX_VALUE;

        for (Message m : msgs) {
            double lon = m.location.getX(), lat = m.location.getY();
            maxLon = Math.max(maxLon, lon);
            minLon = Math.min(minLon, lon);
            maxLat = Math.max(maxLat, lat);
            minLat = Math.min(minLat, lat);
        }
        ReferencedEnvelope envelope = new ReferencedEnvelope(minLon, maxLon, minLat, maxLat, DEFAULT_CRS);
        return Grids.createSquareGrid(envelope, convertToDegrees(envelope, widthInMeters));
    }

    private ReferencedEnvelope getBoundingPolygon(List<List<Polygon>> finalSolution) {
        double maxLat = -Double.MAX_VALUE, minLat = Double.MAX_VALUE, maxLon = -Double.MAX_VALUE, minLon = Double
                .MAX_VALUE;
        for (List<Polygon> region : finalSolution) {
            for (Polygon p : region) {
                maxLon = Math.max(maxLon, p.getEnvelopeInternal().getMaxX());
                minLon = Math.min(minLon, p.getEnvelopeInternal().getMinX());
                maxLat = Math.max(maxLat, p.getEnvelopeInternal().getMaxY());
                minLat = Math.min(minLat, p.getEnvelopeInternal().getMinY());
            }
        }

        return new ReferencedEnvelope(minLon, maxLon, minLat, maxLat, DEFAULT_CRS);
    }

    public List<Polygon> generateGrid(Set<Message> msgs, int widthInMeters) throws IOException {
        double maxLat = -Double.MAX_VALUE, minLat = Double.MAX_VALUE, maxLon = -Double.MAX_VALUE, minLon = Double
                .MAX_VALUE;

        for (Message m : msgs) {
            double lon = m.location.getX(), lat = m.location.getY();
            maxLon = Math.max(maxLon, lon);
            minLon = Math.min(minLon, lon);
            maxLat = Math.max(maxLat, lat);
            minLat = Math.min(minLat, lat);
        }
        ReferencedEnvelope envelope = new ReferencedEnvelope(minLon, maxLon, minLat, maxLat, DEFAULT_CRS);
        return getPolygons(Grids.createSquareGrid(envelope, convertToDegrees(envelope, widthInMeters)));
    }

    public List<Polygon> generateGrid(List<List<Polygon>> solution, int widthInMeters) throws IOException {
        ReferencedEnvelope envelope = getBoundingPolygon(solution);
        return getPolygons(Grids.createSquareGrid(envelope,
                convertToDegrees(envelope, widthInMeters)));
    }

    private List<Polygon> getPolygons(SimpleFeatureSource grid) throws IOException {
        List<Polygon> $ = new LinkedList<>();
        try (SimpleFeatureIterator featuresIter = grid.getFeatures().features()) {
            while (featuresIter.hasNext()) {
                $.add(JTS.toGeometry(featuresIter.next().getBounds()));
            }
        }
        return $;
    }


    /**
     * @return diff in degrees
     */
    private double convertToDegrees(ReferencedEnvelope envelope, double distInMeters) {
        GeometryBuilder builder = new GeometryBuilder();
        Point bl = builder.point(envelope.getMinX(), envelope.getMinY()), br = builder.point(envelope.getMaxX(),
                envelope.getMinY()), tl = builder.point(envelope.getMinX(), envelope.getMaxY()), tr = builder.point
                (envelope.getMaxX(), envelope.getMaxY());

        GeodeticCalculator gc = new GeodeticCalculator(DEFAULT_CRS);
        gc.setStartingGeographicPoint(bl.getX(), bl.getY());
        gc.setDestinationGeographicPoint(br.getX(), br.getY());
        double width = gc.getOrthodromicDistance();

        gc.setDestinationGeographicPoint(tl.getX(), tl.getY());
        double length = gc.getOrthodromicDistance();

        double widthDeg = bl.distance(br), lengthDeg = bl.distance(tl);
        if (widthDeg > lengthDeg)
            return widthDeg / (width / distInMeters);
        else
            return lengthDeg / (length / distInMeters);
    }

    public List<Polygon> getIntersectingCells(SimpleFeatureSource grid, Polygon p) throws IOException {
        List<Polygon> $ = new LinkedList<>();

        try (SimpleFeatureIterator featuresIter = grid.getFeatures().features()) {
            while (featuresIter.hasNext()) {
                SimpleFeature cell = featuresIter.next();
                Polygon geom = JTS.toGeometry(cell.getBounds());
                if (p.intersects(geom) || p.contains(geom)) {
                    $.add(geom);
                }
            }
        }

        System.out.println($.size() + " intersecting cells");
        return $;
    }

    public List<Polygon> getIntersectingCells(List<Polygon> grid, Geometry p) {
        List<Polygon> $ = new ArrayList<>(grid.size());
        for (Polygon curr : grid) {
            if (p.intersects(curr) || p.contains(curr)) {
                $.add(curr);
            }
        }
        return $;
    }

    public List<Polygon> getIntersectingCells(List<Polygon> grid, Polygon p, int[][] gridMapping) {
        List<Polygon> $ = new ArrayList<>(grid.size());
        int numCol = gridMapping[0].length;

        for (int i = 0; i < grid.size(); ++i) {
            Polygon curr = grid.get(i);
            if (p.intersects(curr) || p.contains(curr)) {
                $.add(curr);
                int row = i / numCol, col = i % numCol;
                gridMapping[row][col] = 1;
            }
        }
        return $;
    }

    public int[][] getMappingOfIntersectingCells(List<Polygon> grid, Polygon p) {
        int[][] gridMapping = getGridMapping(grid);
        int numCol = gridMapping[0].length;
        for (int i = 0; i < grid.size(); ++i) {
            Polygon curr = grid.get(i);
            if (p.intersects(curr) || p.contains(curr)) {
                int row = i / numCol, col = i % numCol;
                gridMapping[row][col] = 1;
            }
        }
        return gridMapping;
    }

    public int[][] getGridMapping(List<Polygon> grid) {
        int cols = 0, rows = 0;
        for (int i = 1; i < grid.size(); ++i) {
            if (grid.get(i).getEnvelopeInternal().getMinY() > grid.get(0).getEnvelopeInternal().getMinY())
                cols = i + 1;
        }
        rows = (grid.size() + 1) / cols;
        int[][] gridInt = new int[rows][cols];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < rows; ++j) {
                gridInt[i][j] = 0;
            }
        }
        return gridInt;
    }

    protected SimpleFeatureType creatType(String finalName) {
        SimpleFeatureTypeBuilder tb = new SimpleFeatureTypeBuilder();
        tb.setName(finalName);
        tb.add("element", Polygon.class, DEFAULT_CRS);
        tb.add("id", Integer.class);
        return tb.buildFeatureType();
    }

    public MultiPolygon getMultiPolygon(List<Polygon> polygons) throws IOException {
        Polygon[] arr = new Polygon[polygons.size()];
        return new GeometryBuilder().multiPolygon(polygons.toArray(arr));
    }

    public Set<Message> getIntersectingMsgs(Set<Message> msgs, Polygon p) {
        Set<Message> $ = new HashSet<>();
        for (Message m : msgs) {
            if (p.contains(m.location))
                $.add(m);
        }
        return $;
    }

    public List<MultiPolygon> getMultiPolygons(List<List<Polygon>> solution) throws IOException {
        List<MultiPolygon> $ = new ArrayList<>(solution.size());

        for (List<Polygon> region : solution) {
            $.add(getMultiPolygon(region));
        }
        return $;
    }

//    public static void main(String[] args) throws IOException, ParseException {
//        GeometryJSON gjson = new GeometryJSON();
//
//        Geometry gg = gjson.read(new FileInputStream(new File("src/main/resources/centralParkExample2.json")));
//
//
//        InputReader reader = new InputReader();
//        Set<Message> msgs = reader.readFromAllFields(Paths.get
//                ("src/main/resources/input/tweets.csv"), 2000000);
////        Set<Message> msgs = reader.readFromMessages(Paths.get
////                ("src/main/resources/input/regions/centralPark"), 1000000);
//        System.out.println(msgs.size() + " messages");
////
//        GridService g = new GridService();
//
//        int widthInMeters = 1000;
//        SimpleFeatureSource gridSF = g.generateGridFromMessages(msgs, widthInMeters);
//        System.out.println(gridSF.getFeatures().size() + " cells of " + widthInMeters + "^2 sq. meters");
//        List<Polygon> grid = g.getPolygons(gridSF);
////        gjson.write(gridMP, System.out);
////        System.out.println();
//
//        Reader jsonReader = new FileReader(new File
//                ("src/main/resources/input/runningConfigurations/polygons/CentralParkBoundaries.json"));
//        Polygon p = gjson.readPolygon(jsonReader);
//
//        int[][] gridMapping = g.getGridMapping(grid);
//        List<Polygon> intersectingCells = g.getIntersectingCells(grid, p, gridMapping);
////        gjson.write(intersectingCells, System.out);
////        System.out.println();
//
//        IRegionSplit splitAlgo = new RandomRegionSplit();
//        List<List<Polygon>> split = splitAlgo.splitToConnectedSubgraphs(intersectingCells,
//                3);
//
//        System.out.println(split.size() + " connected sugraphs:");
//        for (List<Polygon> polygons : split) {
//            gjson.write(g.getMultiPolygon(polygons), System.out);
//            System.out.println();
//        }
//
//        /**
//         * Collect messages for each region (also verify within AOI)
//         */
//        List<SeparatorStringBuilder> prepDocs = new ArrayList<>(split.size());
//        for (int i = 0; i < split.size(); ++i) {
//            prepDocs.add(new SeparatorStringBuilder('\n'));
//        }
//        for (Message m : msgs) {
//            for (List<Polygon> polygons : split) {
//                if (m.location.within(g.getMultiPolygon(polygons)) && m.location.within(p)) {
//                    int i = split.indexOf(g.getMultiPolygon(polygons));
//                    prepDocs.get(i).append(m.getMsg());
//                }
//            }
//        }
//
//        List<String> docs = new ArrayList<>(prepDocs.size());
//        for (int i = 0; i < prepDocs.size(); ++i) {
//            docs.add(i, prepDocs.get(i).toString());
//        }
//        System.out.println("assigned " + msgs.size() + " messages to cells");
//
//        int numTopics = 10;
//        double splitProb = new SplitProbabilityCalcualtor().getSplitProb(numTopics, docs);
//        System.out.println(new StringBuilder().append(numTopics).append(" topics,").append('\t').append("average
// log " +
//                "probability: ").append
//                (splitProb).toString());
//
//    }
}
