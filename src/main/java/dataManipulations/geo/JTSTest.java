package dataManipulations.geo;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

/**
 * Created by ekravi on 27/06/2017.
 */
public class JTSTest {
    private Coordinate createPoint(double lat, double lon) {
        double EARTH_RADIUS = 6378137.0;
        double x = lon * EARTH_RADIUS * Math.PI / 180.;
        double y = EARTH_RADIUS * Math.sin(Math.toRadians(lat));
        return new Coordinate(x, y, 0.);
//        geometryFactory.createPoint(new Coordinate(longitude, latitude))
    }

    public static void main(String[] args) {
        GeometryFactory factory;
        Coordinate point = new JTSTest().createPoint(0.1, 0.1);
    }
}
