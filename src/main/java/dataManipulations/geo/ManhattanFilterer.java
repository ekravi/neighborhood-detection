package dataManipulations.geo;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import domain.Message;
import experiments.settings.DefaultSettings;
import org.geotools.geojson.geom.GeometryJSON;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ManhattanFilterer {
    public static void main(String[] args) throws IOException {
        Set<Message> msgs = new InputHanlder().readFromMessages(DefaultSettings.resourceFolder.resolve(DefaultSettings
                .allMessages), 1500000);
        GridService gs = new GridService();
        GeometryJSON gj = new GeometryJSON();

        System.out.println("messages read");
        Reader jsonReader = new FileReader(DefaultSettings.resourceFolder.resolve
                (DefaultSettings.MANHATTAN_POLYGON_FILE_NAME).toFile());
        Geometry manhattanPolygon = gj.readMultiPolygon(jsonReader);

        System.out.println("polygon read");

        Set<Message> manhattanMsgs = new HashSet<>();

        int i = 0;
        for (Message m : msgs) {
            if (manhattanPolygon.contains(m.location))
                manhattanMsgs.add(m);
            if (++i % 10000 == 0) System.out.println(String.valueOf(i) + " steps");
        }
        System.out.println("filtered messages");

        new InputHanlder().saveAsCSV(manhattanMsgs, DefaultSettings.resourceFolder.resolve(DefaultSettings
                .manhattanMessages));
        System.out.println("messages saved");


        List<Polygon> grid = gs.generateGrid(manhattanMsgs, 100);
        try (BufferedWriter bw = Files.newBufferedWriter(DefaultSettings.resourceFolder.resolve(DefaultSettings
                .manhattanGrid))) {
            bw.write(gj.toString(gs.getMultiPolygon(grid)));
        }
        System.out.println("grid saved");
        System.out.println(grid.size() + " - cells in grid");

        List<Polygon> intersectingCells = gs.getIntersectingCells(grid,
                manhattanPolygon);
        System.out.println(intersectingCells.size() + " - intersecting cells");
        try (BufferedWriter bw = Files.newBufferedWriter(DefaultSettings.resourceFolder.resolve
                ("manhattanIntersectingCells"))) {
            bw.write(gj.toString(gs.getMultiPolygon(intersectingCells)));
        }
    }
}
