package dataManipulations.geo;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import experiments.settings.DefaultSettings;
import org.apache.commons.io.IOUtils;
import org.geotools.geojson.geom.GeometryJSON;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ManhattanJSONFilterer {
    public static final String NEIGHBORHOODS = "fixedNeighborhoods";

    public void filter(Path in, Path out) throws IOException {
        JSONObject data = new JSONObject(IOUtils.toString(new FileReader(in.toFile())));
        JSONArray features = data.getJSONArray("features");

        JSONObject outJSON = new JSONObject();
        outJSON.put("type", "FeatureCollection");
        JSONArray outFeatures = new JSONArray();

        for (int i = 0; i < features.length(); ++i) {
            JSONObject feature = features.getJSONObject(i);
            if (feature.getJSONObject("properties").getString("borough").equals("Manhattan")) {
                outFeatures.put(feature);
            }
        }
        outJSON.put("features", outFeatures);
        outJSON.write(Files.newBufferedWriter(out));
    }

    public void convertToGeometryCollection(Path in, Path out) throws IOException {
        JSONObject data = new JSONObject(IOUtils.toString(new FileReader(in.toFile())));
        JSONArray features = data.getJSONArray("features");

        List<Geometry> geometries = new ArrayList<>(features.length());
        for (int i = 0; i < features.length(); ++i) {
            geometries.add(new GeometryJSON().read(features.getJSONObject(i).getJSONObject("geometry").toString()));
        }

        Geometry[] arr = new Geometry[geometries.size()];
        new GeometryJSON().writeGeometryCollection(new GeometryFactory().createGeometryCollection(geometries.toArray
                (arr)), new FileOutputStream(out.toFile()));
    }

    public static void main(String[] args) throws IOException {
        Path base = DefaultSettings.resourceFolder.resolve(NEIGHBORHOODS);
//        new ManhattanJSONFilterer().filter(base.resolve("in.json"), base.resolve("out.json"));
        new ManhattanJSONFilterer().convertToGeometryCollection(base.resolve("out-rewind-clean.json"), base.resolve
                ("out-geometry.json"));
    }
}
