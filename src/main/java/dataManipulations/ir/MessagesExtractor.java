package dataManipulations.ir;

import domain.Message;

import java.io.IOException;
import java.util.*;

/**
 * Extracts texts from a set of messages
 */
public class MessagesExtractor {

    public Set<List<String>> getCleanTexts(Set<Message> toProcess) throws IOException {
        new TextCleaner().clean(toProcess);
        Set<List<String>> $ = new HashSet<>();
        for (Message m : toProcess) {
            $.add(new ArrayList<>(Arrays.asList(m.getMsg().split("\\s+"))));
        }
        return $;
    }

    public Set<List<String>> getTexts(Set<Message> toProcess) throws IOException {
        Set<List<String>> $ = new HashSet<>();
        for (Message m : toProcess) {
            $.add(new ArrayList<>(Arrays.asList(m.getMsg().split("\\s+"))));
        }
        return $;
    }

}
