package dataManipulations.ir;

import dataManipulations.files.InputHanlder;
import domain.Message;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import service.SeparatorStringBuilder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

/**
 * Cleaning given messages by keeping only English, splitting whitespaces, lowercasing and removing stopwords
 * No Stemming is done (as the input is Twitter messages)
 */
public class TextCleaner {
    Analyzer analyzer = new StopwordAnalyzerBase() {
        @Override
        protected TokenStreamComponents createComponents(String fieldName) {
            Tokenizer source = new WhitespaceTokenizer();
            TokenStream tok = new StandardFilter(source);
            tok = new URLFilter(tok);
            tok = new LowerCaseFilter(tok);
            tok = new StopFilter(tok, StandardAnalyzer.ENGLISH_STOP_WORDS_SET);
            return new TokenStreamComponents(source, tok);
        }
    };

    public void clean(Set<Message> toClean) throws IOException {
        Set<Message> toRemove = new HashSet<>();

        for (Message m : toClean) {
            TokenStream stream = analyzer.tokenStream(null, m.getMsg());
            stream.reset();
            SeparatorStringBuilder ssb = new SeparatorStringBuilder(' ');
            while (stream.incrementToken()) {
                ssb.append(stream.getAttribute(CharTermAttribute.class).toString());
            }
            stream.close();
            String clean = ssb.toString();
            if (clean.isEmpty() || !isValidISOLatin1(clean)) toRemove.add(m);
            else m.setMsg(clean);
        }
        toClean.removeAll(toRemove);
    }

    private boolean isValidISOLatin1(String s) {
        return Charset.forName("US-ASCII").newEncoder().canEncode(s);
    } // or "ISO-8859-1" for ISO Latin 1


    public static void main(String[] args) throws IOException {
        InputHanlder reader = new InputHanlder();
//        for (CentralPark3EqualRegions inputFile : CentralPark3EqualRegions.values()) {
//            Path input = Paths.get(CentralPark3EqualRegions.BASE_FOLDER).resolve(inputFile.name);
        Path input = Paths.get
                ("src/main/resources/tweets.csv");
        Set<Message> toClean = reader.readFromAllFields(input, 5000000);
        new TextCleaner().clean(toClean);
        reader.saveAsCSV(toClean, Paths.get(input.toFile().getPath() + "Clean"));
//        }
    }
}
