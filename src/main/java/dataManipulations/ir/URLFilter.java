package dataManipulations.ir;

import org.apache.lucene.analysis.FilteringTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;

/**
 * Created by ekravi on 23/05/2017.
 */
public class URLFilter extends FilteringTokenFilter {
    private final CharTermAttribute termAtt = (CharTermAttribute) this.addAttribute(CharTermAttribute.class);


    public URLFilter(TokenStream in) {
        super(in);
    }

    @Override
    protected boolean accept() throws IOException {
        String term = this.termAtt.toString().toLowerCase();
        return (!term.startsWith("http://") && !term.startsWith("https://"));
    }
}
