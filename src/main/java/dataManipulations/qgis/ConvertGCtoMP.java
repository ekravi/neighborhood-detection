package dataManipulations.qgis;

import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import experiments.settings.DefaultSettings;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.GeometryBuilder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ConvertGCtoMP {
    public void convert(Path in, Path out) throws IOException {
        if (!Files.exists(in) || Files.isWritable(out))
            throw new IllegalArgumentException();
        GeometryJSON gj = new GeometryJSON();
        GeometryCollection gc = gj.readGeometryCollection(new FileInputStream(in.toFile()));
        List<Polygon> polygons = new ArrayList<>(gc.getNumGeometries());
        for (int i = 0; i < gc.getNumGeometries(); ++i) {
            MultiPolygon mp = (MultiPolygon) gc.getGeometryN(i);
            Polygon p = (Polygon) mp.getGeometryN(0);
            for (int j = 0; j < mp.getNumGeometries(); ++j) {
                Polygon p2 = (Polygon) mp.getGeometryN(j);
                p = (Polygon) p.union(p2);
            }
            polygons.add(p);
        }
        Polygon[] arr = new Polygon[polygons.size()];
        new GeometryJSON().writeMultiPolygon(new GeometryBuilder().multiPolygon(polygons.toArray(arr)), new
                FileOutputStream(out.toFile()));
    }

    public static void main(String[] args) throws IOException {
        Path baseDir = DefaultSettings.resourceFolder.resolve("chinatownTribecaLittleItaly");
        new ConvertGCtoMP().convert(baseDir.resolve
                        ("INITIAL_FIRST_regionGC_5_1000000_southMnhtn_1507571562222_results").resolve("solution.json"),
                baseDir.resolve("INITIAL_FIRST_regionGC_5_1000000_southMnhtn_1507571562222_results").resolve
                        ("solutoinMP.json"));
    }
}
