package dataManipulations.settings;

/**
 * Created by ekravi on 26/06/2017.
 */
public enum CentralPark3EqualRegions {
    ST59TO76("st59To76"), ST76TO93("st76To93"), ST93TO110("st93To110");

    public static final String BASE_FOLDER = "src/main/resources/input/regions/centralPark3EqualRegions";
    public final String name;

    CentralPark3EqualRegions(String name) {
        this.name = name;
    }
}
