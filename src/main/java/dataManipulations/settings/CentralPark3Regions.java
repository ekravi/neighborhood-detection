package dataManipulations.settings;

/**
 * Created by ekravi on 26/06/2017.
 */
public enum CentralPark3Regions {
    ST59TO79("st59To79"), ST79TO97("st79To97"), ST97TO110("st97To110");

    public static final String BASE_FOLDER = "src/main/resources/input/regions/centralPark3Regions";
    public final String name;

    CentralPark3Regions(String name) {
        this.name = name;
    }
}
