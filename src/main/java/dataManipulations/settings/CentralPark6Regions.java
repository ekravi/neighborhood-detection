package dataManipulations.settings;

/**
 * Created by ekravi on 23/05/2017.
 */
public enum CentralPark6Regions {
    //    CENTRAL_PARK("centralPark"), LITTLE_ITALY("littlyItaly"), UPPER_EAST_SIDE("upperEastSide"),
    st59To65("st59To65"), st65To72("st65To72"), st72To79("st72To79"), st79To86("st79To86"), st86To97("st86To97"),
    st97To110("st97To110");

    public static final String BASE_FOLDER = "src/main/resources/input/regions/centralPark6Regions";
    public final String name;

    CentralPark6Regions(String name) {
        this.name = name;
    }
}

