package dataManipulations.settings;

/**
 * Created by ekravi on 26/06/2017.
 */
public enum ManhattanRegions {
    CENTRAL_PARK("centralPark"), LITTLE_ITALY("littlyItaly"), UPPER_EAST_SIDE("upperEastSide");

    public static final String BASE_FOLDER = "src/main/resources/input/regions";
    public final String name;

    ManhattanRegions(String name) {
        this.name = name;
    }
}
