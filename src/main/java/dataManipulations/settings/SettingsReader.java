package dataManipulations.settings;

import domain.experiments.InitialSplit;
import domain.experiments.RandomSplit;
import domain.experiments.Run;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class SettingsReader {
    public List<Run> getRuns(Path settings) throws IOException {
        List<Run> $ = new LinkedList<>();

        try (BufferedReader br = Files.newBufferedReader(settings)) {
            JSONObject runs = new JSONObject(IOUtils.readLines(br));
            Path resourceFolder = Paths.get(runs.getString("resource-folder")), runFolder = Paths.get(runs.getString
                    ("run-folder")), resultsFolder = Paths.get(runs.getString("results-folder"));
            JSONArray allRuns = runs.getJSONArray("runs");
            for (int i = 0; i < allRuns.length(); ++i) {
                JSONObject run = allRuns.getJSONObject(i);
                if (run.getString("type").equals(RUN_TYPE.RANDOM.name))
                    $.add(new RandomSplit(resourceFolder, runFolder, resultsFolder, run.getString("name"), run.getInt
                            ("grid-width"), run.getInt("split-size"), run.getInt("num-runs"), run.getInt
                            ("num-steps"), RUN_TYPE.RANDOM));

                else
                    $.add(new InitialSplit(resourceFolder, runFolder, resultsFolder, run.getString("name"), run.getInt
                            ("grid-width"), run.getInt("split-size"), run.getInt("num-runs"), run.getInt
                            ("num-steps"), RUN_TYPE.INITIAL_SPLIT, run.getString("initial-file")));
            }
            return $;
        }
    }

    public static enum RUN_TYPE {
        RANDOM("random"), INITIAL_SPLIT("initial");
        public final String name;

        RUN_TYPE(String name) {
            this.name = name;
        }
    }

}