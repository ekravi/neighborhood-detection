package dataManipulations.temporal;

import dataManipulations.files.InputHanlder;
import dataManipulations.settings.CentralPark6Regions;
import domain.Message;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ekravi on 23/05/2017.
 */
public class EpochDivider {

    public Map<DailyEpoch, Set<Message>> divide(Set<Message> msgs) {
        Map<DailyEpoch, Set<Message>> $ = new HashMap<>();
        for (DailyEpoch d : DailyEpoch.values()) {
            $.put(d, new HashSet<>());
        }
        for (Message m : msgs) {
            double hour = getHour(m.time);
            $.get(getEpoch(hour)).add(m);
        }
        return $;
    }

    private DailyEpoch getEpoch(double hour) {
        for (DailyEpoch de : DailyEpoch.values()) {
            if (de.start <= hour && de.end >= hour)
                return de;
        }
        return DailyEpoch.NIGHT;
    }

    private double getHour(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(time * 1000l));
        cal.setTimeZone(TimeZone.getTimeZone("EST"));
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    public static enum DailyEpoch {
        MORNING(7, 11), NOON(12, 16), EVENING(17, 20), NIGHT(21, 1), LATENIGHT(2, 6);

        public final int start, end;

        DailyEpoch(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public static void main(String[] args) throws IOException {
        InputHanlder reader = new InputHanlder();
//        Path input = Paths.get(InputFileNames.BASE_FOLDER).resolve("pizza");
        for (CentralPark6Regions inputFile : CentralPark6Regions.values()) {
            Path input = Paths.get(CentralPark6Regions.BASE_FOLDER).resolve(inputFile.name + "Clean");
            Set<Message> toDivide = reader.readFromMessages(input, 2000000);
            Map<DailyEpoch, Set<Message>> divide = new EpochDivider().divide(toDivide);
            for (DailyEpoch de : divide.keySet()) {
                reader.saveAsCSV(divide.get(de), Paths.get(CentralPark6Regions.BASE_FOLDER).resolve("daily").resolve
                        (input
                                .toFile().getName() + "_" + de.name().toLowerCase()));
            }
        }
    }


    public static enum WeeklyEpoch {
        WEEKDAYS(1, 5), WEEKEND(6, 7);

        public final int start, end;

        WeeklyEpoch(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public static enum QuaterlyEpoch {
        FIRST(1, 3), SECOND(4, 6), THIRD(7, 9), FOURTH(10, 12);

        public final int start, end;

        QuaterlyEpoch(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
}
