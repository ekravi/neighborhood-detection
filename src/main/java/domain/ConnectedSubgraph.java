package domain;

import domain.Grid.Cell;
import service.SeparatorStringBuilder;

import java.util.*;

/**
 * A connected subgrpah of cells
 */
public class ConnectedSubgraph {
    final Set<Cell> cells;
    final int[][] cellsMatrix;
    final Grid grid;

    public ConnectedSubgraph(Grid g, Set<Cell> cells) throws IllegalArgumentException {
        if (cells == null || cells.isEmpty())
            throw new IllegalArgumentException("Not a connected subgraph");

        int[][] cellsMat = initCellsMat(g, cells);
        this.cells = new HashSet<>(cells);
        this.cellsMatrix = cellsMat;
        this.grid = g;
        if (!verifyConnectedSubgraph(cells)) {
            throw new IllegalArgumentException("Not a connected subgraph");
        }
    }

    public ConnectedSubgraph(ConnectedSubgraph that) {
        this.cells = new HashSet<>(that.cells);
        this.cellsMatrix = new int[that.cellsMatrix.length][that.cellsMatrix[0].length];
        this.grid = that.grid;
        for (int i = 0; i < cellsMatrix.length; ++i) {
            System.arraycopy(that.cellsMatrix[i], 0, cellsMatrix[i], 0, cellsMatrix[i].length);
        }
    }

    /**
     * add cell if not already contained and its neighbors are in the matrix
     */
    public ConnectedSubgraph extend(Cell c) {
        // cannot add cell that already added or one without any neighbor
        if ((cellsMatrix[c.i][c.j] == 1) || !hasNeighbor(c))
            throw new IllegalArgumentException("illegal cell to add");
        cellsMatrix[c.i][c.j] = 1;
        cells.add(c);
        return this;
    }

    int[][] initCellsMat(Grid g, Set<Cell> cells) {
        int[][] $ = new int[g.numRows()][g.numColumns()];
        for (Cell c : cells) {
            $[c.i][c.j] = 1;
        }
        return $;
    }

    /**
     * Iterates the cells list and verify there is a neighbor in the cell
     */
    boolean verifyConnectedSubgraph(Set<Cell> cells) {
        if (cells.size() == 1)
            return true;
        for (Cell c : cells) {
            if (!hasNeighbor(c))
                return false;
        }
        return true;
    }

    private boolean hasNeighbor(Cell c) {
        return !getNeighbor(c).isEmpty();
    }

    private Set<Cell> getNeighbor(Cell c) {
        Set<Cell> $ = new HashSet<>();
        if (c.i > 0)
            if (cellsMatrix[c.i - 1][c.j] == 1)
                $.add(new Cell(c.i - 1, c.j));
        if (c.i < cellsMatrix.length - 1)
            if (cellsMatrix[c.i + 1][c.j] == 1)
                $.add(new Cell(c.i + 1, c.j));
        if (c.j > 0)
            if (cellsMatrix[c.i][c.j - 1] == 1)
                $.add(new Cell(c.i, c.j - 1));
        if (c.j < cellsMatrix[0].length - 1)
            if (cellsMatrix[c.i][c.j + 1] == 1)
                $.add(new Cell(c.i, c.j + 1));
        return $;
    }


    public int size() {
        return cells.size();
    }

    public String toString() {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\t');
        List<Cell> ordered = new ArrayList<>(cells);
        Collections.sort(ordered, new Comparator<Cell>() {
            @Override
            public int compare(Cell c1, Cell c2) {
                int iCmp = Integer.compare(c1.i, c2.i);

                if (iCmp != 0) return -iCmp;
                else return -Integer.compare(c1.j, c2.j);
            }
        });
        for (Cell c : cells
                ) {
            ssb.append(c);
        }
        return ssb.toString();
    }

    public boolean equals(Object that) {
        if (that == null) return false;
        return (this == that || this.equals((ConnectedSubgraph) that));
    }

    public boolean equals(ConnectedSubgraph that) {
        return (this.cells.equals(that.cells));
    }

    public int hashCode() {
        return cells.hashCode();
    }

    public Set<Cell> getCells() {
        return cells;
    }

    public boolean contains(Cell c) {
        return cellsMatrix[c.i][c.j] == 1;
    }

    /**
     * @return cells from the input contained within self
     */
    public Set<Cell> intersects(Set<Cell> input) {
        Set<Cell> $ = new HashSet<>();
        for (Cell c : input) {
            if (contains(c)) $.add(c);
        }
        return $;
    }

    public Grid getGrid() {
        return grid;
    }

    /**
     * @return neighbors of self within the grid
     */
    public Set<Cell> getNeighbors() {
        Set<Cell> $ = new HashSet<>();
        for (Cell c : cells) {
            if (c.i > 0)
                if (cellsMatrix[c.i - 1][c.j] == 0)
                    $.add(new Cell(c.i - 1, c.j));
            if (c.i < cellsMatrix.length - 1)
                if (cellsMatrix[c.i + 1][c.j] == 0)
                    $.add(new Cell(c.i + 1, c.j));
            if (c.j > 0)
                if (cellsMatrix[c.i][c.j - 1] == 0)
                    $.add(new Cell(c.i, c.j - 1));
            if (c.j < cellsMatrix.length - 1)
                if (cellsMatrix[c.i][c.j + 1] == 0)
                    $.add(new Cell(c.i, c.j + 1));
        }
        return $;
    }
}
