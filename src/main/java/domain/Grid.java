package domain;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Grid {
    private final double[][] cells;
    private final int rows, columns;

    public Grid(int rows, int column) {
        this.rows = rows;
        columns = column;
        cells = new double[rows][column];
    }

    public List<Cell> getCells() {
        List<Cell> $ = new ArrayList<>();
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < columns; ++j) {
                $.add(new Cell(i, j));
            }
        }
        return $;
    }

    public List<Cell> getNeighbors(Cell c) {
        List<Cell> $ = new LinkedList<>();
        if (c.i > 0)
            $.add(new Cell(c.i - 1, c.j));
        if (c.i < (rows - 1))
            $.add(new Cell(c.i + 1, c.j));
        if (c.j > 0)
            $.add(new Cell(c.i, c.j - 1));
        if (c.i < (columns - 1))
            $.add(new Cell(c.i, c.j + 1));
        return $;
    }

    public int numRows() {
        return this.rows;
    }

    public int numColumns() {
        return this.columns;
    }

    public static class Cell {
        public final int i;
        public final int j;

        public Cell(int i, int j) {
            if (i < 0 || j < 0) throw new IllegalArgumentException("smaller than 0");
            this.i = i;
            this.j = j;
        }

        public boolean equals(Object that) {
            if (that == null) return false;
            return ((this == that)) || (this.equals((Cell) that));
        }

        public boolean equals(Cell that) {
            return (this.i == that.i && this.j == that.j);
        }

        public int hashCode() {
            return Integer.hashCode(i) + Integer.hashCode(j);
        }

        public String toString() {
            return "(" + String.valueOf(i) + "," + String.valueOf(j) + ")";
        }
    }
}
