package domain;

import dataManipulations.ir.MessagesExtractor;
import domain.Grid.Cell;
import ir.LM;
import ir.LMPersist;
import ir.PersistEDLM;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

/**
 * Created by ekravi on 30/05/2017.
 */
public class TextualCell extends Cell implements TextualContainer {

    public final LM lm;

    public TextualCell(int i, int j) {
        super(i, j);
        lm = new PersistEDLM(2);
    }

    public TextualCell(int i, int j, LM lm) throws IllegalArgumentException {
        super(i, j);
        this.lm = lm;
    }

    public void setPayload(Set<Message> within) throws IOException {
        lm.train(new MessagesExtractor().getTexts(within));
    }

    public void setPayload(Path toLoad) throws IOException {
        ((LMPersist) lm).load(toLoad);
    }

    @Override
    public LM getLM() {
        return lm;
    }
}
