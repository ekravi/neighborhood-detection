package domain;

import ir.LM;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

/**
 * Created by ekravi on 30/05/2017.
 */
public interface TextualContainer {

    public void setPayload(Set<Message> within) throws IOException;

    public void setPayload(Path toLoad) throws IOException;

    public LM getLM();
}
