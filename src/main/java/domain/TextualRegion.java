package domain;

import dataManipulations.ir.MessagesExtractor;
import ir.LM;
import ir.LMAggregator;
import ir.LMPersist;
import ir.PersistEDLM;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

/**
 * A region is a connected subgraph with a payload
 */
public class TextualRegion extends ConnectedSubgraph implements TextualContainer {

    public final LM lm;

    public TextualRegion(Grid g, Set<TextualCell> cells) throws IllegalArgumentException {
        super(g, (Set) cells);
        lm = new PersistEDLM(2);
        ((LMAggregator) lm).aggregate(collectLMs());
    }

    private Set<LM> collectLMs() {
        Set<LM> $ = new HashSet<>();
        Set<TextualCell> containedCells = (Set) this.cells;
        for (TextualCell tc : containedCells) {
            $.add(tc.getLM());
        }
        return $;
    }

    public TextualRegion(Grid g, Set<TextualCell> cells, LM lm) throws IllegalArgumentException {
        super(g, (Set) cells);
        this.lm = lm;
    }

    public void setPayload(Set<Message> within) throws IOException {
        lm.train(new MessagesExtractor().getTexts(within));
    }

    public void setPayload(Path toLoad) throws IOException {
        ((LMPersist) lm).load(toLoad);
    }

    @Override
    public LM getLM() {
        return lm;
    }
}
