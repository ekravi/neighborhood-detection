package domain.experiments;

import dataManipulations.settings.SettingsReader;

import java.nio.file.Path;

public class InitialSplit extends Run {
    public final String initialFile;

    public InitialSplit(Path resourceFolder, Path runFolder, Path resultsFolder, String name, int gridWidth, int
            splitSize, int numSteps, int numRuns, SettingsReader.RUN_TYPE runType, String initialFile) {
        super(resourceFolder, runFolder, resultsFolder, name, gridWidth, splitSize, numSteps, numRuns, runType);
        this.initialFile = initialFile;
    }
}
