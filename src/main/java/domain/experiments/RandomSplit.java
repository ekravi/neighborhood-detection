package domain.experiments;

import dataManipulations.settings.SettingsReader;

import java.nio.file.Path;

public class RandomSplit extends Run {


    public RandomSplit(Path resourceFolder, Path runFolder, Path resultsFolder, String name, int gridWidth, int
            splitSize, int numSteps, int numRuns, SettingsReader.RUN_TYPE runType) {
        super(resourceFolder, runFolder, resultsFolder, name, gridWidth, splitSize, numSteps, numRuns, runType);
    }
}
