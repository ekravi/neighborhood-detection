package domain.experiments;

import dataManipulations.settings.SettingsReader;

import java.nio.file.Path;

public class Run {
    public final Path resourceFolder, runFolder, resultsFolder;
    public final String name;
    public final int gridWidth, splitSize, numSteps, numRuns;
    public final SettingsReader.RUN_TYPE runType;

    public Run(Path resourceFolder, Path runFolder, Path resultsFolder, String name, int gridWidth, int splitSize,
               int numSteps, int numRuns, SettingsReader.RUN_TYPE runType) {
        this.resourceFolder = resourceFolder;
        this.runFolder = runFolder;
        this.resultsFolder = resultsFolder;
        this.name = name;
        this.gridWidth = gridWidth;
        this.splitSize = splitSize;
        this.numSteps = numSteps;
        this.numRuns = numRuns;
        this.runType = runType;
    }
}
