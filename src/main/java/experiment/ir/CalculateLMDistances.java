package experiment.ir;

import ir.LM;
import ir.PersistEDLM;
import ir.distance.KullbackLeiblerDivergenceCalculator;
import statistics.Histogram;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CalculateLMDistances {
    private final KullbackLeiblerDivergenceCalculator dist = new KullbackLeiblerDivergenceCalculator();

    public void printDistance(Path from, Path to) throws IOException {
        LM fromLM = new PersistEDLM().load(from), toLM = new PersistEDLM().load(to);

        System.out.println(new StringBuilder("total distance: ").append(dist.getDistance(fromLM, toLM)).toString());
        Histogram<String> topKEntries = dist.getTopKEntries(fromLM, toLM, 100);
        System.out.println(topKEntries.toString());
    }


    public static void main(String[] args) throws IOException {
        Path baseDir = Paths.get("src/main/resources/runningConfigurations/lms");
        Path to = baseDir.resolve("initialNorthSouth/lm1.lm"), from = baseDir.resolve("fixedNorthSouth/lm3.lm");
        new CalculateLMDistances().printDistance(from, to);
    }
}
