package experiment.ir;

import domain.Grid;
import domain.Grid.Cell;
import domain.Message;

import java.util.HashSet;
import java.util.Set;

/**
 * 1) create a grid
 * 2) assign messages to each cell
 * 3) create a lm for each cell
 * 4) return a region for each cell
 * <p>
 * 1) create a grid
 * 2) assign messages to each cell
 * 3) create a lm for each cell
 * 4) cluster according to lm
 */
public class GridBuilder {

    public Grid buildGrid(Set<Message> from, int rows, int columns) {
        Grid g = new Grid(rows, columns);

        for (Cell c : g.getCells()) {
            Set<Cell> region = new HashSet<Cell>();
            region.add(c);
        }
        return g;
    }

}
