package experiment.ir;

import dataManipulations.files.InputHanlder;
import dataManipulations.ir.MessagesExtractor;
import dataManipulations.settings.CentralPark6Regions;
import domain.Message;
import ir.PersistEDLM;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by ekravi on 16/05/2017.
 */
public class PerplexityCalculator {


    public static void main(String[] args) throws IOException {
        InputHanlder reader = new InputHanlder();
        for (CentralPark6Regions inputFile : CentralPark6Regions.values()) {
            Path input = Paths.get(CentralPark6Regions.BASE_FOLDER).resolve(inputFile.name + "Clean");
            Set<Message> toProcess = reader.readFromMessages(input, 2000000);
            double perplexity = new PerplexityCalculator().calcRegionsPerplexity(toProcess);
            System.out.println(inputFile.name + " " + String.valueOf(perplexity));
        }
    }

    public double calcRegionsPerplexity(Set<Message> toProcess) throws IOException {

        List<List<String>> texts = new LinkedList<>(new MessagesExtractor().getTexts(toProcess));
        // build LM
        int to = (int) (texts.size() * 0.9);
        PersistEDLM lm = new PersistEDLM(2);

        lm.train(new HashSet<>(texts.subList(0, to)));
        // save LM?
//        lm.persist(null);// TODO: add path
        // get Perplexity
        return lm.getPerplexity(new HashSet<>(texts.subList(to, texts.size())));
    }
}
