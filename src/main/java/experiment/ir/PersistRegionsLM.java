package experiment.ir;

import dataManipulations.files.InputHanlder;
import dataManipulations.ir.MessagesExtractor;
import dataManipulations.settings.CentralPark6Regions;
import domain.Message;
import ir.PersistEDLM;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

/**
 * Save the LM of each original region
 */
public class PersistRegionsLM {
    public void persistLMs() throws IOException {
        InputHanlder reader = new InputHanlder();
        for (CentralPark6Regions inputFile : CentralPark6Regions.values()) {
            Path input = Paths.get(CentralPark6Regions.BASE_FOLDER).resolve(inputFile.name + "Clean");
            Set<Message> toProcess = reader.readFromMessages(input, 2000000);
            Set<List<String>> texts = new MessagesExtractor().getTexts(toProcess);

            PersistEDLM lm = new PersistEDLM();
            lm.train(texts);
            lm.persist(Paths.get(CentralPark6Regions.BASE_FOLDER).resolve("lm").resolve(input.toFile().getName() +
                    "LM"));
        }
    }


    public static void main(String[] args) throws IOException {
        new PersistRegionsLM().persistLMs();
    }
}
