package experiments;

import algorithms.localSearch.IBetterNeighborSplitFinder;
import algorithms.splits.InitialSplit.IRegionSplit;
import algorithms.splits.InitialSplit.InitialRegionSplit;
import algorithms.splits.InitialSplit.RandomRegionSplit;
import algorithms.splits.evaluation.IStepEvaluator;
import algorithms.splits.evaluation.SingleLDASplitEvaluator;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.domain.ExperimentResults;
import experiments.domain.HCExperimentResults;
import experiments.domain.HCRunResults;
import experiments.domain.StepResults;
import experiments.settings.ALG_TYPE;
import experiments.settings.DefaultSettings;
import org.apache.commons.io.IOUtils;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.GeometryBuilder;
import service.SeparatorStringBuilder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ekravi on 17/07/2017.
 */
public class BestSplitFinder {
    private static final boolean WIDTH_OPTIMIZATION = true;
    private final Set<Message> trainMsgs;
    private Set<Message> testMsgs;

    private GridService gridService = new GridService();
    private GeometryJSON debugger = new GeometryJSON();
    private final Path runConfDir;
    private String name;

    /**
     * Assuming the set of messages is contained within the evaluated split
     */
    public BestSplitFinder(Set<Message> trainMsgs, Path runConfDir) {
        this.trainMsgs = Collections.unmodifiableSet(trainMsgs);
        this.runConfDir = runConfDir;
    }

    public BestSplitFinder(Set<Message> trainMsgs, Set<Message> testMsgs, Path runConfDir) {
        this.trainMsgs = Collections.unmodifiableSet(trainMsgs);
        this.testMsgs = testMsgs;
        this.runConfDir = runConfDir;
    }

    /**
     * Running an experiment (#runs x #steps) starting from a <strong>random</strong> split
     */
    HCExperimentResults findFromRandomSolution(List<Polygon> intersectingCells, IStepEvaluator heur,
                                               IBetterNeighborSplitFinder bNFinder, int splitSize,
                                             /*int numRuns, */int maxStepsOfSingleRun) throws IOException {
//        long startTime = new Date().getTime();
        IRegionSplit rSplit = new RandomRegionSplit();

//        List<List<Polygon>> bestSolution = null;
//        double bestScore = -Double.MAX_VALUE;
//        List<Double> bestScores = new ArrayList<>(numRuns);

//        for (int run = 0; run < numRuns; ++run) {
//            System.out.println("run #" + (run + 1) + ", start score: " + bestScore);
        List<List<Polygon>> initialSolution = rSplit.splitToConnectedSubgraphs(intersectingCells,
                splitSize);

        HCRunResults rRes = singleRun(initialSolution, maxStepsOfSingleRun, /*intersectingCells, heur,*/bNFinder);
//            bestScores.add(rRes.finalScore);
//            if (rRes.finalScore > bestScore) {
//                bestSolution = rRes.solution;
//                bestScore = rRes.finalScore;
//            }
//            System.out.println("run #" + (run + 1) + ", best score: " + rRes.finalScore);
//        }

        String algorithmName = new SeparatorStringBuilder('\t').append
                (rSplit.getName()).append
                (bNFinder.getName()).toString();
        return new HCExperimentResults(algorithmName, initialSolution, rRes.solution, rRes.baseScore, rRes.finalScore,
                rRes.overallSteps, rRes.avgAttemptsUntilImprovement, rRes.runningTime);
    }

    /**
     * Running an experiment (#runs x #steps) starting from a <strong>given</strong> split
     */
    HCExperimentResults findFromAGivenSolution(List<List<Polygon>> givenSolution, String gSolName, IStepEvaluator
            heur, IBetterNeighborSplitFinder bNFinder, /*int numRuns,*/ int maxStepsOfSingleRun) throws IOException {

//        long startTime = new Date().getTime();

//        List<List<Polygon>> bestSolution = null;
//        double bestScore = -Double.MAX_VALUE;
//        List<Double> bestScores = new ArrayList<>(numRuns);

//        for (int run = 0; run < numRuns; ++run) {
//            System.out.println("run #" + (run + 1) + ", start score: " + bestScore);
        HCRunResults rRes = singleRun(givenSolution, maxStepsOfSingleRun, bNFinder);

//            bestScores.add(rRes.finalScore);
//            if (rRes.finalScore > bestScore) {
//                bestSolution = rRes.solution;
//                bestScore = rRes.finalScore;
//            }
//            System.out.println("run #" + (run + 1) + ", best score: " + rRes.finalScore);
//        }

        String algorithmName = new SeparatorStringBuilder('\t').append
                (gSolName).append
                (bNFinder.getName()).toString();
        return new HCExperimentResults(algorithmName, givenSolution, rRes.solution, rRes.baseScore, rRes.finalScore,
                rRes.overallSteps, rRes.avgAttemptsUntilImprovement, rRes.runningTime);

    }

    /**
     * single run - iteratively searching for a local maximum for maxSteps
     * A run includes improving steps until reaching to steps limits or no more improvement is reached
     */
    private HCRunResults singleRun(List<List<Polygon>> solution, int maxSteps, IBetterNeighborSplitFinder
            bNFinder)
            throws IOException {
        long startTime = new Date().getTime();
        List<List<Polygon>> tmpSolution = new ArrayList<>(solution);
        StepResults sRes = null;
        IStepEvaluator heur = bNFinder.getHeur();
        double baseScore = heur.evaluate(solution);
        int overallSteps = maxSteps, minAttempts = Integer.MAX_VALUE, maxAttempts = Integer.MIN_VALUE;
        double attemptsCount = 0;


        for (int i = 0; i < maxSteps; ++i) {
            if (i % 10 == 0)
                System.out.println("step " + (i + 1));
            sRes = bNFinder.getImprovedNeighbor(tmpSolution);

            minAttempts = Math.min(minAttempts, sRes.attemptsUntilImprovement);
            maxAttempts = Math.max(maxAttempts, sRes.attemptsUntilImprovement);
            attemptsCount += sRes.attemptsUntilImprovement;

            if (sRes.solution != null) {
                tmpSolution = sRes.solution;
//                System.out.println(" found improved solution :)");
            } else {
//                System.out.println(" approached local maximum");
                solution = tmpSolution;
                overallSteps = i;
                break;
            }
        }

        double finalScore = heur.evaluate(solution);
        HCRunResults rRes = new HCRunResults(solution, overallSteps, maxAttempts, minAttempts, attemptsCount /
                overallSteps, baseScore, finalScore, new Date().getTime() - startTime);

        System.out.println(new SeparatorStringBuilder('\t').append("baseScore").append("finalScore").append
                ("overallSteps").append("minAttempts").append("maxAttempts").append("avgAttempts").append
                ("runningTime " +
                        "(ms)").toString());
        System.out.println(new SeparatorStringBuilder('\t').append(baseScore).append(finalScore).append
                (overallSteps)
                .append(minAttempts).append(maxAttempts).append(rRes.avgAttemptsUntilImprovement).append(rRes
                        .runningTime).toString());
        return rRes;
    }

    private HCExperimentResults initialSplit(String initialFile, List<Polygon> grid, int maxSteps,
                                             SeparatorStringBuilder title, SeparatorStringBuilder values, IStepEvaluator
                                                     heur, IBetterNeighborSplitFinder bNFinder) throws IOException {
        Reader jsonReader2 = new FileReader(runConfDir.resolve(initialFile).toFile());
        GeometryCollection initialSolution = new GeometryJSON()
                .readGeometryCollection(jsonReader2);


        String initialSolName = getFileName(initialFile);
        List<Polygon> intersectingCells = new LinkedList<>();
        List<List<Polygon>> givenSplit = new InitialRegionSplit().loadInitialSolution(grid, initialSolution,
                intersectingCells);

        System.out.println(title.append("grid size").append("# of intersecting cells").toString());
        System.out.println(values.append(grid.size())
                .append(intersectingCells.size()).toString());


        heur.init(new HashSet<>(intersectingCells), trainMsgs);

        return findFromAGivenSolution(givenSplit, initialSolName, heur,
                bNFinder, maxSteps);
    }

    private HCExperimentResults randomSplit(int splitSize, List<Polygon> intersectingCells, int
            maxSteps, SeparatorStringBuilder title, SeparatorStringBuilder values, IStepEvaluator heur, List<Polygon>
                                                    grid, IBetterNeighborSplitFinder bNFinder) throws IOException {
        System.out.println(title.append("grid size").append("# of intersecting cells").toString());
        System.out.println(values.append(grid.size()).append(intersectingCells.size()).toString());

        heur.init(new HashSet<>(intersectingCells), trainMsgs);

        return findFromRandomSolution(intersectingCells, heur, bNFinder, splitSize,
                maxSteps);
    }

    private HCExperimentResults findBestSplit(String[] args, int splitSize, int maxSteps, Polygon p, EXP_TYPE
            exp_type, ALG_TYPE algType, int cellWidth) throws IOException {
        Set<Message> all = new HashSet<>(trainMsgs);
        all.addAll(testMsgs);

        List<Polygon> grid = gridService.generateGrid(all, cellWidth);

        debugger.write(gridService.getMultiPolygon(grid), System.out);
        System.out.println();

        SeparatorStringBuilder title = new SeparatorStringBuilder('\t').append("expe type").append("alg type")
                .append("cell size").append("split size").append("max steps"), values = new
                SeparatorStringBuilder('\t').append(exp_type).append(algType).append
                (cellWidth).append(splitSize).append(maxSteps);

        IStepEvaluator heur = new SingleLDASplitEvaluator(new HashSet<>(IOUtils.readLines(new FileReader(runConfDir
                .resolve("stopwords.txt").toFile()))));
//        heur.init(new HashSet<>(gridService.getIntersectingCells(grid, p)), trainMsgs);
        //LMSplitEvaluator
        // (DefaultSettings
        // .LM_ORDER);

        HCExperimentResults eRes = null;

        IBetterNeighborSplitFinder bNFinder = algType.getSplitAlg(heur);

        SeparatorStringBuilder sbName = new SeparatorStringBuilder('_').append(exp_type).append(algType
        ), sbSettings = new SeparatorStringBuilder('_').append(splitSize).append(maxSteps).append(args[2]).append(new
                Date().getTime());


        if (exp_type.equals(EXP_TYPE.RANDOM)) { // random split
            name = sbName.append(sbSettings).toString();
            eRes = randomSplit(splitSize, gridService.getIntersectingCells(grid, p),
                    maxSteps, title, values, heur, grid, bNFinder);
        } else { // initial split
            String initialFile = args[7], initialFileName = getFileName(initialFile);
            name = sbName.append(initialFileName).append(sbSettings)
                    .toString();
            eRes = initialSplit(initialFile, grid, maxSteps, title, values, heur, bNFinder);
        }
        return eRes;
    }

    private String getFileName(String initialFile) {
        return initialFile.substring(0, initialFile.indexOf(".json"));
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 7 || args.length > 8)
            throw new IllegalArgumentException("app <random/initial> <run folder> <prefix> <split size> <max steps> " +
                    "<first/stoc/steep> <resOpt> <initial file>?");
        // random src/main/resources/runningConfigurations/WESideNCentralPark/ WESideNCentralPark 3 1000000 first
        // random ~/data/runningConfigurations/WESideNCentralPark/ WESideNCentralPark 3 1000000 STOC
        // initial ~/data/ ~/data/runningConfigurations/WESideNCentralPark/ 3 1000000 STEEP
        // initialSplitEastWest.json
        Path runConfDir = Paths.get(args[1]);


        Set<Message> train = new InputHanlder().readFromMessages(runConfDir.resolve(args[2] + "Train.csv"), 1000000);
        Set<Message> test = new InputHanlder().readFromMessages(runConfDir.resolve(args[2] + "Test.csv"), 1000000);

        Reader jsonReader = new FileReader(runConfDir.resolve("boundingPolygon.json").toFile());
        Polygon p = new GeometryJSON().readPolygon(jsonReader);

        System.out.println(new SeparatorStringBuilder('\t').append("train").append("visualization").toString());
        System.out.println(new SeparatorStringBuilder('\t').append(train.size()).append(test.size()).toString());

        BestSplitFinder finder = new BestSplitFinder(train, test, runConfDir);
        finder.runExperiment(args, /* split size */ Integer.valueOf(args[3]),
                /* max steps */ Integer.valueOf(args[4]), p, EXP_TYPE.valueOf(args[0].toUpperCase()), ALG_TYPE
                        .valueOf(args[5].toUpperCase()), Boolean.valueOf(args[6]));
    }

    private void runExperiment(String[] args, int splitSize, int maxSteps, Polygon p, EXP_TYPE
            expType, ALG_TYPE algType, boolean resOpt) throws IOException {
        ExperimentResults eRes = null;


        if (resOpt == true) {
            eRes = findBestSplit(args, splitSize, maxSteps, p, expType, algType, DefaultSettings
                    .CELL_WIDTH_ZOOMING[0]);
            for (int i = 1; i < DefaultSettings.CELL_WIDTH_ZOOMING.length; ++i) {
                // TODO: debug
                for (int j = 0; j < eRes.finalSolution.size(); ++j) {
                    System.out.println(new GeometryJSON().toString(new GridService().getMultiPolygon(eRes
                            .finalSolution.get(j))));
                }


                int cellWidth = DefaultSettings.CELL_WIDTH_ZOOMING[i];

                // split to finer sizes
                List<List<Polygon>> finerSolution = getFinerSolution(eRes.finalSolution, cellWidth);

                String[] newArgs = Arrays.copyOf(args, 7);
                newArgs[6] = getTmpInitialFile(eRes.finalSolution, cellWidth);
                eRes = findBestSplit(newArgs, splitSize, maxSteps, p, EXP_TYPE.INITIAL, algType, cellWidth);

            }
        } else {
            int cellWidth = DefaultSettings.CELL_WIDTH_IN_METERS;
            eRes = findBestSplit(args, splitSize, maxSteps, p, expType, algType, cellWidth);
        }

        new ExperimentSaver().saveFinalSolution(runConfDir, name, eRes, trainMsgs, testMsgs);

        // save results
    }

    // returns a translation of the privious solution in the given width
    private List<List<Polygon>> getFinerSolution(List<List<Polygon>> finalSolution, int cellWidth) throws IOException {
        List<MultiPolygon> mps = new GridService().getMultiPolygons(finalSolution);
        List<Polygon> newGrid = new GridService().generateGrid(finalSolution, cellWidth);

        List<List<Polygon>> $ = new ArrayList<>(finalSolution.size());
        for (int i = 0; i < mps.size(); ++i) {
            $.add(new LinkedList<>());
        }
        for (Polygon p : newGrid) {
            for (MultiPolygon mp : mps) {
                if (mp.contains(p)) {
                    $.get(mps.indexOf(mp)).add(p);
                    break;
                }
            }
        }
        return $;
    }

    private String getTmpInitialFile(List<List<Polygon>> finalSolution, int cellWidth) throws IOException {
        Path path = Files.createTempFile(runConfDir, "tmpSolution", String.valueOf(cellWidth) + ".json");
        GeometryCollection collection = getGeometryCollection(finalSolution);

        File file = path.toFile();
        file.deleteOnExit();

        new GeometryJSON().write(collection, new FileOutputStream(file));
        return file.getName();
    }

    private GeometryCollection getGeometryCollection(List<List<Polygon>> finalSolution) {
        MultiPolygon[] split = new MultiPolygon[finalSolution.size()];
        for (int i = 0; i < finalSolution.size(); ++i) {
            Polygon[] arr = new Polygon[finalSolution.get(i).size()];
            split[i] = new GeometryBuilder().multiPolygon(finalSolution.get(i).toArray(arr));
        }
        return new GeometryBuilder().geometryCollection(split);
    }

    private String getTmpFile(List<MultiPolygon> split, String suffix, Set<Message> trainMsgs) throws IOException {
        Path path = Files.createTempFile("msgsAssgnmnt", suffix);
//        appendMessages(split, path, trainMsgs);

        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        File file = path.toFile();
        file.deleteOnExit();
        return file.getAbsolutePath();
    }

    public static enum EXP_TYPE {
        RANDOM, INITIAL
    }
}
