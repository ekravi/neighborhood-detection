package experiments;

import algorithms.splits.InitialSplit.InitialRegionSplit;
import algorithms.splits.evaluation.ISplitEvaluator;
import algorithms.splits.evaluation.LDASplitEvaluator;
import algorithms.splits.evaluation.LMSplitEvaluator;
import algorithms.splits.evaluation.SingleLDASplitEvaluator;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.domain.ExperimentResults;
import experiments.domain.HCExperimentResults;
import experiments.settings.DefaultSettings;
import ir.EDLM;
import ir.LM;
import ir.LMPersist;
import ir.PersistEDLM;
import org.apache.commons.io.IOUtils;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.GeometryBuilder;
import service.SeparatorStringBuilder;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ExperimentSaver {
    public void saveFinalSolution(Path runConfDir, String expName, ExperimentResults eRes, Set<Message> trainSet,
                                  Set<Message> testSet) throws IOException {
        Path expOutDir = runConfDir.resolve(expName + "_results");
        Files.createDirectory(expOutDir);
        List<ISplitEvaluator> evaluators = getEvalList(trainSet, testSet, runConfDir, eRes.finalSolution);

        saveSplitGeoJSON(eRes, expOutDir);
        for (ISplitEvaluator eval : evaluators) {
            if (eval instanceof LMSplitEvaluator) {
                saveSplitLMs((LMSplitEvaluator) eval, eRes.finalSolution, expOutDir);
                break;
            }
        }
        saveMetrics(evaluators, eRes, trainSet, testSet, expOutDir, expName);
    }

    private List<ISplitEvaluator> getEvalList(Set<Message> trainSet, Set<Message> testSet, Path runConfDir, List<List
            <Polygon>> solution) throws
            IOException {
        List<ISplitEvaluator> $ = new LinkedList<>();


        Set<Polygon> polygons = new HashSet<>();
        for (List<Polygon> region : solution) {
            polygons.addAll(region);
        }
//        LMSplitEvaluator lmEval = new LMSplitEvaluator(DefaultSettings.LM_ORDER);
//        lmEval.init(polygons, testSet);
//        $.add(lmEval);

        HashSet<String> stopwords = new HashSet<>(IOUtils.readLines(new FileReader(runConfDir.resolve
                ("stopwords.txt").toFile())));

        SingleLDASplitEvaluator singleLdaEval =
                new SingleLDASplitEvaluator(stopwords);
        $.add(singleLdaEval);

        LDASplitEvaluator ldaEval = new LDASplitEvaluator(stopwords, DefaultSettings.LDA_NUM_TOPICS, DefaultSettings
                .LDA_NUM_ITERATIONS);
        $.add(ldaEval);

//        $.add(new MLSplitEvaluator(trainSet, testSet));
//        $.add(new PerplexityEvaluator(DefaultSettings.LM_ORDER));
//
//        $.add(new TopTermsSplitEvaluator());
//        $.add(new PresentationComplexity());

        return $;
    }

    /**
     * Save values generated for each evaluators over test set
     */
    private void saveMetrics(List<ISplitEvaluator> evaluators, ExperimentResults eRes, Set<Message>
            trainSet, Set<Message> testSet, Path runConfDir, String expName) throws IOException {
        SeparatorStringBuilder lines = new SeparatorStringBuilder('\n');
        SeparatorStringBuilder title = new SeparatorStringBuilder(',').append("exp name").append("running time"),
                scores = new SeparatorStringBuilder(',').append(expName).append(eRes.runningTime);

        for (ISplitEvaluator eval : evaluators) {
            title.append(eval.getName());

            eval.train(eRes.finalSolution, trainSet);
            scores.append(eval.test(eRes.finalSolution, testSet));
        }
        title.append("base score").append("final score").append("overall steps").append("avg attempts to " +
                "improvement");
        if (eRes instanceof HCExperimentResults) {
            HCExperimentResults res = (HCExperimentResults) eRes;
            scores.append(res.baseScore).append(res.finalScore).append(res.overallSteps).append(res
                    .avgAttmptsToImprovement);
        }

        Path metricsFile = runConfDir.resolve("../metrics.txt");
        if (!Files.exists(metricsFile)) {
            Files.createFile(metricsFile);
            lines.append(title);
        }
        Files.write(metricsFile, (lines.append(scores).toString() + '\n').getBytes(), StandardOpenOption.APPEND);
    }

    /**
     * Save the language model generated for every split
     */
    private void saveSplitLMs(LMSplitEvaluator heur, List<List<Polygon>> finalSolution, Path resultDir) throws
            IOException {
        List<LM> lms = heur.getAggregatedLMs(finalSolution);
        for (LM lm : lms) {
            LMPersist pLM = new PersistEDLM((EDLM) lm);
            pLM.persist(resultDir.resolve("lm" + (lms
                    .indexOf(lm) + 1) + ".lm"));
        }
    }

    /**
     * Save geo-json of the solution
     */
    private void saveSplitGeoJSON(ExperimentResults eRes, Path expDir) throws IOException {
        if (eRes instanceof HCExperimentResults) {
            saveSolution(expDir, "initial", ((HCExperimentResults) eRes).initialSolution);
        }
        saveSolution(expDir, "solution", eRes.finalSolution);
    }

    private void saveSolution(Path expDir, String name, List<List<Polygon>> solution) throws IOException {
        Geometry[] split = new Geometry[solution.size()];
        for (int i = 0; i < solution.size(); ++i) {
            GridService gs = new GridService();
            split[i] = gs.getMultiPolygon(solution.get(i));
        }
        GeometryCollection c = new GeometryBuilder().geometryCollection(split);

        new GeometryJSON().writeGeometryCollection(c, Files.newOutputStream(expDir.resolve(new
                SeparatorStringBuilder('_').append(name).toString() + ".json")));
    }

    public static void main(String[] args) throws IOException {
        args = new String[]{"src/main/resources/runningConfigurations/WESideNCentralPark", "WESideNCentralPark"};
        Path runConfDir = args[0] != null ? Paths.get(args[0]) : DefaultSettings.resourceFolder;
        List<Polygon> intersectingCells = new LinkedList<>();
        Set<Message> trainSet = new InputHanlder().readFromMessages(runConfDir.resolve(args[1] + "Train" +
                        ".csv"),
                1000000);
        Set<Message> testSet = new InputHanlder().readFromMessages(runConfDir.resolve(args[1] + "Test.csv"),
                1000000);

        List<Polygon> grid = new GridService().generateGrid(trainSet, 100);
        Polygon[] arr = new Polygon[grid.size()];


        List<List<Polygon>> solution = new InitialRegionSplit().loadInitialSolution(grid, new GeometryJSON()
                .readGeometryCollection(new FileInputStream(runConfDir.resolve("initialSplitNorthSouth.json").toFile
                        ())), intersectingCells);

        new ExperimentSaver().saveFinalSolution(runConfDir, "initial_ns_5_topics", new
                ExperimentResults("fixed", solution, -1), trainSet, testSet);
//        new ExperimentSaver().saveFinalSolution(neighborhoods, "fixedNeighborhoods", new
//                ExperimentResults("fixed", solution, -1), trainSet, testSet);
//        System.out.println(new PresentationComplexity().test(solution, testSet));
//        LDASplitEvaluator ldaEval = new LDASplitEvaluator(trainSet, runConfDir.resolve("../.."));
//        ldaEval.setNumTopics(DefaultSettings.LDA_NUM_TOPICS);
//        ldaEval.train(solution,trainSet);
//ldaEval.test(solution,testSet);

//        ExperimentSaver es = new ExperimentSaver();
//        ExperimentResults res = new
//                ExperimentResults("rnd_steep", solution, -1);
//        es.saveMetrics(es.getEvalList(trainSet, testSet, runConfDir), res,
//                trainSet, testSet, runConfDir, "rnd_steep");

    }
}
