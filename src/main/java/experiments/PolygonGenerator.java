package experiments;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.settings.DefaultSettings;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.GeometryBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class PolygonGenerator {
    public Polygon generate(List<Polygon> region, int minCells, int maxCells) {
        int numCells = new Random().nextInt(maxCells - minCells) + minCells;
        System.out.println(numCells);
        Collections.shuffle(region);

        List<Polygon> chosen = region.subList(0, numCells);
        Polygon[] arr = new Polygon[chosen.size()];

        return (Polygon) new GeometryBuilder().geometryCollection(chosen.toArray(arr)).convexHull();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        Path dataFolder = args.length > 0 ? Paths.get(args[0]) : DefaultSettings.resourceFolder;

        Set<Message> msgs = new InputHanlder().readFromMessages(dataFolder.resolve(DefaultSettings.manhattanMessages),
                15000000);
        MultiPolygon mp = new GeometryJSON().readMultiPolygon(new FileReader(dataFolder.resolve(DefaultSettings
                .manhattanIntersectingCells).toFile()));
        List<Polygon> intersecting = new ArrayList<>(mp.getNumGeometries());
        for (int i = 0; i < mp.getNumGeometries(); ++i) {
            intersecting.add((Polygon) mp.getGeometryN(i));
        }

        Polygon p = new PolygonGenerator().generate(intersecting, DefaultSettings.MIN_RAND_CELLS, DefaultSettings
                .MAX_RAND_CELLS);

        Path destDir = dataFolder.resolve("runningConfigurations").resolve("polygons").resolve
                ("ManhattanRandomArea" + new Date().toString());
        Files.createDirectory(destDir);
        new GeometryJSON().write(p, Files.newOutputStream(destDir.resolve(DefaultSettings.POLYGON_FILE_NAME)));
        new TrainTestCreator().create(new GridService().getIntersectingMsgs(msgs, p), destDir, "randomManhattan");
    }
}
