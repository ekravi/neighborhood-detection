package experiments;

import algorithms.clustering.BaseSplitClusterer;
import algorithms.clustering.BottomUpClusterer;
import algorithms.splits.evaluation.LMSplitEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.domain.ExperimentResults;
import experiments.domain.RunResults;
import experiments.settings.DefaultSettings;
import org.geotools.geojson.geom.GeometryJSON;
import service.SeparatorStringBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SplitClustererFinder {

    private final Set<Message> trainMsgs, testMsgs;
    private GridService gridService = new GridService();
    private GeometryJSON debugger = new GeometryJSON();
    private final String name;
    private final Path runConfDir;

    public SplitClustererFinder(Set<Message> train, Set<Message> test, Path runConfDir) {
        this.trainMsgs = Collections.unmodifiableSet(train);
        this.testMsgs = Collections.unmodifiableSet(test);
        this.runConfDir = runConfDir;

        this.name = getClass().getSimpleName() + new Date().toString();
    }

    public void cluster(Integer k) throws IOException {
        Set<Message> all = new HashSet<>(trainMsgs);
        all.addAll(testMsgs);
        List<Polygon> grid = gridService.generateGrid(all, DefaultSettings.CELL_WIDTH_IN_METERS);

        Reader jsonReader = new FileReader(runConfDir.resolve(DefaultSettings.POLYGON_FILE_NAME).toFile());
        Polygon p = new GeometryJSON().readPolygon(jsonReader);

        System.out.println(new SeparatorStringBuilder('\t').append("train").append("visualization").toString());
        System.out.println(new SeparatorStringBuilder('\t').append(trainMsgs.size()).append(testMsgs.size()).toString
                ());

        ExperimentResults eRes = getFinalSplit(gridService.getIntersectingCells(grid, p), k);

        new ExperimentSaver().saveFinalSolution(runConfDir, name, eRes, trainMsgs, testMsgs);
    }

    private ExperimentResults getFinalSplit(List<Polygon> intersectingCells, Integer k) {

        LMSplitEvaluator heur = new LMSplitEvaluator(DefaultSettings.LM_ORDER);
        heur.init(new HashSet<>(intersectingCells), trainMsgs);
        BaseSplitClusterer sc = new BottomUpClusterer(heur);


        RunResults rRes = sc.cluster(new HashSet<>(intersectingCells), k);

        return new ExperimentResults(sc.getName(), rRes.solution, rRes.runningTime);
        //String algorithmName, List<List<Polygon>> initialSolution, List<List<Polygon>>
//        finalSolution, double baseScore, double finalScore,
//        int overallSteps, double avgAttemptsUntilImprovement, long runningTime)
    }

    public static void main(String[] args) throws IOException {
        Path runConfDir = Paths.get(args[0]);
        String prefix = args[1];
        Set<Message> train = new InputHanlder().readFromMessages(runConfDir.resolve(prefix + "Train.csv"), 1000000),
                test = new InputHanlder().readFromMessages(runConfDir.resolve(prefix + "Test.csv"), 1000000);


        new SplitClustererFinder(train, test, runConfDir).cluster(Integer.valueOf(args[2]));
    }
}
