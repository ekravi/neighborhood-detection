package experiments;

import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import domain.Message;
import experiments.settings.DefaultSettings;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;
import service.SeparatorStringBuilder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class TrainTestCreator {

    public static final String NAME_PREFIX = "manhattanClean";

    public void create(Set<Message> relevantMsgs, Path outDir, String namePrefix) throws IOException {

        List<Message> toSplit = new ArrayList<>(relevantMsgs);
        Collections.shuffle(toSplit);
        int toIdx = (int) Math.floor(toSplit.size() * DefaultSettings.TRAIN_PORTION);

        HashSet<Message> train = new HashSet<>(toSplit.subList(0, toIdx)), test = new HashSet<>(toSplit.subList
                (toIdx, toSplit.size()));

        saveMessages(outDir.resolve(namePrefix + "All.csv"), relevantMsgs);
        saveMessages(outDir.resolve(namePrefix + "Train.csv"), train);
        saveMessages(outDir.resolve(namePrefix + "Test.csv"), test);

        System.out.println(new SeparatorStringBuilder('\t').append("relevant messages").append
                ("train").append("visualization").toString());
        System.out.println(new SeparatorStringBuilder('\t').append(relevantMsgs.size()).append(train
                .size()).append(test.size()).toString());
    }

    private void saveMessages(Path out, Set<Message> relevantMsgs) throws IOException {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        int i = 0;

        Files.createFile(out);
        for (Message m : relevantMsgs) {
            if (++i % 100000 == 0) {
                Files.write(out, (ssb.toString() + '\n').getBytes(), StandardOpenOption.APPEND);
                ssb = new SeparatorStringBuilder('\n');
            }
            ssb.append(m.toString());
        }
        Files.write(out, (ssb.toString() + '\n').getBytes(), StandardOpenOption.APPEND);
    }


    public static void main(String[] args) throws IOException {
        Set<Message> msgs = new InputHanlder().readFromMessages(
                DefaultSettings.resourceFolder.resolve(DefaultSettings.manhattanMessages), 15000000), filtered = new
                HashSet<>();//DefaultSettings
        Path baseDir = DefaultSettings.resourceFolder.resolve
                ("chinatownTribecaLittleItaly");
        FeatureCollection fc = new FeatureJSON().readFeatureCollection(new FileInputStream
                (baseDir.resolve("regionFC.json").toFile()));

        FeatureIterator iter = fc.features();
        List<Polygon> polygons = new ArrayList<>(fc.size());
        Polygon union = (Polygon) ((SimpleFeature) fc.features().next()).getDefaultGeometry();
        while (iter.hasNext()) {
            SimpleFeature feature = (SimpleFeature) iter.next();
            Polygon p = (Polygon) feature.getDefaultGeometry();
            polygons.add(p);
            union = (Polygon) union.union(p);
        }

//        System.out.println(new GeometryJSON().toString(union));

        Polygon[] arr = new Polygon[polygons.size()];
        MultiPolygon mp = new GeometryBuilder().multiPolygon(polygons.toArray(arr));

        new GeometryJSON().writeGeometryCollection(new GeometryBuilder().geometryCollection(polygons.toArray(arr)),
                new FileOutputStream(baseDir.resolve("regionGC.json").toFile()));

        new GeometryJSON().write(union, new FileOutputStream(baseDir
                .resolve("boundingPolygon.json").toFile()));

        for (Message m : msgs) {
            if (mp.contains(m.location)) {
                filtered.add(m);
            }
        }

        String prefix = "msgs";

        // .BY_TOPIC_PATH
        // .resolve
        // ("building
        // .nature.sea.csv")


//        Reader jsonReader = new FileReader(outDir.resolve(DefaultSettings
//                .POLYGON_FILE_NAME).toFile());
//        Polygon p = new GeometryJSON().readPolygon(jsonReader);
//
//        Set<Message> relevantMsgs = new GridService().getIntersectingMsgs(msgs, p);

        new TrainTestCreator().create(filtered, baseDir, prefix);
    }

}
