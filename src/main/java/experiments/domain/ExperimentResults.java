package experiments.domain;

import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

public class ExperimentResults {
    public final String algorithmName;
    public final List<List<Polygon>> finalSolution;
    public final long runningTime;

    public ExperimentResults(String algorithmName, List<List<Polygon>> finalSolution, long runningTime) {
        this.algorithmName = algorithmName;
        this.finalSolution = finalSolution;
        this.runningTime = runningTime;
    }
}
