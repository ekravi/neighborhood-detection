package experiments.domain;

import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

public class HCExperimentResults extends ExperimentResults {
    public final List<List<Polygon>> initialSolution;
    public final double baseScore, finalScore, avgAttmptsToImprovement;
    public final int overallSteps;

    public HCExperimentResults(String algorithmName, List<List<Polygon>> initialSolution, List<List<Polygon>>
            finalSolution, double baseScore, double finalScore,
                               int overallSteps, double avgAttemptsUntilImprovement, long runningTime) {
        super(algorithmName, finalSolution, runningTime);
        this.initialSolution = initialSolution;
        this.baseScore = baseScore;
        this.finalScore = finalScore;
        this.overallSteps = overallSteps;
        this.avgAttmptsToImprovement = avgAttemptsUntilImprovement;
    }
}
