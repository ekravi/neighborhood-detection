package experiments.domain;

import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

public final class HCRunResults extends RunResults {
    public final int overallSteps, minAttemptsUntilImprovement, maxAttemptsUntilImprovement;
    public final double avgAttemptsUntilImprovement, baseScore, finalScore;

    public HCRunResults(List<List<Polygon>> solution, int overallSteps, int
            minAttemptsUntilImprovement, int maxAttemptsUntilImprovement, double avgAttemptsUntilImprovement, double
                                baseScore,
                        double finalScore, long runningTime) {
        super(solution, runningTime);
        this.overallSteps = overallSteps;
        this.minAttemptsUntilImprovement = minAttemptsUntilImprovement;
        this.maxAttemptsUntilImprovement = maxAttemptsUntilImprovement;
        this.avgAttemptsUntilImprovement = avgAttemptsUntilImprovement;
        this.baseScore = baseScore;
        this.finalScore = finalScore;
    }
}
