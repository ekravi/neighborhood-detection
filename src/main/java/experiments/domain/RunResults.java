package experiments.domain;

import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

public class RunResults {
    public final List<List<Polygon>> solution;
    public final long runningTime;

    public RunResults(List<List<Polygon>> solution, long runningTime) {
        this.solution = solution;
        this.runningTime = runningTime;
    }
}
