package experiments.domain;

import com.vividsolutions.jts.geom.Polygon;

import java.util.List;

public class StepResults {
    public final List<List<Polygon>> solution;
    public final int attemptsUntilImprovement;
    public final long time;

    public StepResults(List<List<Polygon>> solution, int attemptsUntilImprovement, long time) {
        this.solution = solution;
        this.attemptsUntilImprovement = attemptsUntilImprovement;
        this.time = time;
    }

    public StepResults(StepResults toCopy) {
        this.solution = toCopy.solution;
        this.attemptsUntilImprovement = toCopy.attemptsUntilImprovement;
        this.time = toCopy.time;
    }
}
