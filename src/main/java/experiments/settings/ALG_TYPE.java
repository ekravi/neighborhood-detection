package experiments.settings;

import algorithms.localSearch.FirstChioceSplitHillClimbing;
import algorithms.localSearch.IBetterNeighborSplitFinder;
import algorithms.localSearch.LocalBeamSplitHillClimbing;
import algorithms.localSearch.SteepestAscentSplitHillClimbing;
import algorithms.splits.evaluation.IStepEvaluator;

public enum ALG_TYPE {
    FIRST {
        public IBetterNeighborSplitFinder getSplitAlg(IStepEvaluator heur) {
            return new FirstChioceSplitHillClimbing(heur);
        }
    }, STOC {
        public IBetterNeighborSplitFinder getSplitAlg(IStepEvaluator heur) {
            return new LocalBeamSplitHillClimbing(heur, 10);
        }
    },
    STEEP {
        public IBetterNeighborSplitFinder getSplitAlg(IStepEvaluator heur) {
            return new SteepestAscentSplitHillClimbing(heur);
        }
//    }, BUTTOM {
//        public IBetterNeighborSplitFinder getSplitAlg(IStepEvaluator heur) {
//            return new BottomUpClusterer((IDistanceMeasure) heur);
//        }
    };

    public abstract IBetterNeighborSplitFinder getSplitAlg(IStepEvaluator heur);
}
