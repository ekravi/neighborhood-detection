package experiments.settings;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class DefaultSettings {
    public static final Path resourceFolder = Paths.get("src/main/resources/");

    // messages
    public static final Path allMessages = Paths.get("tweetsClean.csv");
    public static final Path allMessagesGrid = Paths.get("grid.json");
    public static final Path manhattanMessages = Paths.get("manhattanClean.csv");
    public static final Path BY_TOPIC_PATH = resourceFolder.resolve("byTopic");
    public static Path manhattanGrid = Paths.get("manhattanGrid.json");
    public static final Path manhattanIntersectingCells = Paths.get("manhattanIntersectingCells.json");

    // Grid
    public static final int CELL_WIDTH_IN_METERS = 100;
    public static final int[] CELL_WIDTH_ZOOMING = new int[]{1000, 500, 100};


    // Polygon
    public static final String POLYGON_FILE_NAME = "polygon.json";
    public static final String MANHATTAN_POLYGON_FILE_NAME = "manhattanPolygon.json";
    public static final int MIN_RAND_CELLS = 3;
    public static final int MAX_RAND_CELLS = 10;

    // IR
    public static final int IR_K_TERMS = 10;
    // LM
    public static final int LM_ORDER = 2;

    // LDA
    public static final double LDA_ALPHA_PRIOR = 0.01, LDA_BETA_PRIOR = 0.01;
    public static final int LDA_NUM_ITERATIONS = 100;
    public static final int SINGLE_LDA_NUM_ITERATIONS = 10;
    public static final int LDA_NUM_THREADS = 4;
    public static final int LDA_NUM_TOPICS = 5;

    // Data Generation
    public static final double TRAIN_PORTION = 0.9;
}
