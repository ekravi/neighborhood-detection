package ir;

import java.util.Set;

/**
 * Created by ekravi on 30/05/2017.
 */
public interface LMAggregator {
    /**
     * aggregate several language models to one language model
     */
    LM aggregate(Set<LM> lms);
}
