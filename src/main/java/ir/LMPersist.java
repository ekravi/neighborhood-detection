package ir;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by ekravi on 16/05/2017.
 */
public interface LMPersist {
    String PROBS_SEPARATOR = "\t";
    String ORDER_SEPARATOR = "---------------";

    public void persist(Path toSave) throws IOException;

    public LM load(Path toLoad) throws IOException;
}
