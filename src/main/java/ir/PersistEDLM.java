package ir;

import service.SeparatorStringBuilder;
import statistics.DoubleValue;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ekravi on 17/05/2017.
 */
public class PersistEDLM extends EDLM implements LMPersist {
    public PersistEDLM(int n) {
        super(n);
    }

    public PersistEDLM(EDLM other) {
        super(other);
    }

    public PersistEDLM() {
        super();
    }

    public void persist(Path toSave) throws IOException {
        try (BufferedWriter bw = Files.newBufferedWriter(toSave, StandardOpenOption.CREATE_NEW)) {
            for (int i = 1; i <= order; ++i) {
                SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
                Map<String, DoubleValue> map = probs.get(i);
                for (String k : map.keySet()) {
                    ssb.append(new StringBuilder(k).append(LMPersist.PROBS_SEPARATOR).append(map.get(k).getValue()));
                }
                ssb.append(LMPersist.ORDER_SEPARATOR);
                bw.append(ssb.toString() + '\n');
                bw.flush();
            }
        }
    }

    public LM load(Path toLoad) throws IOException {
        probs.clear();

        List<String> lines = Files.readAllLines(toLoad);

        int cOrder = 1;
        probs.put(cOrder, new HashMap<>());
        Map<String, DoubleValue> map = probs.get(cOrder);
        for (String line : lines) {
            if (line.equals(LMPersist.ORDER_SEPARATOR)) {
                cOrder++;
                probs.put(cOrder, new HashMap<>());
                map = probs.get(cOrder);
            } else {
                String[] lineArr = line.split(LMPersist.PROBS_SEPARATOR);
                if (map.containsKey(lineArr[0]))
                    throw new RuntimeException("key " + lineArr[0] + " already exists");
                map.put(lineArr[0], new DoubleValue(Double.valueOf(lineArr[1])));
            }
        }
        return this;
    }
}
