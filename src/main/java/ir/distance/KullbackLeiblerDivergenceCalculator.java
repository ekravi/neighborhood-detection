package ir.distance;

import ir.LM;
import statistics.Histogram;

/**
 * Created by ekravi on 16/05/2017.
 */
public class KullbackLeiblerDivergenceCalculator implements LMDistance {

    @Override
    public double getDistance(LM from, LM to) {
        double $ = 0.0;
        int maxOrder = from.getOrder();
        if (to.getOrder() < maxOrder)
            throw new IllegalArgumentException("Can't compare a LM having lower order than the other");
        Histogram<String> fromProbs = from.getProbability(maxOrder), toProbs = to.getProbability(maxOrder);
        for (String k : fromProbs.keySet()) {
            if (toProbs.get(k) == null)
                continue;
            $ += getKLDivValue(fromProbs.get(k).getValue(), toProbs.get(k).getValue());
        }
        return $;
    }

    @Override
    public Histogram<String> getTopKEntries(LM from, LM to, int k) {
        Histogram<String> $ = new Histogram<>();
        int maxOrder = from.getOrder();
        if (to.getOrder() < maxOrder)
            throw new IllegalArgumentException("Can't compare to a LM having lower order than mine");

        Histogram<String> fromProbs = from.getProbability(maxOrder), toProbs = to.getProbability(maxOrder);
        for (String key : fromProbs.keySet()) {
            if (toProbs.get(key) == null)
                continue;
            $.set(key, getKLDivValue(fromProbs.get(key).getValue(), toProbs.get(key).getValue()));
        }
        return $.getTopK(k);
    }

    /**
     * see <a href="https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence">this link</a>
     */
    private double getKLDivValue(double p, double q) {
        if (p == 0 || q == 0) return 0;
        return p * Math.log(p / q);
    }


    /**
     * The KL distance between two distributions is the sum over the KL value for all the values
     */
    public double getDistance(double[] fromDist, double[] toDist) {
        if (fromDist.length != toDist.length)
            throw new IllegalArgumentException("distributions having different lengths");
        double $ = 0;
        for (int i = 0; i < fromDist.length; ++i) {
            $ += getKLDivValue(fromDist[i], toDist[i]);
        }
        return $;
    }


}
