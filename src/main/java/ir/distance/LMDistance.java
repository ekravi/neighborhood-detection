package ir.distance;

import ir.LM;
import statistics.Histogram;

public interface LMDistance {
    double getDistance(LM from, LM to);

    Histogram<String> getTopKEntries(LM from, LM to, int k);
}
