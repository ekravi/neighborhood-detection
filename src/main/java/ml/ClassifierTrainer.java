package ml;

import edu.stanford.nlp.classify.Classifier;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.objectbank.ObjectBank;
import edu.stanford.nlp.util.ErasureUtils;
import edu.stanford.nlp.util.Pair;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class ClassifierTrainer {
    private final Properties props;
    private final ColumnDataClassifier cdc;
    private final Path baseFolder;

    public ClassifierTrainer(Path baseFolder) {
        this.baseFolder = baseFolder;
        this.props = getSettings();
        this.cdc = new ColumnDataClassifier(props);
    }

    public void train(Path trainFile) throws IOException {
        cdc.trainClassifier(baseFolder.resolve(trainFile).toString());
    }

    public void classifyItems(Path testFile) throws IOException {
        try (BufferedReader br = Files.newBufferedReader(baseFolder.resolve(testFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                Datum<String, String> d = cdc.makeDatumFromLine(line);
                System.out.printf("%s  ==>  %s (%.4f)%n", line, cdc.classOf(d), cdc.scoresOf(d).getCount(cdc.classOf
                        (d)));
            }
        }
    }

    public void test(Path testFile) throws IOException {
        Pair<Double, Double> performance = cdc.testClassifier(baseFolder.resolve(testFile).toString());
        System.out.printf("Accuracy: %.3f; macro-F1: %.3f%n", performance.first(), performance.second());
    }

    public static void main(String[] args) throws IOException {
        ClassifierTrainer c = new ClassifierTrainer(Paths.get
                ("src/main/resources/classifier"));
        c.train(Paths.get("cheeseDisease.train"));
        c.classifyItems(Paths.get("cheeseDisease.test"));
        c.test(Paths.get("cheeseDisease.test"));
    }

    private static String where = "";

    private static void demonstrateSerialization()
            throws IOException, ClassNotFoundException {
        System.out.println();
        System.out.println("Demonstrating working with a serialized classifier");
        ColumnDataClassifier cdc = new ColumnDataClassifier(where + "examples/cheese2007.prop");
        Classifier<String, String> cl =
                cdc.makeClassifier(cdc.readTrainingExamples(where + "examples/cheeseDisease.train"));

        // Exhibit serialization and deserialization working. Serialized to bytes in memory for simplicity
        System.out.println();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(cl);
        oos.close();

        byte[] object = baos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(object);
        ObjectInputStream ois = new ObjectInputStream(bais);
        LinearClassifier<String, String> lc = ErasureUtils.uncheckedCast(ois.readObject());
        ois.close();
        ColumnDataClassifier cdc2 = new ColumnDataClassifier(where + "examples/cheese2007.prop");

        // We compare the output of the deserialized classifier lc versus the original one cl
        // For both we use a ColumnDataClassifier to convert text lines to examples
        System.out.println();
        System.out.println("Making predictions with both classifiers");
        for (String line : ObjectBank.getLineIterator(where + "examples/cheeseDisease.test", "utf-8")) {
            Datum<String, String> d = cdc.makeDatumFromLine(line);
            Datum<String, String> d2 = cdc2.makeDatumFromLine(line);
            System.out.printf("%s  =origi=>  %s (%.4f)%n", line, cl.classOf(d), cl.scoresOf(d).getCount(cl.classOf(d)));
            System.out.printf("%s  =deser=>  %s (%.4f)%n", line, lc.classOf(d2), lc.scoresOf(d).getCount(lc.classOf
                    (d)));
        }
    }

    private static void demonstrateSerializationColumnDataClassifier()
            throws IOException, ClassNotFoundException {
        System.out.println();
        System.out.println("Demonstrating working with a serialized classifier using serializeTo");
        ColumnDataClassifier cdc = new ColumnDataClassifier(where + "examples/cheese2007.prop");
        cdc.trainClassifier(where + "examples/cheeseDisease.train");

        // Exhibit serialization and deserialization working. Serialized to bytes in memory for simplicity
        System.out.println();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        cdc.serializeClassifier(oos);
        oos.close();

        byte[] object = baos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(object);
        ObjectInputStream ois = new ObjectInputStream(bais);
        ColumnDataClassifier cdc2 = ColumnDataClassifier.getClassifier(ois);
        ois.close();

        // We compare the output of the deserialized classifier cdc2 versus the original one cl
        // For both we use a ColumnDataClassifier to convert text lines to examples
        System.out.println("Making predictions with both classifiers");
        for (String line : ObjectBank.getLineIterator(where + "examples/cheeseDisease.test", "utf-8")) {
            Datum<String, String> d = cdc.makeDatumFromLine(line);
            Datum<String, String> d2 = cdc2.makeDatumFromLine(line);
            System.out.printf("%s  =origi=>  %s (%.4f)%n", line, cdc.classOf(d), cdc.scoresOf(d).getCount(cdc.classOf
                    (d)));
            System.out.printf("%s  =deser=>  %s (%.4f)%n", line, cdc2.classOf(d2), cdc2.scoresOf(d).getCount
                    (cdc2.classOf(d)));
        }
    }

    public Properties getSettings() {
        Properties $ = new Properties();
        IOUtils.toInputStream(
                // features
                "useClassFeature=false\n" + // Include a feature for the class (as a class marginal)
                        "1.useNGrams=true\n" + // Make features from letter n-grams
                        "1.usePrefixSuffixNGrams=true\n" + // Make features from prefix and suffix substrings
                        "1.maxNGramLeng=4\n" + // n-grams above this size will not be used in the model
                        "1.minNGramLeng=1\n" + // n-grams below this size will not be used in the model
                        "1.binnedLengths=10,20,30\n" + // The feature represents the length of the String in this
                        // column.

                        // Printing
                        "printClassifierParam=200\n" + // A parameter to the printing style

                        // Mapping
                        "goldAnswerColumn=0\n" + // Column number that contains the correct class for each data item
                        "displayedColumn=1\n" + // Column number that will be printed out to stdout in the output

                        //Optimization
                        "intern=true\n" + // intern all of the (final) feature names
                        "sigma=3\n" + // smoothing (i.e., regularization) methods, bigger number is more smoothing
                        "useQN=true\n" + // Use Quasi-Newton optimization
                        "QNsize=15\n" + // Number of previous iterations of Quasi-Newton to store
                        "tolerance=1e-4\n" + // Convergence tolerance in parameter optimization

                        // Training input
                        "trainFile=./examples/cheeseDisease.train\n" +
                        "testFile=./examples/cheeseDisease.test\n", Charset.defaultCharset());

        return $;
    }
}
