package service;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ekravi on 21/06/2017.
 */
public class FileUtils {
    public static InputStream getResource(String fileName) throws IOException {

        StringBuilder result = new StringBuilder("");

        //Get file from resources folder
        ClassLoader classLoader = FileUtils.class.getClassLoader();
        return classLoader.getResource(fileName).openStream();

    }
}