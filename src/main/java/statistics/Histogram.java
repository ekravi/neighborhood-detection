package statistics;

import service.MapUtils;
import service.SeparatorStringBuilder;

import java.util.*;

public class Histogram<T extends Comparable<T>> {
    private final Map<T, DoubleValue> map;

    public Histogram() {
        this.map = new HashMap<>();
    }

    public Histogram(Map<T, DoubleValue> initial) {
        this.map = initial;
    }

    public void inc(T k) {
        if (!map.containsKey(k))
            map.put(k, new DoubleValue(0));
        map.get(k).increment();
    }

    public void inc(T k, int val) {
        if (!map.containsKey(k))
            map.put(k, new DoubleValue(0));
        map.get(k).increment(val);
    }

    public void set(T k, int val) {
        map.put(k, new DoubleValue(val));
    }

    public void set(T k, double val) {
        map.put(k, new DoubleValue(val));
    }

    public DoubleValue get(T k) {
        return map.get(k);
    }

    public List<T> getTopKKeys(int k) {
        Map<T, DoubleValue> sortedByValue = MapUtils.sortByValue(map);
        List<T> $ = new ArrayList<>(k);

        for (T key : sortedByValue.keySet()) {
            $.add(key);
            if ($.size() == k) break;
        }
        return $;
    }

    public Histogram<T> getTopK(int k) {
        Map<T, DoubleValue> sortedByValue = MapUtils.sortByValue(map);
        Histogram<T> $ = new Histogram<>();
        for (T key : sortedByValue.keySet()) {
            $.set(key, sortedByValue.get(key).getValue());
            if ($.size() == k) break;
        }
        return $;
    }

    public int size() {
        return map.size();
    }

    public Set<T> keySet() {
        return map.keySet();
    }

    public String toString() {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (T key : map.keySet()) {
            ssb.append(new StringBuilder(key.toString()).append('\t').append(map.get(key).getValue()));
        }
        return ssb.toString();
    }
}
