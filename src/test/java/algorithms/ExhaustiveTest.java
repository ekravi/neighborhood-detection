package algorithms;

import domain.ConnectedSubgraph;
import domain.Grid;
import domain.Grid.Cell;
import org.junit.Before;
import org.junit.Test;
import service.SeparatorStringBuilder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by ekravi on 18/05/2017.
 */
public class ExhaustiveTest {

    private Grid g = new Grid(2, 2);
    private Exhaustive toTest;

    @Before
    public void setup() {
        toTest = new Exhaustive(g);
    }

    @Test
    public void simpleGrid() {
        g = new Grid(1, 1);
        toTest = new Exhaustive(g);

        Set<ConnectedSubgraph> cs = toTest.retrieveForSize(1);
        assertTrue(cs.size() == 1);

        ConnectedSubgraph expectedCS = new ConnectedSubgraph(g, new HashSet<>(Arrays.asList(new Cell[]{new Cell(0, 0)
        })));
        assertEquals(new HashSet<ConnectedSubgraph>(Arrays.asList(new ConnectedSubgraph[]{expectedCS})), cs);
    }

    @Test
    public void twoOnOneGrid() {
        g = new Grid(2, 1);
        toTest = new Exhaustive(g);

        Set<ConnectedSubgraph> cs = toTest.retrieveForSize(1);
        assertTrue(cs.size() == 2);
    }

    private String printList(List<ConnectedSubgraph> cs1) {
        SeparatorStringBuilder ssb = new SeparatorStringBuilder('\n');
        for (ConnectedSubgraph cs : cs1
                ) {
            ssb.append(cs);
        }
        return ssb.toString();
    }


    @Test
    public void getUpdatedOptional() {
        Cell c = new Cell(0, 0);
        Set<Cell> optional = new HashSet<>(Arrays.asList(new Cell[]{c, new Cell(0, 1)}));
        Set<Cell> updatedOptional = toTest.getUpdatedOptional(optional, c);
        optional.remove(c);
        assertTrue(updatedOptional.size() == 1);
        assertEquals(optional, updatedOptional);
    }

    @Test
    public void getUpdatedNeighbors() {
        Cell c = new Cell(1, 1);
        ConnectedSubgraph cs = new ConnectedSubgraph(g, new HashSet<Cell>(Arrays.asList(new
                Cell[]{new Cell(0, 0), new Cell(0, 1)})));
        Set<Cell> neighbors = new HashSet<>(Arrays.asList(new Cell[]{new Cell(1, 1), new Cell(1, 0)}));

        toTest.getUpdatedNeighbors(neighbors, c, cs);
    }

    @Test
    public void getUpdatedSolution() {
        assertEquals(new ConnectedSubgraph(g, new HashSet<>(Arrays.asList(new Cell[]{new Cell(1, 0)}))), toTest
                .getUpdatedSolution(null,
                        new Cell(1, 0)));
        ConnectedSubgraph cs = new ConnectedSubgraph(g, new HashSet<Cell>(Arrays.asList(new
                Cell[]{new Cell(0, 0), new Cell(0, 1)})));
        ConnectedSubgraph updatedSolution = toTest.getUpdatedSolution(cs, new Cell(1, 0));
        assertTrue(updatedSolution.size() == 3);
        assertTrue(updatedSolution.contains(new Cell(1, 0)));
    }

    @Test
    public void getRelevantCells() {
        Set<Cell> optional = new HashSet<>(Arrays.asList(new Cell[]{new Cell(0, 0), new Cell(0, 1)}));
        Set<Cell> neighbors = new HashSet<>(Arrays.asList(new
                Cell[]{new Cell(0,
                1), new Cell(1, 1)
        }));
        Set<Cell> relevantCells = toTest.getRelevantCells(optional, neighbors);

        assertTrue(relevantCells.size() == 1);
        assertEquals(relevantCells.iterator().next(), new Cell(0,
                1));
    }
}