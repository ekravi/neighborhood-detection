package algorithms.clustering;

import algorithms.distance.IDistanceMeasure;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertTrue;

public class BottomUpClustererTest {

    private final BottomUpClusterer toTest = new BottomUpClusterer(new IDistanceMeasure() {
        @Override
        public double getDistance(Set<Polygon> from, Set<Polygon> to) {
            return from.size() - to.size();
        }

        @Override
        public double getDistance(Polygon from, Polygon to) {
            return 0;
        }
    });

    private Set<Polygon> polygons = new HashSet<>();
    private Polygon m_p11, m_p12, m_p13, m_p21, m_p22, m_p23, m_p31, m_p32, m_p33, m_p41, m_p42, m_p43;
    private Map<Set<Polygon>, Set<Set<Polygon>>> neighbors = new HashMap<>();
    private PriorityQueue<Pair<Set<Polygon>, Set<Polygon>>> byDistance = new PriorityQueue<>(toTest.dm);

    @Before
    public void setup() {
        GeometryFactory f = new GeometryFactory();
        m_p11 = f.createPolygon(new Coordinate[]{new Coordinate(0, 0), new Coordinate(1, 0), new
                Coordinate(1, 1), new Coordinate(0, 1), new Coordinate(0, 0)});
        m_p12 = f.createPolygon(new Coordinate[]{new Coordinate(0, 1), new Coordinate(1, 1), new Coordinate
                (1, 2), new Coordinate(0, 2), new Coordinate(0, 1)});
        m_p13 = f.createPolygon(new Coordinate[]{new Coordinate(0, 2), new Coordinate(1, 2), new Coordinate
                (1, 3), new Coordinate(0, 3), new Coordinate(0, 2)});
        m_p21 = f.createPolygon(new Coordinate[]{new Coordinate(1, 0), new Coordinate(2, 0), new Coordinate
                (2, 1), new Coordinate(1, 1), new Coordinate(1, 0)});
        m_p22 = f.createPolygon(new Coordinate[]{new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate
                (2, 2), new Coordinate(1, 2), new Coordinate(1, 1)});
        m_p23 = f.createPolygon(new Coordinate[]{new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate
                (2, 3), new Coordinate(1, 3), new Coordinate(1, 2)});
        m_p31 = f.createPolygon(new Coordinate[]{new Coordinate(2, 0), new Coordinate(3, 0), new Coordinate
                (3, 1), new Coordinate(2, 1), new Coordinate(2, 0)});
        m_p32 = f.createPolygon(new Coordinate[]{new Coordinate(2, 1), new Coordinate(3, 1), new Coordinate
                (3, 2), new Coordinate(2, 2), new Coordinate(2, 1)});
        m_p33 = f.createPolygon(new Coordinate[]{new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate
                (3, 3), new Coordinate(2, 3), new Coordinate(2, 2)});
        m_p41 = f.createPolygon(new Coordinate[]{new Coordinate(3, 0), new Coordinate(4, 0), new Coordinate
                (4, 1), new Coordinate(3, 1), new Coordinate(3, 0)});
        m_p42 = f.createPolygon(new Coordinate[]{new Coordinate(3, 1), new Coordinate(4, 1), new Coordinate
                (4, 2), new Coordinate(3, 2), new Coordinate(3, 1)});
        m_p43 = f.createPolygon(new Coordinate[]{new Coordinate(3, 2), new Coordinate(4, 2), new Coordinate
                (4, 3), new Coordinate(3, 3), new Coordinate(3, 2)});

        polygons.addAll(Arrays.asList(new Polygon[]{m_p11, m_p12, m_p13, m_p21, m_p22}));

        toTest.init(neighbors, byDistance, polygons);
    }

    @Test
    public void testBigCluster() throws Exception {
        polygons.addAll(Arrays.asList(new Polygon[]{m_p23, m_p31, m_p32, m_p33, m_p41, m_p42, m_p43}));
        toTest.cluster(polygons, 3);
    }

    @Test
    public void testCluster() throws Exception {
        assertTrue(toTest.cluster(polygons, 5).solution.size() == 5);
        assertTrue(toTest.cluster(polygons, 1).solution.iterator().next().size() == 5);

        List<List<Polygon>> cluster = toTest.cluster(polygons, 2).solution;
        assertTrue(cluster.size() == 2);
        List<Polygon> next = cluster.iterator().next();
        assertTrue(next.size() == 4 || next.size() == 1);
    }

    @Test
    public void testInit() throws Exception {
        Map<Set<Polygon>, Set<Set<Polygon>>> tmpNeighbors = new HashMap<>();
        PriorityQueue<Pair<Set<Polygon>, Set<Polygon>>> tmpByDistance = new PriorityQueue<>(toTest.dm);
        toTest.init(tmpNeighbors, tmpByDistance, polygons);

        Set<Polygon> p11 = new HashSet<>(), p12 = new HashSet<>(), p21 = new HashSet<>(), p22 = new HashSet<>();
        p11.add(m_p11);
        p12.add(m_p12);
        p21.add(m_p21);
        p22.add(m_p22);

        assertTrue(tmpNeighbors.get(p11).size() == 2);
        assertTrue(tmpNeighbors.get(p11).contains(p12));

        assertTrue(tmpByDistance.contains(new Pair<>(p11, p12)));
        assertTrue(tmpByDistance.contains(new Pair<>(p11, p21)));
        assertTrue(!tmpByDistance.contains(new Pair<>(p11, p22)));
        assertTrue(tmpByDistance.contains(new Pair<>(p12, p11)));
        assertTrue(tmpByDistance.contains(new Pair<>(p21, p11)));
        assertTrue(!tmpByDistance.contains(new Pair<>(p22, p11)));

        assertTrue(tmpByDistance.size() == 10);
    }

    @Test
    public void testUpdatePQ() {
        Set<Polygon> p11 = new HashSet<>(), p12 = new HashSet<>(), p13 = new HashSet<>(), fromAndTo = new HashSet<>();
        p11.add(m_p11);
        p12.add(m_p12);
        p13.add(m_p13);

        fromAndTo.addAll(p11);
        fromAndTo.addAll(p12);

        neighbors.get(p11).remove(p12);
        neighbors.get(p12).remove(p11);

        toTest.updatePQ(byDistance, p11, p12, neighbors.get(p11), neighbors.get(p12), fromAndTo);

        assertTrue(!byDistance.contains(new Pair<>(p12, p13)));
        assertTrue(byDistance.contains(new Pair<>(fromAndTo, p13)));
        assertTrue(!byDistance.contains(new Pair<>(p13, p12)));
        assertTrue(byDistance.contains(new Pair<>(p13, fromAndTo)));

        assertTrue(!byDistance.contains(new Pair<>(fromAndTo, p12)));
        assertTrue(!byDistance.contains(new Pair<>(p12, fromAndTo)));

    }

    @Test
    public void testUpdateMap() {
        Set<Polygon> p11 = new HashSet<>(), p12 = new HashSet<>(), p13 = new HashSet<>(), fromAndTo = new HashSet<>();
        p11.add(m_p11);
        p12.add(m_p12);
        p13.add(m_p13);

        fromAndTo.addAll(p11);
        fromAndTo.addAll(p12);

        int p11_n = neighbors.get(p11).size(), p12_n = neighbors.get(p12).size();

        neighbors.get(p11).remove(p12);
        neighbors.get(p12).remove(p11);

        toTest.updateMap(neighbors, p11, p12, neighbors.get(p11), neighbors.get(p12), fromAndTo);

        assertTrue(!neighbors.containsKey(p12));
        assertTrue(String.valueOf(neighbors.get(fromAndTo).size()), neighbors.get(fromAndTo).size() == (p11_n + p12_n
                - 2));
    }

    @Test
    public void testSingleMerge() {
        int nSize = neighbors.size(), bdSize = byDistance.size();
        toTest.singleMerge(neighbors, byDistance);

        assertTrue(neighbors.size() == (nSize - 1));
        assertTrue(String.valueOf(byDistance.size()), byDistance.size() == (bdSize - 2));
    }
}