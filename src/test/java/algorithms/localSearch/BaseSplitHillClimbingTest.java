package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import domain.Message;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by ekravi on 17/07/2017.
 */
public class BaseSplitHillClimbingTest {
    private BaseSplitHillClimbing toTest = new FirstChioceSplitHillClimbing(new IStepEvaluator() {
        @Override
        public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
            return 0;
        }

        @Override
        public void init(Set<Polygon> roi, Set<Message> initSet) {
            // do nothing
        }

    });
    protected List<List<Polygon>> mpList = new ArrayList<>(2);
    protected Polygon p11;

    @Before
    public void setup() {
        GeometryFactory f = new GeometryFactory();
        Polygon p11 = f.createPolygon(new Coordinate[]{new Coordinate(0, 0), new Coordinate(1, 0), new
                Coordinate(1, 1), new Coordinate(0, 1), new Coordinate(0, 0)}),
                p12 = f.createPolygon(new Coordinate[]{new Coordinate(0, 1), new Coordinate(1, 1), new Coordinate
                        (1, 2), new Coordinate(0, 2), new Coordinate(0, 1)}),
                p13 = f.createPolygon(new Coordinate[]{new Coordinate(0, 2), new Coordinate(1, 2), new Coordinate
                        (1, 3), new Coordinate(0, 3), new Coordinate(0, 2)}),
                p21 = f.createPolygon(new Coordinate[]{new Coordinate(1, 0), new Coordinate(2, 0), new Coordinate
                        (2, 1), new Coordinate(1, 1), new Coordinate(1, 0)}),
                p22 = f.createPolygon(new Coordinate[]{new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate
                        (2, 2), new Coordinate(1, 2), new Coordinate(1, 1)});
        this.p11 = p11;
        mpList.add(Arrays.asList(new Polygon[]{p11, p12, p13}));
        mpList.add(Arrays.asList(new Polygon[]{p21, p22}));
    }

    @Test
    public void getNeighboringCells() throws Exception {
        Map<Pair<Integer, Integer>, Set<Integer>> actual = toTest.getCellsWithNeighbors(mpList);
        // expecting (0,0)->1, (0,1)->1,(1,0)->0, (1,1)->1
        assertTrue(actual.size() == 4);
        assertTrue(actual.containsKey(new Pair<Integer, Integer>(1, 0)));
        assertTrue(actual.get(new Pair<Integer, Integer>(1, 0)).size() == 1);
        assertTrue(actual.get(new Pair<Integer, Integer>(1, 0)).iterator().next() == 0);
    }

}