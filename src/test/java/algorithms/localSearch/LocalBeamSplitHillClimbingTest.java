package algorithms.localSearch;

import algorithms.splits.evaluation.IStepEvaluator;
import com.vividsolutions.jts.geom.Polygon;
import domain.Message;
import experiments.domain.StepResults;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by ekravi on 17/07/2017.
 */
public class LocalBeamSplitHillClimbingTest extends BaseSplitHillClimbingTest {
    private LocalBeamSplitHillClimbing toTest = new LocalBeamSplitHillClimbing(new IStepEvaluator() {
        @Override
        public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
            return 0;
        }

        @Override
        public void init(Set<Polygon> roi, Set<Message> initSet) {

        }

    }, 3);

    @Before
    public void setup() {
        super.setup();
    }


    @Test
    public void getImprovedNeighborNull() throws Exception {
        assertNull(toTest.getImprovedNeighbor(mpList).solution);
    }

    @Test
    public void getImprovedNeighborNotNull() throws Exception {
        toTest = new LocalBeamSplitHillClimbing(new IStepEvaluator() {
            @Override
            public double evaluate(List<List<Polygon>> toEvaluate) throws IOException {
                double $ = 0;
                for (List<Polygon> polygons : toEvaluate) {
                    $ += Math.pow(polygons.size(), 2);
                }
                return $;
            }

            @Override
            public void init(Set<Polygon> roi, Set<Message> initSet) {

            }

        }, 1);
        StepResults sRes = toTest.getImprovedNeighbor(mpList);
        // expected: (0,0)->(1),(0,1)->(1),(1,0)->(0),(1,1)->(0)
        assertTrue(sRes.solution.get(0).size() == 4 || sRes.solution.get(1).size() == 4);
    }
}