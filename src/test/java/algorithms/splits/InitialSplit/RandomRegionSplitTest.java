package algorithms.splits.InitialSplit;

import domain.ConnectedSubgraph;
import domain.Grid;
import domain.Grid.Cell;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by ekravi on 06/07/2017.
 */
public class RandomRegionSplitTest {

    private RandomRegionSplit toTest = new RandomRegionSplit();
    private Grid grid = new Grid(3, 3);
    private ConnectedSubgraph region = new ConnectedSubgraph(grid, new HashSet<>(grid.getCells()));

    @Test
    public void splitTo2ConnectedSubgraphs() throws Exception {
        assertTrue(splitAndCount(2) == 9);
        assertTrue(splitAndCount(5) == 9);
        assertTrue(splitAndCount(9) == 9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void splitToTooManyConnectedSubgraphs() throws Exception {
        toTest.splitToConnectedSubgraphs(region, 10);
    }

    private int splitAndCount(int k) {
        Set<ConnectedSubgraph> css = toTest.splitToConnectedSubgraphs(region, k);

        int covered = 0;
        for (ConnectedSubgraph cs : css) {
            covered += cs.size();
        }
        return covered;
    }

    @Test
    public void random2CellCover() throws Exception {
        LinkedHashMap<Cell, ConnectedSubgraph> split = new LinkedHashMap<>();
        Cell c1 = new Cell(0, 0), c2 = new Cell(2, 2);
        split.put(c1, new ConnectedSubgraph(region.getGrid(), new HashSet<>(Arrays.asList(new Cell[]{c1}))));
        split.put(c2, new ConnectedSubgraph(region.getGrid(), new HashSet<>(Arrays.asList(new Cell[]{c2}))));

        Map<Cell, Cell> cellAssignment = new HashMap<>();
        cellAssignment.put(c1, c1);
        cellAssignment.put(c2, c2);

        assertTrue(split.get(c1).size() + split.get(c2).size() == 2);
        assertTrue(cellAssignment.size() == 2);

        toTest.randomCellCover(region, 2, split, cellAssignment, new Random());

        assertTrue(split.get(c1).size() + split.get(c2).size() == 3);
        assertTrue(cellAssignment.size() == 3);
    }
}