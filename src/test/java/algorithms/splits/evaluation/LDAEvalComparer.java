package algorithms.splits.evaluation;

import dataManipulations.files.InputHanlder;
import domain.Message;
import experiments.settings.DefaultSettings;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LDAEvalComparer {

    private final List<ISplitEvaluator> toCompare;
    private Set<Message> msgs;
    private List<String> stopwords;

    public LDAEvalComparer() throws IOException {
        msgs = new InputHanlder().readFromMessages(DefaultSettings.resourceFolder.resolve("forTest")
                .resolve("sample.csv"), 100000);
        stopwords = IOUtils.readLines(new FileInputStream
                (DefaultSettings.resourceFolder.resolve("stopwords.txt").toFile()), Charset.defaultCharset());

        toCompare = Arrays.asList(new ISplitEvaluator[]{/*new LDASplitEvaluator(),*/ new
                SingleLDASplitEvaluator(new HashSet<>(stopwords))});
    }

    @Test
    public void compare() {

    }
}
