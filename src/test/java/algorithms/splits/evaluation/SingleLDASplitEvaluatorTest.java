package algorithms.splits.evaluation;

import com.vividsolutions.jts.geom.Polygon;
import dataManipulations.files.InputHanlder;
import dataManipulations.geo.GridService;
import domain.Message;
import experiments.settings.DefaultSettings;
import ir.distance.KullbackLeiblerDivergenceCalculator;
import org.apache.commons.io.IOUtils;
import org.geotools.geojson.geom.GeometryJSON;
import org.junit.Test;
import service.SeparatorStringBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class SingleLDASplitEvaluatorTest {

    private final SingleLDASplitEvaluator toTest;
    private GridService gs = new GridService();
    private GeometryJSON geometryJSON = new GeometryJSON();

    private Set<Message> msgs;
    private List<Polygon> grid;

    private Set<Polygon> from = new HashSet<>(), to = new HashSet<>();

    public SingleLDASplitEvaluatorTest() throws IOException {
        toTest = new SingleLDASplitEvaluator(new HashSet<>(IOUtils.readLines(new FileInputStream
                (DefaultSettings.resourceFolder.resolve("stopwords.txt").toFile()), Charset.defaultCharset())), 3,
                5);
        msgs = new InputHanlder().readFromMessages(DefaultSettings.resourceFolder.resolve("forTest")
                .resolve("sample.csv"), 100000);
        grid = gs.generateGrid(msgs, 100);

        toTest.init(new HashSet<>(grid), msgs);

        printDebug();

        from.add(grid.get(0));
        from.add(grid.get(1));
        from.add(grid.get(2));

        to.add(grid.get(10));
        to.add(grid.get(11));
        to.add(grid.get(12));

    }

    private void printDebug() {
        System.out.println(grid.size() + " cells");
        List<Map<String, Integer>> topWordsPerTopic = toTest.tc.getTopWordsPerTopic(toTest.model, new
                ArrayList<String>(toTest.msgsAssignment
                .values()), 10);
        for (Map<String, Integer> topicTerms : topWordsPerTopic) {
            System.out.println(topicTerms);
        }

        for (Map.Entry<Polygon, double[]> entry :
                toTest.topicsPerCell.entrySet()) {
            SeparatorStringBuilder ssb = new SeparatorStringBuilder(',');
            ssb.append(geometryJSON.toString(entry.getKey()));
            for (double val : entry.getValue())
                ssb.append(val);
            System.out.println(ssb.toString());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitNull() {
        toTest.init(null, msgs);
    }

    /**
     * as no messages were sent from polygon 0 or 1 it is expected that an exception will be thrown
     *
     * @throws IOException
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetDistancePolygonPolygon() throws IOException {
        toTest.getDistance(grid.get(0), grid.get(1));
    }

    @Test
    public void testGetAvgDist() {
        double[] avgDist = new double[toTest.topicsPerCell.values().iterator().next().length];

        int numMsgs = 0;
        for (Polygon p : from) {
            if (toTest.topicsPerCell.containsKey(p)) {
                double[] dist = toTest.topicsPerCell.get(p);
                int pMsgs = toTest.polygonMsgs.get(p).size();
                for (int i = 0; i < avgDist.length; ++i) {
                    avgDist[i] += (dist[i] * pMsgs);
                }
                numMsgs += pMsgs;
            }
        }
        for (int i = 0; i < avgDist.length; ++i) {
            avgDist[i] /= numMsgs;
        }

        double[] actual = toTest.getAverageDist(from);
        for (int i = 0; i < avgDist.length; ++i) {
            assertEquals(avgDist[i], actual[i], 0.001);
        }
    }

    @Test
    public void testGetDistanceSetSet() throws IOException {
        double[] fromDist = toTest.getAverageDist(from), toDist = toTest
                .getAverageDist(to);
        double expected = new KullbackLeiblerDivergenceCalculator().getDistance(fromDist, toDist);
        double actual = toTest.getDistance(from, to);
        assertEquals(expected, actual, 0.001);
    }

//    @Test
//    public void testEvaluate() {
//        toTest.evaluate();
//    }
}