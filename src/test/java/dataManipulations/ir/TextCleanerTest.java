package dataManipulations.ir;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import domain.Message;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class TextCleanerTest {
    private TextCleaner toTest = new TextCleaner();

    @Test
    public void cleanEmpty() throws Exception {
        Set<Message> msgs = new HashSet<>();
        toTest.clean(msgs);
    }

    @Test
    public void cleanNoChange() throws Exception {
        Set<Message> msgs = new HashSet<>();
        msgs.add(new Message("@ekravi", new GeometryFactory().createPoint(new Coordinate(0, 0)), 0L));
        toTest.clean(msgs);
        assertEquals("@ekravi", msgs.iterator().next().getMsg());
    }

    @Test
    public void cleanToLowerCase() throws Exception {
        Set<Message> msgs = new HashSet<>();
        msgs.add(new Message("Week", new GeometryFactory().createPoint(new Coordinate(0, 0)), 0L));
        toTest.clean(msgs);
        assertEquals("week", msgs.iterator().next().getMsg());
    }

    @Test
    public void cleanTokenizeSpaces() throws Exception {
        Set<Message> msgs = new HashSet<>();
        msgs.add(new Message("two       weeks", new GeometryFactory().createPoint(new Coordinate(0, 0)), 0L));
        toTest.clean(msgs);
        assertEquals("two weeks", msgs.iterator().next().getMsg());
    }

    @Test
    public void cleanStopWords() throws Exception {
        Set<Message> msgs = new HashSet<>();
        msgs.add(new Message("a week", new GeometryFactory().createPoint(new Coordinate(0, 0)), 0L));
        toTest.clean(msgs);
        assertEquals("week", msgs.iterator().next().getMsg());
    }

}