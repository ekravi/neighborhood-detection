package domain;

import domain.Grid.Cell;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ConnectedSubgraphTest {

    private final Grid g = new Grid(3, 3);
    private Set<Cell> cells;
    private ConnectedSubgraph toTest;

    @Before
    public void setup() {
        cells = new HashSet<>(Arrays.asList(new Cell[]{new Cell(0, 0), new Cell(0, 1)}));
        toTest = new ConnectedSubgraph(g, cells);
    }

    @Test
    public void cells() {
        assertEquals(cells, toTest.cells);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotConnected() throws IllegalAccessException {
        new ConnectedSubgraph(g, new HashSet<>(Arrays.asList(new Cell[]{new Cell(0, 0), new Cell(1, 1)})));
    }

    @Test(expected = IllegalArgumentException.class)
    public void verifyEmpty() {
        new ConnectedSubgraph(g, new HashSet<Cell>());
    }

    @Test
    public void initCellsMat() {
        cells.add(new Cell(2, 1));
        int[][] cellsMat = toTest.initCellsMat(g, cells);
        assertTrue(cellsMat.length == g.numRows() && cellsMat[0].length == g.numColumns() && cellsMat[0][0] == 1 &&
                cellsMat[0][1] == 1 &&
                cellsMat[2][1] == 1);
    }

    @Test
    public void verifyConnectedSubgraph() {
        int[][] cellsMat = toTest.initCellsMat(g, cells);
        assertTrue(toTest.verifyConnectedSubgraph(cells));
    }

    @Test
    public void verifyNotAConnectedSubgraph() {
        cells.add(new Cell(2, 1));
        int[][] cellsMat = toTest.initCellsMat(g, cells);
        assertFalse(toTest.verifyConnectedSubgraph(cells));
    }
}