package domain;

import org.junit.Test;

import static org.junit.Assert.*;
import domain.Grid.Cell;

import java.util.List;

/**
 * Created by ekravi on 18/05/2017.
 */
public class GridTest {
    Grid toTest = new Grid(2,4);

    @Test
    public void getCells() throws Exception {
        List<Cell> cells = toTest.getCells();
        assertTrue(cells.size()==8);
        assertTrue(cells.contains(new Cell(1,2)));
    }

    @Test
    public void getNeighbors() throws Exception {
        assertTrue(toTest.getNeighbors(new Cell(1, 0)).size()==2);
        assertTrue(toTest.getNeighbors(new Cell(1, 0)).contains(new Cell(0,0)));
        assertTrue(toTest.getNeighbors(new Cell(1, 0)).contains(new Cell(1,1)));
    }

}