package ir;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import statistics.DoubleValue;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by ekravi on 16/05/2017.
 */
public class LMTest {
    private EDLM toTest;
    private Set<List<String>> train = new HashSet<>();

    public LMTest() {
        train.add(new LinkedList<String>(Arrays.asList(new String[]{"spatial", "temporal", "event", "detection"})));
        train.add(new LinkedList<String>(Arrays.asList(new String[]{"spatial", "signal"})));
        train.add(new LinkedList<String>(Arrays.asList(new String[]{"offline", "event", "detection"})));

    }

    @Before
    public void setup() {
        toTest = new EDLM();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor() {
        toTest = new EDLM(-2);
    }

    @Test
    public void initProbs() throws Exception {
        toTest.initProbs();
        for (int i = 1; i <= EDLM.DEFAULT_ORDER; ++i) {
            Map<String, DoubleValue> map = toTest.probs.get(i);
            Assert.assertTrue(map.get(EDLM.UNKNOWN) != null);
//            Assert.assertTrue(map.get(LM.PREFIX) != null);
//            Assert.assertTrue(map.get(LM.SUFFIX) != null);
        }
    }

    @Test
    public void getOrder() throws Exception {
        assertTrue(toTest.getOrder() == EDLM.DEFAULT_ORDER);
        toTest = new EDLM(3);
        assertTrue(toTest.getOrder() == 3);
    }

    @Test
    public void createNGram() throws Exception {
        assertEquals(("spatial" + EDLM.WORD_SEPARATOR + "signal"), toTest.createNGram(Arrays.asList(new
                String[]{"spatial", "signal"}), 0, 2));
    }

    @Test
    public void trainDocument() throws Exception {
        List<String> doc = Arrays.asList(new String[]{EDLM.PREFIX, "spatial", "signal", EDLM.SUFFIX});
        toTest.trainDocument(doc);

        Map<String, DoubleValue> map = toTest.probs.get(1);
        assertNull(map.get("event"));
        assertNotNull(map.get("spatial"));
        assertNotNull(map.get("signal"));

        assertTrue(String.valueOf(map.get("spatial").getValue()), map.get("spatial").getValue() == 0.0);
        assertTrue(map.get("signal").getValue() == 0.0);
        assertTrue(String.valueOf(map.get(EDLM.UNKNOWN).getValue()), map.get(EDLM.UNKNOWN).getValue() == 2.0);

        map = toTest.probs.get(2);
        assertNull(map.get("hi" + EDLM.WORD_SEPARATOR + "there"));
        String bigram = toTest.createNGram(doc, 0, 2);
        assertNotNull(map.get(bigram));
        assertTrue(map.get(bigram).getValue() == 0.0);
        assertTrue(String.valueOf(map.get(EDLM.UNKNOWN).getValue()), map.get(EDLM.UNKNOWN).getValue() == 3.0);
    }

    @Test
    public void updateProbs() throws Exception {
        List<String> doc = Arrays.asList(new String[]{EDLM.PREFIX, "hi", "hi", "there", EDLM.SUFFIX});
        toTest.trainDocument(doc);
        toTest.updateProbs();
        assertProbsEquals();
    }

    @Test
    public void train() throws Exception {
        Set<List<String>> docs = new HashSet<>();
        docs.add(new LinkedList<String>(Arrays.asList(new String[]{"hi", "hi", "there"})));
        toTest.train(docs);

        assertProbsEquals();

    }

    private void assertProbsEquals() {
        assertEquals(toTest.probs.get(1).get("hi").getValue(), ((double) (1 + 1) / (5 + 5)), 0.001);
        assertEquals(toTest.probs.get(1).get("there").getValue(), ((double) (0 + 1) / (5 + 5)), 0.001);

        String ngram = toTest.createNGram(Arrays.asList(new String[]{"hi", "hi"}), 0, 2);
        assertEquals(toTest.probs.get(2).get(ngram).getValue(), ((double) (0 + 1) / (1 + 5)), 0.001);
    }

    @Test
    public void getInterpulationWithZero() throws Exception {
        List<Double> probs = new LinkedList<Double>();
        probs.add(0.0);
        probs.add(0.1);

        assertEquals(toTest.getInterpulation(probs), 0.1, 0.001);
    }

    @Test
    public void getInterpulation() throws Exception {
        List<Double> probs = new LinkedList<Double>();
        probs.add(0.5);
        probs.add(0.1);

        assertEquals(toTest.getInterpulation(probs), (EDLM.LAMBDA * 0.5 + (1 - EDLM.LAMBDA) * 0.1), 0.001);
    }

    @Test
    public void getLogProb() throws Exception {
        toTest.train(train);
        toTest.updateProbs();
        LinkedList<String> test = new LinkedList<String>(Arrays.asList(new String[]{EDLM.PREFIX, "spatial", "signal",
                EDLM.SUFFIX}));

        double sumProbs = 0;
        List<Double> logProbs = new LinkedList<Double>();
        Map<String, DoubleValue> map = toTest.probs.get(2);
        for (int start = 0; start < test.size() - 1; ++start) {
            String ngram = toTest.createNGram(test, start, 2);
            sumProbs += toTest.getInterpulation(new LinkedList<Double>(Arrays.asList(new Double[]{Math.log(map.get
                    (ngram)
                    .getValue()), Math.log
                    (toTest.probs.get(1).get(test.get(start)).getValue())})));
        }

        test.removeFirst();
        test.removeLast();
        toTest.generate();
        assertEquals(sumProbs, toTest.getLogProb(test), 0.001);
    }
}